﻿namespace Shop.Services.Infrastructure
{
    using Microsoft.EntityFrameworkCore.Storage;

    public class NikanDbTransaction
    {
        private readonly IDbContextTransaction _transaction;

        public NikanDbTransaction(IDbContextTransaction transaction)
        {
            _transaction = transaction;
        }

        public bool Committed { get; private set; } = false;

        public bool RolledBack { get; private set; } = false;

        public bool Completed { get => Committed || RolledBack; }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            finally
            {
                Committed = true;
            }
        }

        public void Rollback()
        {
            try
            {
                _transaction.Rollback();
            }
            finally
            {
                RolledBack = true;
            }
        }
    }
}

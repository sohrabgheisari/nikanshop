﻿using System.Collections.Generic;

namespace Shop.Services.Infrastructure
{
    public static class ZarinPalMessageDictionary
    {
        static Dictionary<int, string> dic = new Dictionary<int, string>();

        public static string GetError(this int code)
        {
            #region DefineErros
            if (dic.Count == 0)
            {
                dic.Add(-1, "اطلاعات ارسال شده ناقص است");
                dic.Add(-2, "آی پی يا مرچنت كد پذيرنده صحيح نيست");
                dic.Add(-3, "با توجه به محدوديت هاي شاپرك امكان پرداخت با رقم درخواست شده ميسر نمي باشد");
                dic.Add(-4, "سطح تاييد پذيرنده پايين تر از سطح نقره اي است");
                dic.Add(-11, "درخواست مورد نظر يافت نشد");
                dic.Add(-12, "امكان ويرايش درخواست ميسر نمي باشد");
                dic.Add(-21, "هيچ نوع عمليات مالي براي اين تراكنش يافت نشد");
                dic.Add(-22, "تراكنش نا موفق مي باشد");
                dic.Add(-33, "رقم تراكنش با رقم پرداخت شده مطابقت ندارد");
                dic.Add(-34, "سقف تقسيم تراكنش از لحاظ تعداد يا رقم عبور نموده است");
                dic.Add(-40, "اجازه دسترسي به متد مربوطه وجود ندارد");
                dic.Add(-41, "اطلاعات ارسال شده مربوط به AdditionalData غیر معتبر است");
                dic.Add(-42, "مدت زمان معتبر طول عمر شناسه پرداخت بايد بين 30 دقيقه تا 45 روز مي باشد.");
                dic.Add(-54, "درخواست مورد نظر آرشيو شده است.");
                dic.Add(100, "عملیات پرداخت با موفقيت انجام شد.");
                dic.Add(101, "عمليات پرداخت موفق بوده و قبلا PaymentVerification تراکنش انجام شده است");
                dic.Add(502, "تراكنش نا موفق بوده و يا توسط كاربر لغو شده است");
                dic.Add(504, "سفارش یافت نشد");
                dic.Add(505, "سفارش قبلا پرداخت شده است");
                dic.Add(800, "تنظیمات درگاه بانکی انجام نشده است");
                dic.Add(801, "شماره درگاه بانکی تنظیم نشده است");
                dic.Add(802, "سفارش به شما تعلق ندارد");
            }
            #endregion

            string message = "خطا";
            dic.TryGetValue(code, out message);
            return message;
        }
    }
}

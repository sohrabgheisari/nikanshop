﻿using AutoMapper;
using NikanBase.Core.Domain.Shop;
using Shop.Services.Dto.Brand;
using Shop.Services.Dto.Product;
using Shop.Services.Dto.Slider;
using System;

namespace Shop.Services.Infrastructure.Mapping
{
    public static class Mapping
    {
        private static readonly Lazy<IMapper> Lazy = new Lazy<IMapper>(() =>
        {
            var config = new MapperConfiguration(cfg =>
            {
                // This line ensures that internal properties are also mapped over.
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsAssembly;
                cfg.AddProfile<MappingProfile>();
            });
            var mapper = config.CreateMapper();
            return mapper;
        });

        public static IMapper Mapper => Lazy.Value;
    }

    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Product, ProductCardModel>().ReverseMap();
            CreateMap<Discount, DiscountCardModel>();
            CreateMap<Product, ProductModel>().ReverseMap();
            CreateMap<Slider, SliderModel>().ReverseMap();
            CreateMap<Brand, BrandModel>().ReverseMap();
        }
    }
}

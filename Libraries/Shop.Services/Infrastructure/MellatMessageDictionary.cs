﻿using System.Collections.Generic;

namespace Shop.Services.Infrastructure
{
    public static class MellatMessageDictionary
    {
        static Dictionary<int, string> mellatDic = new Dictionary<int, string>();

        public static string GetMessage(this int code)
        {
            #region DefineErros
            if (mellatDic.Count == 0)
            {
               
               mellatDic.Add(0, "تراكنش با موفقيت انجام شد");
               mellatDic.Add(11, "شماره كارت نامعتبر است");
               mellatDic.Add(12, "موجودي كافي نيست");
               mellatDic.Add(13, "رمز نادرست است");
               mellatDic.Add(14, "تعداد دفعات وارد كردن رمز بيش از حد مجاز است");
               mellatDic.Add(15, "كارت نامعتبر است");
               mellatDic.Add(16, "دفعات برداشت وجه بيش از حد مجاز است");
               mellatDic.Add(17, "كاربر از انجام تراكنش منصرف شده است");
               mellatDic.Add(18, "تاريخ انقضاي كارت گذشته است");
               mellatDic.Add(19, "مبلغ برداشت وجه بيش از حد مجاز است");
               mellatDic.Add(111, "صادر كننده كارت نامعتبر است");
               mellatDic.Add(112, "خطاي سوييچ صادر كننده كارت");
               mellatDic.Add(113, "پاسخي از صادر كننده كارت دريافت نشد");
               mellatDic.Add(114, "دارنده كارت مجاز به انجام اين تراكنش نيست");
               mellatDic.Add(21, "پذيرنده نامعتبر است");
               mellatDic.Add(23, "خطاي امنيتي رخ داده است");
               mellatDic.Add(24, "اطلاعات كاربري پذيرنده نامعتبر است");
               mellatDic.Add(25, "مبلغ نامعتبر است");
               mellatDic.Add(31, "پاسخ نامعتبر است");
               mellatDic.Add(32, "فرمت اطلاعات وارد شده صحيح نمي باشد");
               mellatDic.Add(33, "حساب نامعتبر است");
               mellatDic.Add(34, "خطاي سيستمي");
               mellatDic.Add(35, "تاريخ نامعتبر است");
               mellatDic.Add(41, "شماره درخواست تكراري است");
               mellatDic.Add(42, "تراکنش Sale یافت نشد");
               mellatDic.Add(43, "داده شده است Verify قبلا درخواست");
               mellatDic.Add(44, "يافت نشدVerfiy درخواست");
               mellatDic.Add(45, "شده است Settle تراكنش");
               mellatDic.Add(46, "نشده است Settle تراكنش");
               mellatDic.Add(47, "یافت نشد Settle تراكنش");
               mellatDic.Add(48, "شده است Reverse تراكنش");
               mellatDic.Add(49, "يافت نشد Refund تراكنش");
               mellatDic.Add(412, "شناسه قبض نادرست است");
               mellatDic.Add(413, "شناسه پرداخت نادرست است");
               mellatDic.Add(414, "سازمان صادر كننده قبض نامعتبر است");
               mellatDic.Add(415, "زمان جلسه كاري به پايان رسيده است ");
               mellatDic.Add(416, "خطا در ثبت اطلاعات");
               mellatDic.Add(417, "شناسه پرداخت كننده نامعتبر است");
               mellatDic.Add(418, "اشكال در تعريف اطلاعات مشتري");
               mellatDic.Add(419, "تعداد دفعات ورود اطلاعات از حد مجاز گذشته است");
               mellatDic.Add(421, "IP نامعتبر است");
               mellatDic.Add(51, "تراكنش تكراري است");
               mellatDic.Add(54, "تراكنش مرجع موجود نيست");
               mellatDic.Add(55, "تراكنش نامعتبر است");
               mellatDic.Add(61, "خطا در واريز");
               mellatDic.Add(800, "تنظیمات درگاه بانکی انجام نشده است");
               mellatDic.Add(801, "سفارش به شما تعلق ندارد");
               mellatDic.Add(802, "سفارش یافت نشد");
               mellatDic.Add(803, "سفارش قبلا پرداخت شده است");
               mellatDic.Add(804, "خطا در ارتباط با درگاه بانک ملت");
            }
            #endregion

            string message = "خطا";
            mellatDic.TryGetValue(code, out message);
            return message;
        }
    }
}

﻿namespace Shop.Services.Infrastructure
{
    using global::Shop.Core.Domain;
    using global::Shop.Core.Exceptions;
    using global::Shop.Data;
    using global::Shop.Data.Infrastructure;
    using Microsoft.EntityFrameworkCore;
    //using Shop.Core.Domain;
    //using Shop.Core.Exceptions;
    //using Shop.Data;
    //using Shop.Data.Infrastructure;
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public abstract class BaseService
    {
        #region Fields

        private readonly ConcurrentDictionary<string, object> _entitySets = new ConcurrentDictionary<string, object>();

        #endregion

        #region Properties

        protected AppDbContext _context { get; private set; }

        #endregion

        #region Constructors

        public BaseService(AppDbContext context)
        {
            _context = context;
        }

        #endregion

        #region Methods

        protected virtual void Insert<T>(T entity)
            where T : BaseEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            Entities<T>().Add(entity);
        }

        protected virtual void Insert<T>(IEnumerable<T> entities)
            where T : BaseEntity
        {
            if (entities?.Any() != true)
            {
                throw new ArgumentException(nameof(entities));
            }

            Entities<T>().AddRange(entities);
        }

        protected virtual void Update<T>(T entity, bool update = true, params Expression<Func<T, object>>[] updatedProperties)
                   where T : BaseEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            var entry = _context.Entry(entity);
            if (entry == null)
            {
                var cachedEntry = _context.ChangeTracker.Entries<T>().FirstOrDefault(x => x.Entity.Id == entity.Id);
                if (cachedEntry != null)
                {
                    cachedEntry.State = EntityState.Detached;
                }

                Entities<T>().Attach(entity);
                entry = _context.Entry(entity);
            }

            entry.State = EntityState.Modified;

            if (updatedProperties?.Length > 0)
            {
                var names = updatedProperties.Select(prop =>
                {
                    if (prop.Body is MemberExpression memberExpression)
                    {
                        if (memberExpression.NodeType != ExpressionType.MemberAccess || memberExpression.Member == null || memberExpression.Member.MemberType != System.Reflection.MemberTypes.Property)
                        {
                            throw new ArgumentException("The expression must get a member (property).", "updatedProperties");
                        }

                        return memberExpression.Member.Name;
                    }
                    else if (prop.Body is UnaryExpression valueExpression && valueExpression.Operand is MemberExpression memberExpersion)
                    {
                        return memberExpersion.Member.Name;
                    }
                    else
                    {
                        throw new ArgumentException("The expression must get a member (property).", "updatedProperties");
                    }
                }).ToList();

                var properties = entry.Metadata.GetProperties().ToArray();
                var entityPropertyArray = properties.Select(m => m.Name).ToList();
                if (update)
                {
                    var untocheds = entityPropertyArray.Except(names).ToList();
                    untocheds.ForEach(x => entry.Property(x).IsModified = false);
                }
                else
                {
                    names.ForEach(x => entry.Property(x).IsModified = false);
                }
            }
        }

        protected virtual void Update<T>(IEnumerable<T> entities, bool update = true, params Expression<Func<T, object>>[] updatedProperties)
                   where T : BaseEntity
        {
            if (entities?.Any() != true)
            {
                throw new ArgumentException(nameof(entities));
            }

            Entities<T>().AttachRange(entities);
            foreach (var entity in entities)
            {
                _context.Entry(entity).State = EntityState.Modified;
            }

            if (updatedProperties?.Length > 0)
            {
                var names = updatedProperties.Select(prop =>
                {
                    if (prop.Body is MemberExpression memberExpression)
                    {
                        if (memberExpression.NodeType != ExpressionType.MemberAccess || memberExpression.Member == null || memberExpression.Member.MemberType != System.Reflection.MemberTypes.Property)
                        {
                            throw new ArgumentException("The expression must get a member (property).", "updatedProperties");
                        }

                        return memberExpression.Member.Name;
                    }
                    else if (prop.Body is UnaryExpression valueExpression && valueExpression.Operand is MemberExpression memberExpersion)
                    {
                        return memberExpersion.Member.Name;
                    }
                    else
                    {
                        throw new ArgumentException("The expression must get a member (property).", "updatedProperties");
                    }
                }).ToList();

                var first = entities.First();
                var firstEntry = _context.Entry(first);
                var properties = firstEntry.Metadata.GetProperties().ToArray();
                var entityPropertyArray = properties.Select(m => m.Name).ToList();
                var untocheds = entityPropertyArray.Except(names).ToList();
                foreach (var entity in entities)
                {
                    var entry = _context.Entry(entity);
                    if (update)
                    {
                        untocheds.ForEach(x => entry.Property(x).IsModified = false);
                    }
                    else
                    {
                        names.ForEach(x => entry.Property(x).IsModified = false);
                    }
                }
            }
        }
        protected virtual void Delete<T>(T entity)
            where T : BaseEntity
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            Entities<T>().Remove(entity);
        }

        protected virtual void Delete<T>(IEnumerable<T> entities)
            where T : BaseEntity
        {
            if (entities?.Any() != true)
            {
                throw new ArgumentException(nameof(entities));
            }

            Entities<T>().RemoveRange(entities);
        }

        protected Task<int> Save(object cacheGroupKey = null)
        {
            return _context.SaveChangesAsync();
        }

        protected virtual IQueryable<T> Table<T>()
            where T : BaseEntity
        {
            return Entities<T>().AsNoTracking();
        }

        protected virtual IQueryable<T> TableTracking<T>()
            where T : BaseEntity
        {
            return Entities<T>();
        }

        protected virtual DbSet<T> Entities<T>()
            where T : BaseEntity
        {
            return _entitySets.GetOrAdd(typeof(T).FullName, (key) =>
            {
                return _context.Set<T>();
            }) as DbSet<T>;
        }

        protected virtual Task<T> GetById<T>(long entityId)
            where T : BaseEntity
        {
            return Table<T>().SingleOrDefaultAsync(x => x.Id == entityId);
        }

        protected async Task CheckIfPrimaryKeyIsUsed<T>(Guid id, string friendlyName = null)
        {
            var entityConfigurations = _context.Model.GetEntityTypes().FirstOrDefault(m => m.Name == typeof(T).FullName);
            if (entityConfigurations == null)
            {
                throw new ArgumentException("No Mapping Exists For Type " + typeof(T).FullName);
            }

            var tblName = entityConfigurations.Relational().TableName;

            StoredProcQuery query = _context
                .StoredProc("sp_IsPKValueUsed_Sc")
                .WithParam("tblName", tblName)
                .WithParam("id", id)
                .WithParam("usedTbls", null, direction: System.Data.ParameterDirection.Output, size: 1000);

            // return value does not work
            // .WithParam("returnValue", null, direction: System.Data.ParameterDirection.ReturnValue, size: 1000);
            await query.ExecuteSqlCommandAsync();

            string usedTableNames = query.Parameters[2].Value?.ToString().Trim(',');
            bool isUsed = !string.IsNullOrEmpty(usedTableNames);

            if (!isUsed)
            {
                return;
            }

            if (!string.IsNullOrEmpty(usedTableNames))
            {
                List<string> usedTables = usedTableNames.Split(',').Distinct().ToList();
                string resourceKey = $"msg.NotDeleted-RelatedDataFound{(friendlyName == null ? string.Empty : "-withname")}";
                NikanException ex = new NikanException(resourceKey, usedTables)
                {
                    Arguments = friendlyName == null ? null : new object[] { friendlyName },
                };
                ex.Data.Add($"Delete Entity {typeof(T).Name}", $"Id-{id} depends on other tables ({usedTableNames})");
                throw ex;
            }
        }


        protected async Task RunInTransaction(Func<NikanDbTransaction, Task> action)
        {
            using (var scope = await _context.Database.BeginTransactionAsync())
            {
                var transaction = new NikanDbTransaction(scope);
                await action(transaction);

                if (!transaction.Completed)
                {
                    transaction.Commit();
                }
            }
        }

        protected async Task<T> RunInTransaction<T>(Func<NikanDbTransaction, Task<T>> action)
        {
            using (var scope = await _context.Database.BeginTransactionAsync())
            {
                var transaction = new NikanDbTransaction(scope);
                var result = await action(transaction);

                if (!transaction.Completed)
                {
                    transaction.Commit();
                }

                return result;
            }
        }

        protected void RunInTransaction(Action<NikanDbTransaction> action)
        {
            using (var scope = _context.Database.BeginTransaction())
            {
                var transaction = new NikanDbTransaction(scope);
                action(transaction);

                if (!transaction.Completed)
                {
                    transaction.Commit();
                }
            }
        }

        protected T RunInTransaction<T>(Func<NikanDbTransaction, T> action)
        {
            using (var scope = _context.Database.BeginTransaction())
            {
                var transaction = new NikanDbTransaction(scope);
                var result = action(transaction);

                if (!transaction.Completed)
                {
                    transaction.Commit();
                }

                return result;
            }
        }

        private void WriteProgress(decimal percentage)
        {

        }

        #endregion
    }
}

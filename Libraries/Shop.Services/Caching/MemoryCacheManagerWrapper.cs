﻿namespace Shop.Services.Caching
{
    using global::Shop.Core.Infrastructure;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Threading.Tasks;

    public class MemoryCacheManagerWrapper : ICacheManager
    {
        #region Fields

        private readonly IStaticCacheManager _cacheManager;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger _logger;
        private IAppContext _appContext;

        #endregion

        #region Ctors

        public MemoryCacheManagerWrapper(IStaticCacheManager cacheManager, ILogger<MemoryCacheManagerWrapper> logger, IServiceProvider serviceProvider)
        {
            _cacheManager = cacheManager;
            _serviceProvider = serviceProvider;
            _logger = logger;
        }

        #endregion

        #region Methods

        public void Clear()
        {
            _cacheManager.Clear();
        }

        public T Get<T>(string key, Func<T> acquire, int? cacheTime = null)
        {
            key = InjectVariablesIntoKey(key);
            return _cacheManager.Get(key, acquire, cacheTime);
        }

        public Task<T> GetAsync<T>(string key, Func<Task<T>> acquire, int? cacheTime = null)
        {
            key = InjectVariablesIntoKey(key);
            return _cacheManager.GetAsync(key, acquire, cacheTime);
        }

        public bool IsSet(string key)
        {
            key = InjectVariablesIntoKey(key);
            return _cacheManager.IsSet(key);
        }

        public void Remove(string key)
        {
            key = InjectVariablesIntoKey(key);
            _cacheManager.Remove(key);
        }

        public void RemoveByPattern(string pattern)
        {
            pattern = InjectVariablesIntoKey(pattern);
            _cacheManager.RemoveByPattern(pattern);
        }

        public void Set(string key, object data, int cacheTime)
        {
            key = InjectVariablesIntoKey(key);
            _cacheManager.Set(key, data, cacheTime);
        }

        #endregion

        #region Utilities

        private string InjectVariablesIntoKey(string key)
        {
            try
            {
                _appContext = _serviceProvider.GetService(typeof(IAppContext)) as IAppContext;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            //Dictionary<string, Func<Guid>> variables = new Dictionary<string, Func<Guid>>
            //{
            //    { "companyId", () => _appContext.CompanyId ?? Guid.Empty },
            //    { "periodId", () => _appContext.FiscalPeriod?.Id ?? Guid.Empty },
            //    { "customerId", () => _appContext.HasCustomer() ? _appContext.Customer.Id : Guid.Empty },
            //};

            //foreach (var item in variables)
            //{
            //    string itemKey = $"[{item.Key}]";

            //    if (key.IndexOf(itemKey) > -1)
            //    {
            //        key = key.Replace(itemKey, item.Value().ToString());
            //    }
            //}

            return key;
        }

        #endregion
    }
}

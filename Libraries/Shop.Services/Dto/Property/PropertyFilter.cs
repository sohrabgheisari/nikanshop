﻿namespace Shop.Services.Dto.Property
{
    public class PropertyFilter
    {
        public long Id { get; set; }
        public string Value { get; set; }
    }
}

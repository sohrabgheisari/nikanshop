﻿using System.Collections.Generic;

namespace Shop.Services.Dto.Property
{
    public class ProductFilterGroups
    {
        public long Id { get; set; }
        public List<string> Values { get; set; }
    }
}

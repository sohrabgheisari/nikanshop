﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Shop.Services.Dto.Property
{
    public class PropertyFilterModel
    {
        public string Name { get; set; }
        public List<SelectListItem> Values { get; set; }
    }
}

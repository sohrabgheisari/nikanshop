﻿namespace Shop.Services.Dto.Getway
{
    public class ZarinPalResult
    {
        public int StatusCode { get; set; }
        public string TransactionNumber { get; set; }
        public string RefNumber { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }
        public bool IsSuccess { get; set; }
    }
}

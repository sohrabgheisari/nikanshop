﻿namespace Shop.Services.Dto.Getway
{
    public class MellatPaymentResult
    {
        public string ResCode { get; set; }
        public string TransactionNumber { get; set; }
        public string RefNumber { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}

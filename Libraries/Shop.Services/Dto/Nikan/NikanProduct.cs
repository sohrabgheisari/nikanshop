﻿using Newtonsoft.Json;

namespace Shop.Services.Dto.Nikan
{
    public class NikanProduct
    {
        /// <summary>
        /// کد کالا 
        /// this field is equivalent to Lots_LCode in nikan database
        /// </summary>
        public int LotsId { get; set; }

        /// <summary>
        /// شناسه کالا
        /// </summary>
        public int Lots_Code { get; set; }

        /// <summary>
        /// نام کالا
        /// </summary>
        public string LotsName { get; set; }

        /// <summary>
        /// تصویر کالا
        /// </summary>
        public byte[] LPic { get; set; }

        /// <summary>
        /// آدرس تصویر کالا
        /// </summary>
        public string LPicUrl { get; set; }

        /// <summary>
        /// نظرات کالا
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// بارکد کالا
        /// </summary>
        public string Lots_BarCode { get; set; }

        /// <summary>
        /// نام دسته بندی سطح ۱
        /// </summary>
        public string Level1_Name { get; set; }

        /// <summary>
        /// نام دسته بندی سطح 2
        /// </summary>
        public string Level2_Name { get; set; }

        /// <summary>
        /// نام دسته بندی سطح 3
        /// </summary>
        public string Level3_Name { get; set; }


        /// <summary>
        /// شناسه آخرین سطح دسته بندی کالا
        /// </summary>
        [JsonProperty("lastLevelCode")]
        public string Level_ID { get; set; }

        /// <summary>
        /// کد گروه ویژه کالا
        /// </summary>
        public int SpGroup_Code { get; set; }

        /// <summary>
        /// نام گروه ویژه کالا
        /// </summary>
        public string SpGroup_Name { get; set; }

        /// <summary>
        /// آخرین تاریخ فروش کالا
        /// </summary>
        public string Lots_LastSellDate { get; set; }

        /// <summary>
        /// آخرین تاریخ خرید کالا
        /// </summary>
        public string Lots_LastBuyDate { get; set; }

        /// <summary>
        /// آخرین مبلغ فروش کالا
        /// </summary>
        public long Lots_LastForoosh { get; set; }

        /// <summary>
        /// آخرین مبلغ خرید کالا
        /// </summary>
        public long Lots_LastKharid { get; set; }

        /// <summary>
        /// درصد مالیات
        /// </summary>
        public double TaxPercent { get; set; }

        /// <summary>
        /// درصد عوارض
        /// </summary>
        public double TollPercent { get; set; }

        /// <summary>
        /// اولویت نمایش به ترتیب گروه بندی کالا
        /// </summary>
        public int DisplayOrder { get; set; }

        /// <summary>
        /// کد واحد اصلی
        /// </summary>
        public int DefUnit { get; set; }

        /// <summary>
        /// نام واحد اصلی
        /// </summary>
        public string DefUnitDesc { get; set; }

        /// <summary>
        /// کد واحد فرعی
        /// </summary>
        public int SecUnit { get; set; }

        /// <summary>
        /// نام واحد فرعی
        /// </summary>
        public string SecUnitDesc { get; set; }

        /// <summary>
        /// ضریب تبدیل اصلی که برای کالای تک واحدی 0 است
        /// </summary>
        public double Zarib { get; set; }

        /// <summary>
        /// ضریب تبدیل که برای کالای تک واحدی یک است
        /// </summary>
        public double Lots_Cm { get; set; }

        /// <summary>
        /// موجودی کل بر اساس واحد کوچکتر
        /// </summary>
        public int LotsCount { get; set; }

        /// <summary>
        /// موجودی به واحد اصلی
        /// </summary>
        public int Lots_Count { get; set; }

        /// <summary>
        /// موجودی به واحد فرعی
        /// </summary>
        [JsonProperty("lotsCount1")]
        public int Lots_Count1 { get; set; }

        /// <summary>
        /// false ::  واحد اصلی , واحد بزرگتر باشد
        /// true ::  واحد اصلی , واحد کوچکتر باشد
        /// </summary>
        [JsonProperty("gunit")]
        public bool Lots_Gunit { get; set; }

        /// <summary>
        /// نرخ فروش 1 مربوط به واحد اصلی
        /// </summary>
        public long DefPrice1 { get; set; }

        /// <summary>
        /// نرخ فروش 2 مربوط به واحد اصلی
        /// </summary>
        public long DefPrice2 { get; set; }

        /// <summary>
        /// نرخ فروش 3 مربوط به واحد اصلی
        /// </summary>
        public long DefPrice3 { get; set; }

        /// <summary>
        /// نرخ فروش 4 مربوط به واحد اصلی
        /// </summary>
        public long DefPrice4 { get; set; }

        /// <summary>
        /// نرخ فروش 5 مربوط به واحد اصلی
        /// </summary>
        public long DefPrice5 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 1 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Up1 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 2 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Up2 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 3 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Up3 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 4 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Up4 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 5 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Up5 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 1 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Down1 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 2 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Down2 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 3 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Down3 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 4 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Down4 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 5 مربوط به واحد اصلی کالا
        /// </summary>
        public long DefLimit_Down5 { get; set; }

        /// <summary>
        /// نرخ فروش 1 مربوط به واحد فرعی
        /// </summary>
        public long SecPrice1 { get; set; }

        /// <summary>
        /// نرخ فروش 2 مربوط به واحد فرعی
        /// </summary>
        public long SecPrice2 { get; set; }

        /// <summary>
        /// نرخ فروش 3 مربوط به واحد فرعی
        /// </summary>
        public long SecPrice3 { get; set; }

        /// <summary>
        /// نرخ فروش 4 مربوط به واحد فرعی
        /// </summary>
        public long SecPrice4 { get; set; }

        /// <summary>
        /// نرخ فروش 5 مربوط به واحد فرعی
        /// </summary>
        public long SecPrice5 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 1 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Up1 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 2 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Up2 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 3 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Up3 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 4 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Up4 { get; set; }

        /// <summary>
        ///  حد بالای مجاز خطای کاربر در ورود قیمت 5 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Up5 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 1 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Down1 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 2 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Down2 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 3 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Down3 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 4 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Down4 { get; set; }

        /// <summary>
        ///  حد پایین مجاز خطای کاربر در ورود قیمت 5 مربوط به واحد فرعی کالا
        /// </summary>
        public long SecLimit_Down5 { get; set; }
    }
}

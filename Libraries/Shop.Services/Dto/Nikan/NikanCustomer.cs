﻿namespace Shop.Services.Dto.Nikan
{
    public class NikanCustomer
    {
        public int CustId { get; set; }
        public string CustName { get; set; }
        public string ShopName { get; set; }
        public string Tel1 { get; set; }
        public string Tel2 { get; set; }
        public string Mobile { get; set; }
        public string Accs_Mobil2 { get; set; }
        public string CustAddr { get; set; }
        public string UserName { get; set; }
        public string Province_Name { get; set; }
        public string P_City_Name { get; set; }
        public int? AccsGroup_Code { get; set; }
        public string AccsGroup_Desc { get; set; }
        public long Mandeh { get; set; }
        public int DefaultPrice { get; set; }     
    }
}

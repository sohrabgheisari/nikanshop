﻿namespace Shop.Services.Dto.Nikan
{
    public class NikanCreateAccountingOperationDetailsModel
    {
        /// <summary>
        /// value of this field comes from field LotsId in Product.cs
        /// شناسه کالا
        /// </summary>
        public int ProductCode { get; set; }

        /// <summary>
        /// واحد اندازه گیری اصلی
        /// </summary>
        public int DefUnit { get; set; }

        /// <summary>
        /// تعداد کالا از واحد اندازه گیری اصلی
        /// </summary>
        public double DefCount { get; set; }

        /// <summary>
        /// قیمت کالا برای واحد اندازه گیری اصلی
        /// </summary>
        public long DefPrice { get; set; }

        /// <summary>
        /// واحد اندازه گیری فرعی
        /// </summary>
        public int SecUnit { get; set; }

        /// <summary>
        /// تعداد کالا از واحد اندازه گیری فرعی
        /// </summary>
        public double SecCount { get; set; }

        /// <summary>
        /// قیمت کالا برای واحد اندازه گیری فرعی
        /// </summary>
        public long SecPrice { get; set; }

        /// <summary>
        /// میزان تخفیف ریالی
        /// </summary>
        public long DiscountAmount { get; set; }

        /// <summary>
        /// میزان تخفیف درصدی
        /// </summary>
        public double DiscountPercent { get; set; }

        /// <summary>
        /// تعداد اشانتیون
        /// </summary>
        public int BonusCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Services.Dto.Nikan
{
    public class NikanAccountingOperationResult
    {
        public byte HFac_Type { get; set; }

        public int HFac_No { get; set; }

        public int HFac_Code { get; set; }
    }
}

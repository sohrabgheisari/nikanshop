﻿using System.Collections.Generic;

namespace Shop.Services.Dto.Nikan
{
    public class NikanCreateAccountingOperationHeaderModel
    {
        /// <summary>
        /// نوع عملیات
        /// </summary>
        public byte Type { get; set; }
        /// <summary>
        /// شناسه مشتری
        /// </summary>
        public int CustomerId { get; set; }
        /// <summary>
        /// پرداخت نقدی؟
        /// </summary>
        public bool PaidByCash { get; set; }

        /// <summary>
        /// میزان پرداخت نقدی
        /// </summary>
        public long PaidByCashAmount { get; set; }

        /// <summary>
        /// پرداخت نسیه؟
        /// </summary>
        public bool PaidByCredit { get; set; }

        /// <summary>
        /// روز تسویه
        /// </summary>
        public int PaidByCreditDays { get; set; }

        /// <summary>
        /// میزان پرداخت نسیه
        /// </summary>
        public long PaidByCreditAmount { get; set; }

        /// <summary>
        /// پرداخت با چک؟
        /// </summary>
        public bool PaidByCheque { get; set; }

        /// <summary>
        /// تاریخ چک
        /// </summary>
        public int PaidByChequeDays { get; set; }

        /// <summary>
        /// میزان پرداخت چک
        /// </summary>
        public long PaidByChequeAmount { get; set; }

        /// <summary>
        /// پرداخت با پوز؟
        /// </summary>
        public bool PaidByPos { get; set; }

        /// <summary>
        /// میزان پرداخت پوز
        /// </summary>
        public long PaidByPosAmount { get; set; }
        /// <summary>
        /// تخفیف
        /// </summary>
        public long Discount { get; set; }
        public List<NikanCreateAccountingOperationDetailsModel> Items { get; set; }

    }
}

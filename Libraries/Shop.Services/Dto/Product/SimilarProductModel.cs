﻿using Shop.Core.Domain;

namespace Shop.Services.Dto.Product
{
    public class SimilarProductModel:BaseEntity
    {
        public SimilarProductModel()
        {

        }
        public long SimilarProductId { get; set; }
        public string SimilarProductName { get; set; }
    }
}

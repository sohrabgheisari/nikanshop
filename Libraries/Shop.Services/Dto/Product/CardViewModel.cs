﻿using System.Collections.Generic;

namespace Shop.Services.Dto.Product
{
    public class CardViewModel
    {
        public CardViewModel()
        {

        }
        public List<ShopCardModel> Card { get; set; }
        public List<string> Message { get; set; }
        public bool IsFull { get; set; }
        public bool Status { get; set; }
        public bool IsFactor { get; set; }
        public string FactorNumber { get; set; }
    }
}

﻿using Shop.Core.Domain;

namespace Shop.Services.Dto.Product
{
    public class ProductTraitsModel:BaseEntity
    {
        public ProductTraitsModel()
        {

        }
        public long ProductId { get; set; }
        public long ColorTriatId { get; set; }
        public long WarrantyTriatId { get; set; }
        public int Inventory { get; set; }
        public decimal PriceIncrease { get; set; }
        public bool IsActive { get; set; }
        public TraitModel ColorTrait { get; set; }
        public  TraitModel WarrantyTrait { get; set; }
    }
}

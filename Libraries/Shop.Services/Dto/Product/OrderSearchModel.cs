﻿using Shop.Core.Enums;

namespace Shop.Services.Dto.Product
{
    public class OrderSearchModel
    {
        public StatusTypes? Status { get; set; }
        public ShippingSatusTypes? Shipping { get; set; }
        public long? UserId { get; set; }
        public string PayDate_Start { get; set; }
        public string PayDate_End { get; set; }
    }
}

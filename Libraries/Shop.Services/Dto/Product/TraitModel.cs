﻿using NikanBase.Core.Enums;
using Shop.Core.Domain;

namespace Shop.Services.Dto.Product
{
    public class TraitModel:BaseEntity
    {
        public TraitTypes Type { get; set; }
        public string Title { get; set; }
    }
}

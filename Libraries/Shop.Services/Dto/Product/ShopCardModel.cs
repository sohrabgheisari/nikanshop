﻿namespace Shop.Services.Dto.Product
{
    public class ShopCardModel
    {
        public ProductCardModel Product { get; set; }
        public long OrderId { get; set; }
        public string OrderNumber { get; set; }
        public long UserId { get; set; }
    }
}

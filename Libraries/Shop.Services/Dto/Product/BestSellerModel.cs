﻿namespace Shop.Services.Dto.Product
{
    public class BestSellerModel
    {
        public BestSellerModel()
        {

        }
        public long ProductId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
    }
}

﻿using Shop.Core.Domain;
using System.ComponentModel.DataAnnotations;

namespace Shop.Services.Dto.Product
{
    public class ProductPropertyModel:BaseEntity
    {
        public ProductPropertyModel()
        {

        }
        public long ProductId { get; set; }
        public long PropertyId { get; set; }
        public long CategoryId { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage = "مقدار ویژگی را وارد نمایید")]
        public string Value { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
        public string CategoryName { get; set; }
        public string PropertyName { get; set; }
    }
}

﻿using Shop.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shop.Services.Dto.Product
{
    public class ProductCardModel:BaseEntity
    {
        public ProductCardModel()
        {

        }
        public long OrderItemId { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Count { get; set; }
        public string ImageUrl { get; set; }
        public long? DiscountId { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal PriceWithDiscount { get; set; }
        public decimal PriceIncrease { get; set; }
        public long? ProductTraitId { get; set; }
        public DiscountCardModel Discount { get; set; }
       public List<ProductTraitsModel> ProductTraits { get; set; }
       public string TraitText { get; set; }
        public void FillFields()
        {
            if (Discount != null)
            {
                if ((Discount.DiscountTo.HasValue && Discount.DiscountFrom.HasValue && DateTime.Now.Date >= Discount.DiscountFrom.Value.Date &&
                    DateTime.Now.Date <= Discount.DiscountTo.Value.Date) || (!Discount.DiscountTo.HasValue && !Discount.DiscountFrom.HasValue) ||
                    (!Discount.DiscountTo.HasValue && Discount.DiscountFrom.HasValue && DateTime.Now.Date >= Discount.DiscountFrom.Value.Date))
                {
                    if (Discount.Type == Core.Enums.DiscountTypes.Percentage)
                    {
                        if (Discount.Percentage.HasValue)
                        {
                            decimal discountPrice = Math.Round(Discount.Percentage.Value * Price / 100);
                            if (Discount.MaxAmount.HasValue && discountPrice > Discount.MaxAmount.Value)
                            {
                                discountPrice = Discount.MaxAmount.Value;
                            }
                            if (discountPrice > 0)
                            {
                                DiscountAmount = discountPrice;
                                PriceWithDiscount = Price - DiscountAmount;
                            }
                        }

                    }
                    else
                    {
                        if (Discount.Amount.HasValue && Discount.Amount.Value > 0 && Discount.Amount.Value < Price)
                        {
                            DiscountAmount = Discount.Amount.Value;
                            PriceWithDiscount = Price - DiscountAmount;
                        }
                    }
                }

            }
            else
            {
                PriceWithDiscount = Price;

            }

            if (ProductTraitId.HasValue)
            {
                string text = string.Empty;
                var productTrait = ProductTraits.FirstOrDefault(x => x.Id == ProductTraitId.Value);
                if (productTrait.ColorTrait != null)
                {
                    text += $"رنگ {productTrait.ColorTrait.Title}";
                }
                if (productTrait.WarrantyTrait != null)
                {
                    text += $" - گارانتی {productTrait.WarrantyTrait.Title}";
                }
                PriceIncrease = productTrait.PriceIncrease;
                TraitText = text;
            }
        }
    }
}

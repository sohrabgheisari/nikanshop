﻿using Shop.Core.Domain;
using System.Collections.Generic;

namespace Shop.Services.Dto.Product
{
    public class GroupAllocationPropertyModel:BaseEntity
    {
        public List<GroupPropertyModel> GroupProperties { get; set; }
    }
}

﻿using Shop.Core.Domain;

namespace Shop.Services.Dto.Product
{
    public class GroupPropertyModel:BaseEntity
    {
        public long PropertyCategoryId { get; set; }
        public long PropertyId { get; set; }
        public long ProductCategoryId { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
        public string PropertyCategoryName { get; set; }
        public string PropertyName { get; set; }
        public string ProductCategoryName { get; set; }

    }
}

﻿using Shop.Core.Domain;
using Shop.Core.Enums;
using System;

namespace Shop.Services.Dto.Product
{
    public class DiscountCardModel:BaseEntity
    {
        public DiscountCardModel()
        {

        }
        public DiscountTypes Type { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Percentage { get; set; }
        public DateTime? DiscountFrom { get; set; }
        public DateTime? DiscountTo { get; set; }
    }
}

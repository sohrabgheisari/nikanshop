﻿using Shop.Core.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shop.Services.Dto.Product
{
    public class ProductModel:BaseEntity
    {
        public ProductModel()
        {

        }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public bool Publish { get; set; }
        public bool SpecialProduct { get; set; }
        public bool NewProduct { get; set; }
        public decimal Price { get; set; }
        public int Inventory { get; set; }
        public string ImageUrl { get; set; }
        public long? DiscountId { get; set; }
        public int MinCart { get; set; }
        public int MaxCart { get; set; }
        public string BarCode { get; set; }
        public long? BrandId { get; set; }
        [Range(0, double.MaxValue,ErrorMessage ="امتیاز یاید ")]
        public decimal Score { get; set; }
        public List<ProductPropertyModel> Properties { get; set; }
        public List<SimilarProductModel> Similars { get; set; }
        public List<ProductTraitModel> Traits { get; set; }
    }
}

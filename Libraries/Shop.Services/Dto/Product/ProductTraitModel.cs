﻿using Shop.Core.Domain;

namespace Shop.Services.Dto.Product
{
    public class ProductTraitModel:BaseEntity
    {
        public ProductTraitModel()
        {

        }
        public long ProductId { get; set; }
        public long? ColorTriatId { get; set; }
        public string ColorTriatName { get; set; }
        public long? WarrantyTriatId { get; set; }
        public string WarrantyTriatName { get; set; }
        public string WarrantyDate { get; set; }
        public int TraitInventory { get; set; }
        public decimal PriceIncrease { get; set; }
        public bool IsActive { get; set; }
        public int MinimumCart { get; set; }
        public int MaximumCart { get; set; }
        public string BarCode { get; set; }

    }
}

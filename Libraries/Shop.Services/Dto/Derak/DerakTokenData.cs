﻿namespace Shop.Services.Dto.Derak
{
    public class DerakTokenData
    {
        public string AccessToken { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public int ExpiredIn { get; set; }
    }
}

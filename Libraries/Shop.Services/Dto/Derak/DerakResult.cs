﻿namespace Shop.Services.Dto.Derak
{
    public class DerakResult<T>
    {
        public string RequestId { get; set; }
        public bool IsSuccess { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }
        public T Data { get; set; }
    }
}

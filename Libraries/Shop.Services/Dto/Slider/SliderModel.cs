﻿using Shop.Core.Domain;
using Shop.Core.Enums;

namespace Shop.Services.Dto.Slider
{
    public class SliderModel:BaseEntity
    {
        public SliderModel()
        {

        }
        public string SliderUrl { get; set; }
        public int Order { get; set; }
        public SliderTypes Type { get; set; }
    }
}

﻿using Shop.Core.Domain;

namespace Shop.Services.Dto.Brand
{
    public class BrandModel:BaseEntity
    {
        public string Title { get; set; }
        public string ImageUrl { get; set; }
    }
}

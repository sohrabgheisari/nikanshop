﻿using NikanBase.Core.Enums;

namespace Shop.Services.Dto.Setting
{
    public class GetwaySettingsModel
    {
        public ZarinPalTypes BankGateway_Type { get; set; }
        public string BankGateway_TerminalNo { get; set; }
    }
}

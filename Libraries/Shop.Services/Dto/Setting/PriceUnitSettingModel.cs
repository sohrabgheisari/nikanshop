﻿using NikanBase.Core.Enums;

namespace Shop.Services.Dto.Setting
{
    public class PriceUnitSettingModel
    {
        public PriceUnitTypes Unit { get; set; }
    }
}

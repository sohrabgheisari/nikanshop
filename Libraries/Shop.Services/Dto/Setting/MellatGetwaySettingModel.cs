﻿namespace Shop.Services.Dto.Setting
{
    public class MellatGetwaySettingModel
    {
        public string TerminalId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}

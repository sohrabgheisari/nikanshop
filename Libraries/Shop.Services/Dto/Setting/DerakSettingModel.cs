﻿namespace Shop.Services.Dto.Setting
{
    public class DerakSettingModel
    {
        public string PublicKey { get; set; }
        public string BaseUrl { get; set; }
    }
}

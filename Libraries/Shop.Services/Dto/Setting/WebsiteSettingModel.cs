﻿namespace Shop.Services.Dto.Setting
{
    public class WebsiteSettingModel
    {
        public string LogoUrl { get; set; }
        public string AboutUs { get; set; }
        public string ShopName { get; set; }
        public bool ShowAboutUs { get; set; }
        public bool ShowContactUs { get; set; }
        public string InstagramAddress { get; set; }
        public string FaceBookAddress { get; set; }
        public string TelegramAddress { get; set; }
        public string WhatsAppAddress { get; set; }
        public string ShopAddress { get; set; }
        public string ShopPhones { get; set; }
        public int PageCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shop.Core.Domain.Users;
using Shop.Core.Enums;
using Shop.Data;

namespace Shop.Services.Users
{
    public class UserManager : UserManager<User>
    {
        private readonly AppDbContext _context;
        private readonly RoleManager<Role> _roleManager;
        private readonly SignInManager _signInManager;
        public UserManager(IUserStore<User> store, IOptions<IdentityOptions> optionsAccessor,
            IPasswordHasher<User> passwordHasher, IEnumerable<IUserValidator<User>> userValidators,
            IEnumerable<IPasswordValidator<User>> passwordValidators, ILookupNormalizer keyNormalizer,
            IdentityErrorDescriber errors, IServiceProvider services, ILogger<UserManager<User>> logger,
            RoleManager<Role> roleManager, SignInManager signInManager, AppDbContext context
            ) :

            base(store, optionsAccessor, passwordHasher, userValidators, passwordValidators, keyNormalizer, errors, services, logger)
        {
            _roleManager = roleManager;
            _signInManager = signInManager;
            _context = context;
        }

        public async Task<IdentityResult> Insert(User user, string password, List<string> roleNames)
        {
            var roles = await _roleManager.Roles.Where(m => roleNames.Contains(m.Name)).Select(m => m.Id).ToListAsync();

            var result = await CreateAsync(user, password);
            if (result.Succeeded)
            {
                foreach (var role in roleNames)
                {
                    await AddToRoleAsync(user, role);
                }
            }

            return result;
        }
        public async Task<IdentityResult> Update(User user, List<string> roles, bool updateSensitiveFields)
        {
            IdentityResult result = null;
            var mainUser = await GetByIdWithRoles(user.Id);
            if (mainUser == null)
            {
                return IdentityResult.Failed(new IdentityError
                {
                    Code = "UserNotFound"
                });
            }
            using (var trans = await _context.Database.BeginTransactionAsync(System.Data.IsolationLevel.ReadUncommitted))
            {
                mainUser.Email = user.Email;
                mainUser.FirstName = user.FirstName;
                mainUser.LastName = user.LastName;
                mainUser.Address = user.Address;
                mainUser.PhoneNumber = user.PhoneNumber;
                mainUser.UserName = user.UserName;
                mainUser.Address = user.Address;

                if (updateSensitiveFields)
                {
                    mainUser.IsActive = user.IsActive;

                    //update user roles    
                    var prevRoles = await GetRolesAsync(mainUser);
                    foreach (var role in prevRoles)
                    {
                        await RemoveFromRoleAsync(mainUser, role);
                    }
                    foreach (var role in roles)
                    {
                        await AddToRoleAsync(mainUser, role);
                    }
                }
                result = await UpdateAsync(mainUser);
                if (result.Succeeded)
                {
                    result = await UpdateSecurityStampAsync(mainUser);
                }

                trans.Commit();
            }

            return result;
        }

        public async Task<IdentityResult> ChangePasswordAndCookie(User user, string oldPassword, string newPassword)
        {
            var result = await ChangePasswordAsync(user, oldPassword, newPassword);
            if (result.Succeeded)
            {
                // reflect the changes in the Identity cookie
                await UpdateSecurityStampAsync(user).ConfigureAwait(false);
                await _signInManager.RefreshSignInAsync(user).ConfigureAwait(false);
            }
            return result;
        }

        public async Task<List<User>> GetAll()
        {
            var query = Users.Include(m => m.UserRoles).Include(x=>x.NikanCustomerEquivalent).Where(m => m.UserName.ToLower() != "admin");
            var users = await query.ToListAsync();
            await ConvertUserRoleToRole(users);
            return users;
        }

        public async Task<List<User>> GetUsers()
        {
            var result = await GetUsersInRoleAsync(UserRoleNames.User);
            var users = result as List<User>;
            await ConvertUserRoleToRole(users);
            return users;
        }

        public async Task<User> GetById(long id)
        {
            var user = await Users.FirstOrDefaultAsync(m => m.Id == id);
            return user;
        }

        public async Task<User> GetByPhoneNumber(string mobile)
        {
            var user = await Users.FirstOrDefaultAsync(m => m.PhoneNumber == mobile);
            return user;
        }

        public async Task<User> GetByIdWithRoles(long id)
        {
            var user = await Users.Include(m => m.UserRoles).FirstOrDefaultAsync(m => m.Id == id);
            await ConvertUserRoleToRole(new List<User>() { user });
            return user;
        }

        public async Task ConvertUserRoleToRole(List<User> users)
        {
            foreach (var user in users)
            {
                user.UserRoleNames = await GetRolesAsync(user) as List<string>;
            }
        }

    }
}

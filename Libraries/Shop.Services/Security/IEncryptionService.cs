﻿namespace Shop.Services.Security
{
    public interface IEncryptionService
    {
        string CreatePasswordHash(string password);

        string CreateHash(byte[] data);

        bool VerifyPassword(string password, string hash);
    }
}

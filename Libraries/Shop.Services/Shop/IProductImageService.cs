﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IProductImageService
    {
        IQueryable<ProductImage> GetAllProductImageAsQueryable(long productId);
        Task InsertProductImage(ProductImage entity);
        Task UpdateProductImage(ProductImage entity);
        Task DeleteProductImage(ProductImage entity);
        Task<ProductImage> GetProductImageById(long entityId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Shop.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;

namespace Shop.Services.Shop
{
    public class MessageService : BaseService, IMessageService
    {
        #region Fields

        #endregion

        #region Constructors

        public MessageService(AppDbContext context) :base(context)
        {
        }

        #endregion

        #region Methods

        //این متد به عنوان دیتا سورس گرید کندو استفاده می شود و باید آی کویریبل برگرداند 
        public IQueryable<Message> GetAllMessagesAsQueryable()
        {
            //return Table<Message>().Include(x=>x.Department);
            return Table<Message>();
        }

        public Task DeleteMessage(Message entity)
        {
            Delete<Message>(entity);
            return Save();
        }

        public Task<List<Message>> GetAllMessages()
        {
            return Table<Message>().ToListAsync();
        }

        public Task<Message> GetMessageById(long entityId)
        {
            return GetById<Message>(entityId);
        }

        public Task InsertMessage(Message entity)
        {
            Insert<Message>(entity);
            return Save();
        }

        public Task UpdateMessage(Message entity)
        {
            Update<Message>(entity);
            return Save();
        }

        public async Task ArchiveMessage(long entityId, bool flag)
        {
            var entity = await GetMessageById(entityId);
            entity.IsArchived = flag;
            await UpdateMessage(entity);
            return;
        }

        public async Task MessageIsRead(long entityId)
        {
            var entity = await GetMessageById(entityId);
            if (entity.IsRead == false)
            {
                entity.IsRead = true;
                entity.ReadDate = DateTime.Now;
                await UpdateMessage(entity);
            }
            return;
        }

        public async Task MessageIsReply(long entityId)
        {
            var entity = await GetMessageById(entityId);
            await UpdateMessage(entity);
            return;
        }
        #endregion
    }
}

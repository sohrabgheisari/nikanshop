﻿using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class ProductImageService : BaseService, IProductImageService
    {
        public ProductImageService(AppDbContext context) : base(context)
        {
        }

        public Task DeleteProductImage(ProductImage entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<ProductImage> GetAllProductImageAsQueryable(long productId)
        {
            return Table<ProductImage>().Where(x => x.ProductId == productId);
        }

        public Task<ProductImage> GetProductImageById(long entityId)
        {
            return GetById<ProductImage>(entityId);
        }

        public Task InsertProductImage(ProductImage entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateProductImage(ProductImage entity)
        {
            Update(entity);
            return Save();
        }
    }
}

﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public partial class SettingService : BaseService, ISettingService
    {
        #region Fields
        private string LogoPath = "ShopImage/Logo";
        private readonly IHostingEnvironment _environment;
        private readonly TypeConverter _typeConverter = new TypeConverter();

        #endregion

        #region Ctor

        public SettingService(AppDbContext _context, IHostingEnvironment environment) : base(_context)
        {
            _environment = environment;
        }

        #endregion

        #region Methods

        public Task<Setting> GetByKey(string key, long? sourceId)
        {
            key = NormalizeKey(key);
            return _context.Settings.FirstOrDefaultAsync(x => x.Key == key && x.SourceId == sourceId);
        }

        public async Task<T> GetByKey<T>(string key, long? sourceId, T defaultValue = default(T))
        {
            var setting = await GetByKey(key, sourceId);
            if (setting != null)
            {
                return (T)Convert.ChangeType(setting.Value, typeof(T));
            }

            return defaultValue;
        }

        public T Load<T>(long? sourceId)
            where T : class, new()
        {
            var category = typeof(T).Name + ".";
            category = category.ToLowerInvariant();

            var query = from s in _context.Settings
                        where s.Key.StartsWith(category) &&
                         (s.SourceId == null || s.SourceId == sourceId)
                        orderby s.SourceId == null
                        select s;

            List<Setting> allSettings = query.ToList();
            var entity = ConvertSettingToType<T>(allSettings);
            return entity;
        }

        public async Task SaveAll(Dictionary<object, List<string>> settings, long? sourceId, bool includeKeyNamesInUpdate = true)
        {
            await RunInTransaction(async scope =>
            {
                foreach (var setting in settings.Keys)
                {
                    SaveSync(setting, sourceId, includeKeyNamesInUpdate, settings[setting]?.ToArray());
                }
            });
        }

        public void SaveSync(object setting, long? sourceId, bool includeKeyNamesInUpdate = true, params string[] keySelectors)
        {
            var type = setting.GetType();
            var keyNames = GetKeyNames(setting, keySelectors);
            var settingsToModify = GetSettingsToModify(type, sourceId, includeKeyNamesInUpdate, keyNames);

            foreach (var prop in type.GetProperties())
            {
                // get properties we can read and write to
                if (!prop.CanRead || !prop.CanWrite)
                {
                    continue;
                }

                string key = type.Name + "." + prop.Name;
                key = key.ToLowerInvariant();
                var saveKey = !keyNames.Any();
                if (includeKeyNamesInUpdate)
                {
                    saveKey = saveKey || keyNames.Contains(key);
                }
                else
                {
                    saveKey = saveKey || !keyNames.Contains(key);
                }

                if (saveKey)
                {
                    // Duck typing is not supported in C#. That's why we're using dynamic type
                    dynamic value = prop.GetValue(setting, null);
                    if (value == null)
                    {
                        value = string.Empty;
                    }

                    var keySetting = settingsToModify.FirstOrDefault(x => x.Key == NormalizeKey(key));
                    Set(keySetting, key, value, sourceId);
                }
            }

            _context.SaveChanges();
        }

        public async Task<string> SaveWebsiteLogo(string prevLogoUrl,IFormFile WebsiteSetting_LogoUrl, IFormFile WebsiteSetting_IconUrl)
        {
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, LogoPath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            if (WebsiteSetting_LogoUrl != null && WebsiteSetting_LogoUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(WebsiteSetting_LogoUrl.FileName));
                string newFilename = "logo" + strFileExtension;
                var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await WebsiteSetting_LogoUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                    prevLogoUrl= "/" + LogoPath + "/" + newFilename;
                };
            }
            if (WebsiteSetting_IconUrl != null && WebsiteSetting_IconUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(WebsiteSetting_IconUrl.FileName));
                string newFilename = "favicon" + strFileExtension;
                var FilePath = Path.Combine(_environment.WebRootPath, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await WebsiteSetting_IconUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                };
            }
            return prevLogoUrl;

        }
        #endregion

        #region Utilities

        private List<Setting> GetSettingsToModify(Type type, long? sourceId, bool includeKeyNamesInUpdate, List<string> keyNames)
        {
            var category = (type.Name + ".").ToLowerInvariant();

            var settingsQuery = _context.Settings.Where(m => m.Key.StartsWith(category) && m.SourceId == sourceId);
            if (keyNames.Any())
            {
                if (includeKeyNamesInUpdate)
                {
                    settingsQuery = settingsQuery.Where(m => keyNames.Contains(m.Key));
                }
                else
                {
                    settingsQuery = settingsQuery.Where(m => !keyNames.Contains(m.Key));
                }
            }

            var allSettings = settingsQuery.ToList();
            return allSettings;
        }

        private void Set<T>(Setting setting, string key, T value, long? sourceId)
        {
            var valueStr = value.ToString();

            if (setting != null)
            {
                setting.Value = valueStr;
            }
            else
            {
                setting = new Setting
                {
                    Key = NormalizeKey(key),
                    Value = valueStr,
                    SourceId = sourceId,
                };

                _context.Settings.Add(setting);
            }
        }

        private static T ConvertSettingToType<T>(List<Setting> allSettings)
             where T : class, new()
        {
            var entity = Activator.CreateInstance<T>();
            // if there isn't any by-source settings for specific key, then uses general settings
            var settings = new List<Setting>();
            foreach (var g in allSettings.GroupBy(x => x.Key))
            {
                Setting setting = g.OrderBy(x => x.SourceId == null).FirstOrDefault();
                settings.Add(setting);
            }

            foreach (var prop in typeof(T).GetProperties())
            {
                if (!prop.CanRead || !prop.CanWrite)
                {
                    continue;
                }

                var key = typeof(T).Name + "." + prop.Name;
                var setting = settings.FirstOrDefault(x => x.Key.ToLowerInvariant() == key.ToLowerInvariant());
                if (setting == null)
                {
                    continue;
                }

                var val = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromString(null, new CultureInfo("en"), setting.Value);
                prop.SetValue(entity, val);
            }

            return entity;
        }

        private static List<string> GetKeyNames(object settings, params string[] keySelectors)
        {
            var keyNames = new List<string>();
            if (keySelectors != null && keySelectors.Any())
            {
                foreach (var keySelector in keySelectors)
                {
                    string key = settings.GetType().Name + "." + keySelector;
                    keyNames.Add(key.ToLowerInvariant());
                }
            }

            return keyNames;
        }

        private static string NormalizeKey(string key)
        {
            return key?.Normalize().ToLowerInvariant();
        }

        private void DeleteLogo(string logoPath)
        {
            var mainPath = _environment.WebRootPath + logoPath;
            if (File.Exists(mainPath))
            {
                File.Delete(mainPath);
            }
        } 
        #endregion
    }
}

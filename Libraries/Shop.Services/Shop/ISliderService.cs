﻿using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using Shop.Core.Enums;
using Shop.Services.Dto.Slider;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface ISliderService
    {
        IQueryable<Slider> GetAllSliderAsQueryable(SliderTypes? type);
        Task InsertSlider(Slider entity);
        Task InsertSlider(SliderModel model, IFormFile SliderUrl);
        Task UpdateSlider(SliderModel model, IFormFile SliderUrl);
        Task UpdateSlider(Slider entity);
        Task DeleteSlider(Slider entity);
        Task<Slider> GetSliderById(long entityId);
        IQueryable<Slider> GetSliderByType(SliderTypes type);
    }
}

﻿using Shop.Services.Dto.Getway;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IZarinPalService
    {
        Task<ZarinPalResult> Payment(long orderId, string callBackUrl);
        Task<ZarinPalResult> VerifyPayment(string factorNumber,string status,string authority);
    }
}

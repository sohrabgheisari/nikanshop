﻿using NikanBase.Core.Domain.Shop;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface INikanCustomerEquivalentService
    {
        Task<NikanCustomerEquivalent> GetCustomerEquivalentById(long userId);
        Task InsertCustomerEquivalent(NikanCustomerEquivalent entity);
        Task UpdateCustomerEquivalent(NikanCustomerEquivalent entity);
    }
}

﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;
using Shop.Data;
using Shop.Services.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Shop.Core.Enums;
using Microsoft.AspNetCore.Http;
using Shop.Services.Dto.Product;
using Microsoft.AspNetCore.Hosting;
using Shop.Services.Infrastructure.Mapping;
using System.IO;
using System;
using Shop.Core.Infrastructure;
using EFCore.BulkExtensions;
using Shop.Core.Exceptions;
using NikanBase.Core.Enums;
using Shop.Services.Dto.Property;

namespace Shop.Services.Shop
{
    public class ProductService : BaseService, IProductService
    {
        private readonly IHostingEnvironment _environment;
        private string ImagePath = "ShopImage/Product";
        private readonly IAppContext _appContext;
        public ProductService(AppDbContext context,
            IHostingEnvironment environment,
            IAppContext appContext) : base(context)
        {
            _environment = environment;
            _appContext = appContext;
        }

        public Task DeleteProduct(Product entity)
        {
            Delete<Product>(entity);
            return Save();
        }

        public IQueryable<Product> GetAllProductAsQueryable()
        {
            return Table<Product>().Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.NikanProductEquivalent).OrderByDescending(x => x.InsertDate);
        }

        public IQueryable<Product> GetAllDailyDiscountProductAsQueryable(int skip, int pageCount)
        {
            var query = Table<Product>().Where(x => x.Discount != null && x.Publish == true);
            query = query.Where(x => (x.Discount.DiscountTo.HasValue && x.Discount.DiscountFrom.HasValue && DateTime.Now.Date >= x.Discount.DiscountFrom.Value.Date &&
                      DateTime.Now.Date <= x.Discount.DiscountTo.Value.Date) || (!x.Discount.DiscountTo.HasValue && !x.Discount.DiscountFrom.HasValue) ||
                      (!x.Discount.DiscountTo.HasValue && x.Discount.DiscountFrom.HasValue && DateTime.Now.Date >= x.Discount.DiscountFrom.Value.Date));
            return query.Include(x => x.Category).Include(x => x.Brand)
                .Include(x => x.Discount).Include(x => x.ProductTraits)
                .OrderByDescending(x => x.ProductTraits.Count).ThenByDescending(x => x.Inventory).Skip(skip).Take(pageCount);
        }
        public Task<int> GetAllDailyDiscountProductCount()
        {
            var query = Table<Product>().Where(x => x.Discount != null && x.Publish == true);
            query = query.Where(x => (x.Discount.DiscountTo.HasValue && x.Discount.DiscountFrom.HasValue && DateTime.Now.Date >= x.Discount.DiscountFrom.Value.Date &&
                      DateTime.Now.Date <= x.Discount.DiscountTo.Value.Date) || (!x.Discount.DiscountTo.HasValue && !x.Discount.DiscountFrom.HasValue) ||
                      (!x.Discount.DiscountTo.HasValue && x.Discount.DiscountFrom.HasValue && DateTime.Now.Date >= x.Discount.DiscountFrom.Value.Date));
            return query.CountAsync();
        }

        public IQueryable<Product> GetAllProductAsQueryable(long categoryId)
        {
            return Table<Product>().Where(x => x.CategoryId == categoryId && x.Publish == true)
                .Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits);
        }
        public IQueryable<Product> GetAllProductAsQueryable(long categoryId, int skip, int pageCount, SortTypes sort, List<long> brands, List<long> colors, List<long> warranties, List<PropertyFilter> properties, bool available, decimal? minPrice, decimal? maxPrice)
        {
            var query = Table<Product>().Where(x => x.CategoryId == categoryId && x.Publish == true);
            if (brands.Any())
            {
                query = query.Where(x => brands.Contains(x.BrandId.Value));
            }
            if (colors.Any())
            {
                var colorTraitsProductIds = Table<ProductTrait>().Where(x => colors.Contains(x.ColorTriatId.Value)).Select(x => x.ProductId).ToList();
                query = query.Where(x => colorTraitsProductIds.Contains(x.Id));
            }
            if (warranties.Any())
            {
                var warrantyTraitsProductIds = Table<ProductTrait>().Where(x => warranties.Contains(x.WarrantyTriatId.Value)).Select(x => x.ProductId).ToList();
                query = query.Where(x => warrantyTraitsProductIds.Contains(x.Id));
            }
            if (properties.Any())
            {
                var groups = properties.GroupBy(x => x.Id).Select(x => new ProductFilterGroups()
                {
                    Id = x.Key,
                    Values = properties.Where(c => c.Id == x.Key).Select(c => c.Value).ToList()
                }).ToList();
                foreach (var item in groups)
                {
                    List<long> ids = new List<long>();
                    ids = Table<ProductProperty>().Where(x => x.PropertyId == item.Id && item.Values.Contains(x.Value)).Select(x => x.ProductId).ToList();
                    query = query.Where(x => ids.Contains(x.Id));
                }
            }
            if (available)
            {
                query = query.Where(x => x.ProductTraits.Count > 0 || x.Inventory > 0);
            }
            if (minPrice.HasValue)
            {
                query = query.Where(x => x.Price >= minPrice.Value);
            }
            if (maxPrice.HasValue)
            {
                query = query.Where(x => x.Price <= maxPrice.Value);
            }
            if (sort == SortTypes.MostVisited)
            {
                return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                    .OrderByDescending(x => x.ProductVisits.Count).Skip(skip).Take(pageCount);
            }
            else if (sort == SortTypes.Popular)
            {
                return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                      .OrderByDescending(x => x.Favorites.Count).Skip(skip).Take(pageCount);
            }
            else if (sort == SortTypes.Newest)
            {
                return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                    .OrderByDescending(x => x.InsertDate).Skip(skip).Take(pageCount);
            }
            else if (sort == SortTypes.Cheapest)
            {
                return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                    .OrderBy(x => x.Price).Skip(skip).Take(pageCount);
            }
            else if (sort == SortTypes.Expensive)
            {
                return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                    .OrderByDescending(x => x.Price).Skip(skip).Take(pageCount);
            }
            else if (sort == SortTypes.BestSelling)
            {

                var paidOrderIds = Table<Order>().Where(x => x.Status == StatusTypes.SuccessfulPayment).Select(x => x.Id);
                var orderItems = Table<OrderItem>().Where(x => paidOrderIds.Contains(x.OrderId) && x.Product.CategoryId == categoryId).ToList();
                var productGroups = orderItems.GroupBy(x => x.ProductId)
                    .Select(x => new BestSellerModel
                    {
                        ProductId = x.Key,
                        Count = orderItems.Where(c => c.ProductId == x.Key).Sum(c => c.Count)
                    }).ToList();
                productGroups = productGroups.OrderByDescending(c => c.Count).ToList();
                var ids = productGroups.Select(x => x.ProductId).ToList();
                var result = new List<Product>();
                var list = query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits).ToList();
                foreach (var id in ids)
                {
                    var prd = list.FirstOrDefault(x => x.Id == id);
                    if (prd != null)
                    {
                        result.Add(prd);
                    }
                }
                result.AddRange(list.Where(x => !ids.Contains(x.Id)).ToList());
                return result.AsQueryable().Skip(skip).Take(pageCount);

            }
            return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits)
                .OrderByDescending(x => x.ProductTraits.Count).ThenByDescending(x => x.Inventory).Skip(skip).Take(pageCount);
        }

        public IQueryable<Product> GetAllBrandProductAsQueryable(long brandId)
        {
            return Table<Product>().Where(x => x.BrandId.Value == brandId && x.Publish == true)
                .Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).Include(x => x.ProductTraits);
        }

        public IQueryable<Product> GetAllProductAsQueryable(List<long> categoryIds)
        {
            return Table<Product>().Where(x => categoryIds.Contains(x.CategoryId) && x.Publish == true).Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount);

        }

        public IQueryable<Product> GetAllProductAsQueryable(ResourceTypes type)
        {
            var query = Table<Product>().Where(x => x.Publish == true);
            if (type == ResourceTypes.New)
            {
                query = query.Where(x => x.NewProduct == true);
            }
            else if (type == ResourceTypes.Special)
            {
                query = query.Where(x => x.SpecialProduct == true);
            }
            else if (type == ResourceTypes.BestSellersCount)
            {
                var paidOrderIds = Table<Order>().Where(x => x.Status == StatusTypes.SuccessfulPayment).Select(x => x.Id);
                var orderItems = Table<OrderItem>().Where(x => paidOrderIds.Contains(x.OrderId));
                var productGroups = orderItems.GroupBy(x => x.ProductId)
                    .Select(x => new BestSellerModel
                    {
                        ProductId = x.Key,
                        Count = orderItems.Where(c => c.ProductId == x.Key).Sum(c => c.Count)
                    });
                query = query.Where(x => productGroups.OrderByDescending(c => c.Count).Select(c => c.ProductId).Contains(x.Id));
            }
            else if (type == ResourceTypes.BestSellersPrice)
            {
                var paidOrderIds = Table<Order>().Where(x => x.Status == StatusTypes.SuccessfulPayment).Select(x => x.Id);
                var orderItems = Table<OrderItem>().Where(x => paidOrderIds.Contains(x.OrderId));
                var productGroups = orderItems.GroupBy(x => x.ProductId)
                    .Select(x => new BestSellerModel
                    {
                        ProductId = x.Key,
                        Price = orderItems.Where(c => c.ProductId == x.Key).Sum(c => c.PriceWithDiscount)
                    });
                query = query.Where(x => productGroups.OrderByDescending(c => c.Price).Select(c => c.ProductId).Contains(x.Id));
            }
            else if (type == ResourceTypes.LastVisit)
            {
                var visitIds = Table<ProductVisit>().Where(x => x.UserId == _appContext.User.Id).OrderByDescending(x => x.LastVisitDate).Select(x => x.ProductId);
                query = query.Where(x => visitIds.Contains(x.Id));
            }
            return query.Include(x => x.Category).Include(x => x.Brand).Include(x => x.Discount).OrderByDescending(x => x.InsertDate).Take(20);
        }

        public IQueryable<Product> GetAllOfferAsQueryableById(long productId)
        {
            var product = Table<Product>().FirstOrDefault(x => x.Id == productId);
            var productIds = Table<Product>().Where(x => x.CategoryId == product.CategoryId && x.Id != product.Id).Select(x => x.Id);
            var similarIds = Table<SimilarProduct>().Where(x => x.ProductId == product.Id).Select(x => x.SimilarProductId);
            var ids = productIds.Concat(similarIds).Distinct();
            var matchIds = Table<Order>().Where(x => x.Status == StatusTypes.SuccessfulPayment).SelectMany(x => x.OrderItems)
                .Where(x => ids.Contains(x.ProductId)).Select(x => x.ProductId).Distinct();

            return Table<Product>().Where(x => x.Publish == true && matchIds.Contains(x.Id))
                .Include(x => x.Category).Include(x => x.Discount).OrderByDescending(x => x.InsertDate).Take(20);
        }
        public IQueryable<Product> GetAllSimilarProductAsQueryable(long productId)
        {
            var query = Table<Product>().Where(x => x.Publish == true);
            var similarIds = Table<SimilarProduct>().Where(x => x.ProductId == productId).Select(x => x.SimilarProductId);
            query = query.Where(x => similarIds.Contains(x.Id));
            return query.Include(x => x.Category).Include(x => x.Discount).OrderByDescending(x => x.InsertDate).Take(20);
        }
        public Task<Product> GetProductById(long entityId)
        {
            return Table<Product>().Where(x => x.Id == entityId).Include(x => x.Category).Include(x => x.Discount)
                .Include(x => x.ProductImages).Include(x => x.ProductProperties).ThenInclude(x => x.Property)
                .ThenInclude(x => x.PropertyCategory).Include(x => x.SimilarProduct_Product).ThenInclude(x => x.ProductSimilar)
                .Include(x => x.ProductTraits).ThenInclude(x => x.ColorTrait)
                .Include(x => x.ProductTraits).ThenInclude(x => x.WarrantyTrait)
                .Include(x => x.OrderItems)
                .Include(x => x.NikanProductEquivalent)
                .Include(x => x.Brand)
                .FirstOrDefaultAsync();
        }

        public Task InsertProduct(Product entity)
        {
            Insert(entity);
            return Save();
        }

        public async Task InsertProduct(ProductModel model, IFormFile ImageUrl)
        {
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                }
            }
            async Task Act()
            {
                var entity = Mapping.Mapper.Map<Product>(model);
                entity.InsertDate = DateTime.Now;
                _context.Add(entity);
                _context.SaveChanges();
                if (!String.IsNullOrEmpty(model.BarCode))
                {
                    if (model.Traits != null && model.Traits.Any())
                    {
                        model.Traits.ForEach(x =>
                        {
                            if (x.BarCode == "null")
                            {
                                x.BarCode = "";
                            }
                        });
                        List<string> barCodes = model.Traits.Where(x => String.IsNullOrEmpty(x.BarCode) == false).Select(x => x.BarCode).ToList();
                        if (barCodes.Any())
                        {
                            throw new NikanException("بارکدهای مشخصه محصول باید خالی شوند");
                        }
                        var prd = Table<Product>().Where(x => x.BarCode == model.BarCode);
                        var prdTrait = Table<ProductTrait>().Where(x => x.BarCode == model.BarCode);
                        if (prd.Any() || prdTrait.Any())
                        {
                            throw new NikanException("بارکد محصول تکراری است");
                        }
                    }
                }
                if (model.Properties != null && model.Properties.Any())
                {
                    List<ProductProperty> productProperties = model.Properties.Select(x => new ProductProperty()
                    {
                        Id = 0,
                        Order = x.Order,
                        ProductId = entity.Id,
                        PropertyId = x.PropertyId,
                        Value = x.Value,
                        Visible = x.Visible

                    }).ToList();
                    _context.AddRange(productProperties);
                    _context.SaveChanges();
                }
                if (model.Traits != null && model.Traits.Any())
                {
                    model.Traits.ForEach(x =>
                    {
                        if (x.BarCode == "null")
                        {
                            x.BarCode = "";
                        }
                    });
                    List<ProductTrait> productTraits = model.Traits.Select(x => new ProductTrait()
                    {
                        Id = 0,
                        ProductId = entity.Id,
                        ColorTriatId = x.ColorTriatId,
                        PriceIncrease = x.PriceIncrease,
                        Inventory = x.TraitInventory,
                        WarrantyTriatId = x.WarrantyTriatId,
                        WarrantyDate = x.WarrantyDate.ToGregorianDateTime(),
                        IsActive = x.IsActive,
                        MinimumCart = x.MinimumCart,
                        MaximumCart = x.MaximumCart,
                        BarCode = x.BarCode

                    }).ToList();
                    List<string> barCodes = model.Traits.Where(x => String.IsNullOrEmpty(x.BarCode) == false).Select(x => x.BarCode).ToList();
                    var duplicateItems = barCodes.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key);
                    if (duplicateItems.Any())
                    {
                        throw new NikanException("در مشخصه های محصول بارکد تکراری وجود دارد");
                    }
                    var traits = Table<ProductTrait>().Where(x => barCodes.Contains(x.BarCode));
                    List<string> dublicatedBarcodes = new List<string>();
                    if (traits.Any())
                    {
                        dublicatedBarcodes.AddRange(traits.Select(x => x.BarCode).Distinct().ToList());
                    }
                    var prds = Table<Product>().Where(x => barCodes.Contains(x.BarCode));
                    if (prds.Any())
                    {
                        dublicatedBarcodes.AddRange(prds.Select(x => x.BarCode).Distinct().ToList());
                    }
                    if (dublicatedBarcodes.Any())
                    {
                        string barCodeErrorMessage = "بارکدهای زیر تکراری هستند:" + "</br>";
                        foreach (var code in dublicatedBarcodes)
                        {
                            barCodeErrorMessage = barCodeErrorMessage + code + "</br>";
                        }
                        throw new NikanException(barCodeErrorMessage);
                    }
                    _context.AddRange(productTraits);
                    _context.SaveChanges();
                }
                if (model.Similars != null && model.Similars.Any())
                {
                    List<SimilarProduct> similarProducts = model.Similars.Select(x => new SimilarProduct()
                    {
                        Id = 0,
                        ProductId = entity.Id,
                        SimilarProductId = x.SimilarProductId

                    }).ToList();
                    _context.AddRange(similarProducts);
                    _context.SaveChanges();
                }
                if (ImageUrl != null && ImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        entity.ImageUrl = "/" + ImagePath + "/" + newFilename;
                    };
                    _context.Update(entity);
                    _context.SaveChanges();
                }
            }
        }

        public Task UpdateProduct(Product entity)
        {
            Update(entity);
            return Save();
        }

        public async Task UpdateProduct(ProductModel model, IFormFile ImageUrl)
        {
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                }
            }
            async Task Act()
            {
                var mainProduct = _context.Set<Product>().AsNoTracking().FirstOrDefault(x => x.Id == model.Id);
                var prevTraits = Table<ProductTrait>().Where(x => x.ProductId == model.Id);
                var entity = Mapping.Mapper.Map<Product>(model);
                if (!String.IsNullOrEmpty(model.BarCode))
                {
                    if (model.Traits != null && model.Traits.Any())
                    {
                        model.Traits.ForEach(x =>
                        {
                            if (x.BarCode == "null")
                            {
                                x.BarCode = "";
                            }
                        });
                        List<string> barCodes = model.Traits.Where(x => String.IsNullOrEmpty(x.BarCode) == false).Select(x => x.BarCode).ToList();
                        if (barCodes.Any())
                        {
                            throw new NikanException("بارکدهای مشخصه محصول باید خالی شوند");
                        }
                    }
                    var prd = Table<Product>().Where(x => x.BarCode == model.BarCode && x.Id != mainProduct.Id);
                    var prdTrait = Table<ProductTrait>().Where(x => x.BarCode == model.BarCode && x.Id != mainProduct.Id);
                    if (prd.Any() || prdTrait.Any())
                    {
                        throw new NikanException("بارکد محصول تکراری است");
                    }
                }
                var prevProperties = Table<ProductProperty>().Where(x => x.ProductId == model.Id);
                if (prevProperties.Any())
                {
                    _context.RemoveRange(prevProperties);
                    _context.SaveChanges();
                }

                if (model.Traits != null && model.Traits.Any())
                {
                    var deletedProductTraits = prevTraits.Where(x => !model.Traits.Select(c => c.Id).Contains(x.Id));
                    _context.RemoveRange(deletedProductTraits);
                    _context.SaveChanges();
                    model.Traits.ForEach(x =>
                    {
                        if (x.BarCode == "null")
                        {
                            x.BarCode = "";
                        }
                    });
                    List<ProductTrait> addedProductTraits = model.Traits.Where(x => x.Id <= 0).Select(x => new ProductTrait()
                    {
                        Id = 0,
                        ProductId = entity.Id,
                        ColorTriatId = x.ColorTriatId,
                        PriceIncrease = x.PriceIncrease,
                        Inventory = x.TraitInventory,
                        WarrantyTriatId = x.WarrantyTriatId,
                        WarrantyDate = x.WarrantyDate.ToGregorianDateTime(),
                        IsActive = x.IsActive,
                        MinimumCart = x.MinimumCart,
                        MaximumCart = x.MaximumCart,
                        BarCode = x.BarCode

                    }).ToList();
                    List<ProductTrait> updatedProductTraits = model.Traits.Where(x => x.Id > 0).Select(x => new ProductTrait()
                    {
                        Id = x.Id,
                        ProductId = entity.Id,
                        ColorTriatId = x.ColorTriatId,
                        PriceIncrease = x.PriceIncrease,
                        Inventory = x.TraitInventory,
                        WarrantyTriatId = x.WarrantyTriatId,
                        WarrantyDate = x.WarrantyDate.ToGregorianDateTime(),
                        IsActive = x.IsActive,
                        MinimumCart = x.MinimumCart,
                        MaximumCart = x.MaximumCart,
                        BarCode = x.BarCode
                    }).ToList();
                    List<string> barCodes = addedProductTraits.Concat(updatedProductTraits).Where(x => !String.IsNullOrEmpty(x.BarCode) == true).Select(x => x.BarCode).ToList();
                    var duplicateItems = barCodes.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key);
                    if (duplicateItems.Any())
                    {
                        throw new NikanException("در مشخصه های محصول بارکد تکراری وجود دارد");
                    }
                    var traits = Table<ProductTrait>().Where(x => barCodes.Contains(x.BarCode) && x.ProductId != mainProduct.Id);
                    List<string> dublicatedBarcodes = new List<string>();
                    if (traits.Any())
                    {
                        dublicatedBarcodes.AddRange(traits.Select(x => x.BarCode).Distinct().ToList());
                    }
                    var prds = Table<Product>().Where(x => x.Id != mainProduct.Id && barCodes.Contains(x.BarCode));
                    if (prds.Any())
                    {
                        dublicatedBarcodes.AddRange(prds.Select(x => x.BarCode).Distinct().ToList());
                    }
                    //چک کردن بارکد محصول
                    if (dublicatedBarcodes.Any())
                    {
                        string barCodeErrorMessage = "بارکدهای زیر تکراری هستند:" + "</br>";
                        foreach (var code in dublicatedBarcodes)
                        {
                            barCodeErrorMessage = barCodeErrorMessage + code + "</br>";
                        }
                        throw new NikanException(barCodeErrorMessage);
                    }
                    //List<ProductTrait> productTraits = model.Traits.Select(x => new ProductTrait()
                    //{
                    //    Id = 0,
                    //    ProductId = entity.Id,
                    //    ColorTriatId = x.ColorTriatId,
                    //    PriceIncrease = x.PriceIncrease,
                    //    Inventory = x.TraitInventory,
                    //    WarrantyTriatId = x.WarrantyTriatId,
                    //    WarrantyDate = x.WarrantyDate.ToGregorianDateTime(),
                    //    IsActive = x.IsActive,
                    //    MinimumCart = x.MinimumCart,
                    //    MaximumCart = x.MaximumCart

                    //}).ToList();
                    _context.AddRange(addedProductTraits);
                    _context.UpdateRange(updatedProductTraits);
                    //_context.RemoveRange(deletedProductTraits);
                    _context.SaveChanges();
                }
                else
                {
                    if (prevTraits.Any())
                    {
                        _context.RemoveRange(prevTraits);
                        _context.SaveChanges();
                    }
                }
                var prevSimilars = Table<SimilarProduct>().Where(x => x.ProductId == model.Id);
                if (prevSimilars.Any())
                {
                    _context.RemoveRange(prevSimilars);
                    _context.SaveChanges();
                }
                if (model.Properties != null && model.Properties.Any())
                {
                    List<ProductProperty> productProperties = model.Properties.Select(x => new ProductProperty()
                    {
                        Id = 0,
                        Order = x.Order,
                        ProductId = entity.Id,
                        PropertyId = x.PropertyId,
                        Value = x.Value,
                        Visible = x.Visible

                    }).ToList();
                    _context.AddRange(productProperties);
                    _context.SaveChanges();
                }

                if (model.Similars != null && model.Similars.Any())
                {
                    List<SimilarProduct> similarProducts = model.Similars.Select(x => new SimilarProduct()
                    {
                        Id = 0,
                        ProductId = entity.Id,
                        SimilarProductId = x.SimilarProductId

                    }).ToList();
                    _context.AddRange(similarProducts);
                    _context.SaveChanges();
                }
                if (ImageUrl != null && ImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        entity.ImageUrl = "/" + ImagePath + "/" + newFilename;
                        DeleteImage(mainProduct);
                    };
                }
                else
                {
                    entity.ImageUrl = mainProduct.ImageUrl;
                }
                entity.InsertDate = mainProduct.InsertDate;
                _context.Update(entity);
                _context.SaveChanges();
            }
        }
        public async Task GroupAllocation(GroupAllocationPropertyModel model)
        {
            List<ProductProperty> updateList = new List<ProductProperty>();
            List<ProductProperty> insertList = new List<ProductProperty>();
            foreach (var item in model.GroupProperties)
            {
                var productIds = Table<Product>().Where(x => x.CategoryId == item.ProductCategoryId).Select(x => x.Id);
                foreach (var id in productIds)
                {
                    var productProperty = _context.Set<ProductProperty>().FirstOrDefault(x => x.PropertyId == item.PropertyId && x.ProductId == id);
                    if (productProperty != null)
                    {
                        productProperty.Order = item.Order;
                        productProperty.Visible = item.Visible;
                        productProperty.Value = item.Value;
                        updateList.Add(productProperty);
                    }
                    else
                    {
                        insertList.Add(new ProductProperty()
                        {
                            Value = item.Value,
                            Visible = item.Visible,
                            Order = item.Order,
                            ProductId = id,
                            PropertyId = item.PropertyId
                        });
                    }
                }
            }
            if (updateList.Any())
            {
                _context.BulkUpdate(updateList);
            }
            if (insertList.Any())
            {
                _context.BulkInsert(insertList);
            }
        }
        public async Task<NikanProductEquivalent> GetProductEquivalentById(long productId)
        {
            return Table<NikanProductEquivalent>().FirstOrDefault(x => x.ShopProductId == productId);
        }

        public Task InsertProductEquivalent(NikanProductEquivalent entity)
        {
            var eq = Table<NikanProductEquivalent>().FirstOrDefault(x => x.ShopProductId == entity.ShopProductId || x.NikanProductId == entity.NikanProductId);
            if (eq == null)
            {
                Insert(entity);
                return Save();
            }
            throw new NikanException("معال سازی این محصول قبلا انجام گردیده است");
        }

        public Task UpdateProductEquivalent(NikanProductEquivalent entity)
        {
            var eq = Table<NikanProductEquivalent>().FirstOrDefault(x => x.Id != entity.Id && (x.ShopProductId == entity.ShopProductId || x.NikanProductId == entity.NikanProductId));
            if (eq == null)
            {
                Update(entity);
                return Save();
            }
            throw new NikanException("معال سازی این محصول قبلا انجام گردیده است");
        }
        public Task<int> GetAllProductInCategoryCount(long categoryId, List<long> brands, List<long> colors, List<long> warranties, List<PropertyFilter> properties, bool available, decimal? minPrice, decimal? maxPrice)
        {
            var query = Table<Product>().Where(x => x.CategoryId == categoryId && x.Publish == true);
            if (brands.Any())
            {
                query = query.Where(x => brands.Contains(x.BrandId.Value));
            }
            if (colors.Any())
            {
                var colorTraitsProductIds = Table<ProductTrait>().Where(x => colors.Contains(x.ColorTriatId.Value)).Select(x => x.ProductId).ToList();
                query = query.Where(x => colorTraitsProductIds.Contains(x.Id));
            }
            if (warranties.Any())
            {
                var warrantyTraitsProductIds = Table<ProductTrait>().Where(x => warranties.Contains(x.WarrantyTriatId.Value)).Select(x => x.ProductId).ToList();
                query = query.Where(x => warrantyTraitsProductIds.Contains(x.Id));
            }
            if (properties.Any())
            {
                var groups = properties.GroupBy(x => x.Id).Select(x => new ProductFilterGroups()
                {
                    Id = x.Key,
                    Values = properties.Where(c => c.Id == x.Key).Select(c => c.Value).ToList()
                }).ToList();
                foreach (var item in groups)
                {
                    List<long> ids = new List<long>();
                    ids = Table<ProductProperty>().Where(x => x.PropertyId == item.Id && item.Values.Contains(x.Value)).Select(x => x.ProductId).ToList();
                    query = query.Where(x => ids.Contains(x.Id));
                }
            }
            if (available)
            {
                query = query.Where(x => x.ProductTraits.Count > 0 || x.Inventory > 0);
            }
            if (minPrice.HasValue)
            {
                query = query.Where(x => x.Price >= minPrice.Value);
            }
            if (maxPrice.HasValue)
            {
                query = query.Where(x => x.Price <= maxPrice.Value);
            }
            return query.CountAsync();
        }

        #region Utilities
        private void DeleteImage(Product entity)
        {
            var mainPath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
            var mobilePath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mobilePath))
            {
                System.IO.File.Delete(mobilePath);
            }
        }
        #endregion
    }
}

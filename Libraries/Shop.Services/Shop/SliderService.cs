﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using Shop.Core.Enums;
using Shop.Core.Exceptions;
using Shop.Core.Infrastructure;
using Shop.Data;
using Shop.Services.Dto.Slider;
using Shop.Services.Infrastructure;
using Shop.Services.Infrastructure.Mapping;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class SliderService : BaseService, ISliderService
    {
        private string ImagePath = "ShopImage/Slider";
        private readonly IHostingEnvironment _environment;

        public SliderService(AppDbContext context,
            IHostingEnvironment environment) : base(context)
        {
            _environment = environment;
        }

        public Task DeleteSlider(Slider entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<Slider> GetAllSliderAsQueryable(SliderTypes? type)
        {
            var query = Table<Slider>();
            if (type.HasValue)
            {
                query = query.Where(x => x.Type == type.Value);
            }
            return query.OrderBy(x => x.Order);
        }

        public Task<Slider> GetSliderById(long entityId)
        {
            return GetById<Slider>(entityId);
        }

        public Task InsertSlider(Slider entity)
        {
            Insert(entity);
            return Save();
        }

        public async Task InsertSlider(SliderModel model, IFormFile SliderUrl)
        {
            var slider = Table<Slider>().FirstOrDefault(x => x.Type == model.Type);
            if (slider != null)
            {
                throw new NikanException($"اسلایدر {model.Type.EnumToDescription()} قبلا تعریف شده است");
            }
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            if (SliderUrl != null && SliderUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(SliderUrl.FileName));
                string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await SliderUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                    model.SliderUrl = "/" + ImagePath + "/" + newFilename;
                };
            }
            else
            {
                throw new NikanException("تصویر اصلی الزامی است");
            }
            var entity = Mapping.Mapper.Map<Slider>(model);
            await InsertSlider(entity);
        }

        public async Task UpdateSlider(SliderModel model, IFormFile SliderUrl)
        {
            var slider = Table<Slider>().FirstOrDefault(x => x.Id != model.Id && x.Type == model.Type);
            if (slider != null)
            {
                throw new NikanException($"اسلایدر {model.Type.EnumToDescription()} قبلا تعریف شده است");
            }
            var entity = await GetSliderById(model.Id);
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            if (SliderUrl != null && SliderUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(SliderUrl.FileName));
                string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await SliderUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                    model.SliderUrl = "/" + ImagePath + "/" + newFilename;
                    DeleteImage(entity);
                };
            }
            else
            {
                model.SliderUrl = entity.SliderUrl;
            }
            var entity_Updated = Mapping.Mapper.Map<Slider>(model);
            await UpdateSlider(entity_Updated);
        }

        public Task UpdateSlider(Slider entity)
        {
            Update(entity);
            return Save();
        }

        #region Utilities
        private void DeleteImage(Slider entity)
        {
            var mainPath = _environment.WebRootPath + entity.SliderUrl;
            if (File.Exists(mainPath))
            {
                File.Delete(mainPath);
            }
        }

        public IQueryable<Slider> GetSliderByType(SliderTypes type)
        {
            var query = Table<Slider>();
            if (type == SliderTypes.TwoSlideUp || type == SliderTypes.TwoSlideDown)
            {
                return query.Where(x => x.Type == SliderTypes.TwoSlideDown || x.Type == SliderTypes.TwoSlideUp);
            }
            else if (type == SliderTypes.TwoSlideLeft || type == SliderTypes.TwoSlideRight)
            {
                return query.Where(x => x.Type == SliderTypes.TwoSlideLeft || x.Type == SliderTypes.TwoSlideRight);
            }
            else if (type == SliderTypes.AdvertisingSlider)
            {
                return query.Where(x => x.Type == SliderTypes.AdvertisingSlider);
            }
            return query;
        }
        #endregion
    }
}

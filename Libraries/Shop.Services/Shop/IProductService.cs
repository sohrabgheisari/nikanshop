﻿using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Services.Dto.Product;
using Shop.Services.Dto.Property;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IProductService
    {
        IQueryable<Product> GetAllSimilarProductAsQueryable(long productId);
        IQueryable<Product> GetAllProductAsQueryable(ResourceTypes type);
        IQueryable<Product> GetAllOfferAsQueryableById(long productId);
        IQueryable<Product> GetAllProductAsQueryable();
        IQueryable<Product> GetAllDailyDiscountProductAsQueryable(int skip, int pageCount);
        Task<int> GetAllDailyDiscountProductCount();
        IQueryable<Product> GetAllProductAsQueryable(long categoryId);
        IQueryable<Product> GetAllProductAsQueryable(long categoryId,int skip,int pageCount, SortTypes sort,List<long> brands, List<long> colors, List<long> warranties, List<PropertyFilter> properties,bool available, decimal? minPrice, decimal? maxPrice);
        IQueryable<Product> GetAllBrandProductAsQueryable(long brandId);
        IQueryable<Product> GetAllProductAsQueryable(List<long> categoryIds);
        Task<int> GetAllProductInCategoryCount(long categoryId, List<long> brands, List<long> colors, List<long> warranties, List<PropertyFilter> properties, bool available,decimal?minPrice,decimal?maxPrice);
        Task InsertProduct(Product entity);
        Task InsertProduct(ProductModel model,IFormFile ImageUrl);
        Task DeleteProduct(Product entity);
        Task UpdateProduct(Product entity);
        Task UpdateProduct(ProductModel model, IFormFile ImageUrl);
        Task GroupAllocation(GroupAllocationPropertyModel model);
        Task<Product> GetProductById(long entityId);
        Task<NikanProductEquivalent> GetProductEquivalentById(long productId);
        Task InsertProductEquivalent(NikanProductEquivalent entity);
        Task UpdateProductEquivalent(NikanProductEquivalent entity);
    }
}

﻿using Microsoft.EntityFrameworkCore;
using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class GalleryService : BaseService, IGalleryService
    {
        #region Constructor
        public GalleryService(AppDbContext context) : base(context)
        {
        }

        #endregion

        #region Methode
        public Task DeleteGallery(Gallery entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<Gallery> GetAllGalleries()
        {
            return Table<Gallery>().OrderBy(x => x.DisplayOrder).Include(x=>x.Product);
        }

        public Task<Gallery> GetGalleryById(long entityId)
        {
            return GetById<Gallery>(entityId);
        }

        public Task InsertGallery(Gallery entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateGallery(Gallery entity)
        {
            Update(entity);
            return Save();
        }

        #endregion
    }
}

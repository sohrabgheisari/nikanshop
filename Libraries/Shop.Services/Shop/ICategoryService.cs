﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface ICategoryService
    {
        IQueryable<Category> GetMenuCategoryAsQueryable();
        IQueryable<Category> GetAllCategoryAsQueryable(int level);
        IQueryable<Category> GetAllCategoryAsQueryable();
        IQueryable<Category> GetAllRootCategoryAsQueryable();
        IQueryable<Category> GetAllCategoryChildsAsQueryable(long parentId);
        Task<bool> CategoryHasChild(long catId);
        Task<int> GetLevel(long parentId);
        Task<string> GetFullPathName(long catId);
        Task InsertCategory(Category entity);
        Task UpdateCategory(Category entity);
        Task DeleteCategory(Category entity);
        Task<Category> GetCategoryById(long entityId);
    }
}

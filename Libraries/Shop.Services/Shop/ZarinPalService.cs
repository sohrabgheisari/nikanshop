﻿using Dto.Payment;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Data;
using Shop.Services.Dto.Getway;
using Shop.Services.Dto.Setting;
using Shop.Services.Infrastructure;
using Shop.Services.Users;
using System;
using System.Threading.Tasks;
using ZarinPal.Class;

namespace Shop.Services.Shop
{
    public class ZarinPalService : BaseService, IZarinPalService
    {
        private readonly IOrderService _orderService;
        private readonly IAppContext _appContext;
        private readonly UserManager _userManager;
        private readonly ISettingService _settingService;
        private readonly Payment _payment;
        private readonly IHttpContextAccessor _httpContext;
        private readonly ILogger _logger;

        public ZarinPalService(AppDbContext context, IOrderService orderService,
            IAppContext appContext, UserManager userManager,
             ISettingService settingService, IHttpContextAccessor httpContext,
             ILogger<ZarinPalService> logger) : base(context)
        {
            _orderService = orderService;
            _appContext = appContext;
            _userManager = userManager;
            _settingService = settingService;
            _payment = new Payment();
            _httpContext = httpContext;
            _logger = logger;
        }

        public async Task<ZarinPalResult> Payment(long orderId, string callBackUrl)
        {
            ZarinPalResult result = new ZarinPalResult() { IsSuccess = false };
            var order = await _orderService.GetOrderById(orderId);
            if (order == null)
            {
                result.IsSuccess = false;
                result.StatusCode = 504;
                result.Message = ZarinPalMessageDictionary.GetError(504);
                return result;
            }
            if (order.UserId != _appContext.User.Id)
            {
                result.IsSuccess = false;
                result.StatusCode = 802;
                result.Message = ZarinPalMessageDictionary.GetError(504);
                return result;
            }
            if (order.Status == StatusTypes.SuccessfulPayment)
            {
                result.IsSuccess = false;
                result.StatusCode = 505;
                result.Message = ZarinPalMessageDictionary.GetError(505);
                return result;
            }
            var user = await _userManager.GetById(order.UserId);
            var setting = _settingService.Load<GetwaySettingsModel>(null);
            if (setting != null)
            {
                if (setting.BankGateway_Type != ZarinPalTypes.ZarinPal && setting.BankGateway_Type != ZarinPalTypes.ZarinPalSandBox)
                {
                    result.IsSuccess = false;
                    result.StatusCode = 800;
                    result.Message = ZarinPalMessageDictionary.GetError(800);
                    return result;
                }
                else
                {
                    if (setting.BankGateway_Type == ZarinPalTypes.ZarinPal && String.IsNullOrEmpty(setting.BankGateway_TerminalNo))
                    {
                        result.IsSuccess = false;
                        result.StatusCode = 801;
                        result.Message = ZarinPalMessageDictionary.GetError(801);
                        return result;
                    }
                    else
                    {
                        var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
                        var res = await _payment.Request(new DtoRequestWithExtra()
                        {
                            Mobile = user.PhoneNumber,
                            CallbackUrl = callBackUrl,
                            Description = $"پرداخت فاکتور {order.Number}",
                            Email = user.Email,
                            Amount =priceUnitSetting!=null?(priceUnitSetting.Unit==PriceUnitTypes.Tooman ? (int)order.PayableAmount : (int)order.PayableAmount/10) : ((int)order.PayableAmount/10),
                            MerchantId = setting.BankGateway_Type == ZarinPalTypes.ZarinPal ? setting.BankGateway_TerminalNo : "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                        }, setting.BankGateway_Type == ZarinPalTypes.ZarinPalSandBox ? ZarinPal.Class.Payment.Mode.sandbox : ZarinPal.Class.Payment.Mode.zarinpal);
                        if (res.Status == 100)
                        {
                            if (setting.BankGateway_Type == ZarinPalTypes.ZarinPalSandBox)
                            {
                                //return Redirect($"https://sandbox.zarinpal.com/pg/StartPay/{res.Authority}");
                                result.IsSuccess = true;
                                result.StatusCode = 200;
                                result.Message = "";
                                result.RedirectUrl = $"https://sandbox.zarinpal.com/pg/StartPay/{res.Authority}";
                                return result;
                            }
                            else
                            {
                                //return Redirect($"https://zarinpal.com/pg/StartPay/{res.Authority}");
                                result.IsSuccess = true;
                                result.StatusCode = 200;
                                result.Message = "";
                                result.RedirectUrl = $"https://zarinpal.com/pg/StartPay/{res.Authority}";
                                return result;
                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                            result.StatusCode = res.Status;
                            result.Message = ZarinPalMessageDictionary.GetError(res.Status);
                            return result;
                        }
                    }

                }

            }
            else
            {
                result.IsSuccess = false;
                result.StatusCode = 800;
                result.Message = ZarinPalMessageDictionary.GetError(800);
                return result;
            }
        }

        public async Task<ZarinPalResult> VerifyPayment(string factorNumber, string status, string authority)
        {
            ZarinPalResult result = new ZarinPalResult() { IsSuccess = false };
            var setting = _settingService.Load<GetwaySettingsModel>(null);
            if (setting == null || (setting.BankGateway_Type == ZarinPalTypes.ZarinPal && String.IsNullOrEmpty(setting.BankGateway_TerminalNo)))
            {
                result.StatusCode = 800;
                result.Message = ZarinPalMessageDictionary.GetError(800);
                return result;
            }
            var order = await _orderService.GetOrderByFactorNumber(factorNumber);
            if (order==null)
            {
                result.StatusCode = 504;
                result.Message = ZarinPalMessageDictionary.GetError(504);
                return result;
            }
            else
            {
                if (order.UserId!=_appContext.User.Id)
                {
                    result.StatusCode = 802;
                    result.Message = ZarinPalMessageDictionary.GetError(802);
                    return result;
                }
            }
            if (!String.IsNullOrEmpty(status) && status.ToLower() == "ok"&&!String.IsNullOrEmpty(authority))
            {
                var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
                var res = await _payment.Verification(new DtoVerification
                {
                    Amount = priceUnitSetting != null ? (priceUnitSetting.Unit == PriceUnitTypes.Tooman ? (int)order.PayableAmount : (int)order.PayableAmount / 10) : ((int)order.PayableAmount / 10),
                    MerchantId = setting.BankGateway_Type == ZarinPalTypes.ZarinPal ? setting.BankGateway_TerminalNo : "XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX",
                    Authority = authority
                }, ZarinPal.Class.Payment.Mode.sandbox);
                if (res.Status == 100)
                {
                    order.Status = StatusTypes.SuccessfulPayment;
                    order.TransactionCode = res.RefId.ToString();
                    order.TransactionNumber = authority.TrimStart('0');
                    order.PaidAt = DateTime.Now;
                    order.Getway = setting.BankGateway_Type == ZarinPalTypes.ZarinPal ? BankGatewayTypes.ZarinPal : BankGatewayTypes.ZarinPalSandBox;
                    order.ShippingStatus = ShippingSatusTypes.NotShipped;
                    try
                    {
                        await _orderService.UpdateOrderPayment(order);
                    }
                    catch (Exception ex)
                    {
                        var msg = $"VerifyPayment Method(OrderId-{order.Id}) : Verification Succeeded," +
                                $"StatusCode-{res.Status},Authority-{authority},RefId={ res.RefId}";

                        _logger.LogError(msg + $".But PayOrder method Failed With Message-{ex.Message}");
                    }
                    result.IsSuccess = true;
                    result.StatusCode = res.Status;
                    result.Message = ZarinPalMessageDictionary.GetError(res.Status);
                    result.TransactionNumber = authority.TrimStart('0');
                    result.RefNumber = res.RefId.ToString();
                    _httpContext.HttpContext.Session.Remove("ShopCart");
                    return result;
                }
                else
                {
                    order.Status = StatusTypes.UnsuccessfulPayment;
                    order.Getway = setting.BankGateway_Type == ZarinPalTypes.ZarinPal ? BankGatewayTypes.ZarinPal : BankGatewayTypes.ZarinPalSandBox;
                    await _orderService.UpdateOrder(order);
                    result.IsSuccess = false;
                    result.StatusCode = res.Status;
                    result.Message = ZarinPalMessageDictionary.GetError(res.Status);
                    _logger.LogError($"خطای بانکی: شماره فاکتور:{order.Number} ، کد خطا:{res.Status} ، پیام خطا:{result.Message}");
                    return result;
                }
            }
            else
            {
                order.Status = StatusTypes.UnsuccessfulPayment;
                order.Getway = setting.BankGateway_Type == ZarinPalTypes.ZarinPal ? BankGatewayTypes.ZarinPal : BankGatewayTypes.ZarinPalSandBox;
                await _orderService.UpdateOrder(order);
                result.StatusCode = 502;
                result.Message = ZarinPalMessageDictionary.GetError(502);
                _logger.LogError($"خطای بانکی: شماره فاکتور:{factorNumber} ، کد خطا:{502} ، پیام خطا:{result.Message}");
                return result;
            }
        }
    }
}

﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IProductFavoriteService
    {
        IQueryable<ProductFavorite> GetAllAsQueryable();
        Task InsertFav(ProductFavorite entity);
        Task DeleteFav(long entityId);
        Task<long> HasFav(long productId);
    }
}

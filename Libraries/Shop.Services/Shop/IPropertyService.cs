﻿using NikanBase.Core.Domain.Shop;
using Shop.Services.Dto.Property;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IPropertyService
    {
        IQueryable<Property> GetAllAsQueryable();
        Task<List<Property>> GetAllPropertyInCategory(long catId);
        Task<List<PropertyFilterModel>> GetPropertyFilters();
        Task InsertProperty(Property entity);
        Task UpdateProperty(Property entity);
        Task DeleteProperty(Property entity);
        Task<Property> GetPropertyById(long entityId);
    }
}

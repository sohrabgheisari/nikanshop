﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Dto.Property;
using Shop.Services.Infrastructure;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class PropertyService : BaseService, IPropertyService
    {
        public PropertyService(AppDbContext context) : base(context)
        {
        }

        public Task DeleteProperty(Property entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<Property> GetAllAsQueryable()
        {
            return Table<Property>().Include(x => x.PropertyCategory);
        }

        public Task<List<Property>> GetAllPropertyInCategory(long catId)
        {
            return Table<Property>().Where(x => x.PropertCategoryId == catId).ToListAsync();
        }

        public Task<Property> GetPropertyById(long entityId)
        {
            return GetById<Property>(entityId);
        }

        public async Task<List<PropertyFilterModel>> GetPropertyFilters()
        {
            List<PropertyFilterModel> result = new List<PropertyFilterModel>();
            var propertyCategories = Table<PropertyCategory>().Include(x => x.Properties).ToList();
            foreach (var cat in propertyCategories)
            {
                foreach (var item in cat.Properties)
                {
                    var values = Table<ProductProperty>().Where(x => x.PropertyId == item.Id).Select(x => x.Value).ToList();
                    result.Add(new PropertyFilterModel()
                    {
                        Name = item.Name,
                        Values = values.Any()? values.Distinct().Select(x => new SelectListItem() { Text = x, Value = item.Id.ToString() }).ToList():new List<SelectListItem>()
                    });

                }
            }
            return result;
        }

        public Task InsertProperty(Property entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateProperty(Property entity)
        {
            Update(entity);
            return Save();
        }
    }
}

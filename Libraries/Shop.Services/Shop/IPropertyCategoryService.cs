﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IPropertyCategoryService
    {
        IQueryable<PropertyCategory> GetAllAsQueryable();

        Task InsertPropertyCategory(PropertyCategory entity);
        Task UpdatePropertyCategory(PropertyCategory entity);
        Task DeletePropertyCategory(PropertyCategory entity);
        Task<PropertyCategory> GetPropertyCategoryById(long entityId);
    }
}

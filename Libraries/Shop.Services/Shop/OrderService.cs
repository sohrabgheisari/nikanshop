﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Data;
using Shop.Services.Dto.Nikan;
using Shop.Services.Dto.Product;
using Shop.Services.Dto.Setting;
using Shop.Services.Infrastructure;
using Shop.Services.Infrastructure.Mapping;
using Shop.Services.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class OrderService : BaseService, IOrderService
    {
        private readonly IAppContext _appContext;
        private readonly IProductService _productService;
        private readonly IHttpContextAccessor _httpContext;
        private readonly UserManager _userManager;
        private readonly IDerakService _derakService;
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;
        public OrderService(AppDbContext context,
            IAppContext appContext,
            IProductService productService,
            IHttpContextAccessor httpContext,
            UserManager userManager,
            IDerakService derakService,
             ISettingService settingService,
             ILogger<OrderService> logger) : base(context)
        {
            _appContext = appContext;
            _productService = productService;
            _httpContext = httpContext;
            _userManager = userManager;
            _derakService = derakService;
            _settingService = settingService;
            _logger = logger;
        }

        public Task DeleteOrderItem(OrderItem entity)
        {
            Delete(entity);
            return Save();
        }

        public async Task<List<ShopCardModel>> GetLastRegisterdOrder(long userId)
        {
            List<ShopCardModel> card = new List<ShopCardModel>();
            var order = Table<Order>().Where(x => x.UserId == userId && x.Status == Core.Enums.StatusTypes.Registered).OrderByDescending(x => x.CreatedAt)
                //.Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.Discount)
                //.Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.ColorTrait)
                //.Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.WarrantyTrait)
                .FirstOrDefault();
            if (order == null)
            {
                return card;
            }
            card = await GetCardOrder(order.Id, userId);
            //if (order != null)
            //{
            //    foreach (var item in order.OrderItems)
            //    {
            //        DiscountCardModel discountCardModel = new DiscountCardModel();
            //        long? dicountId = null;
            //        if (item.Product.Discount != null)
            //        {
            //            discountCardModel = new DiscountCardModel()
            //            {
            //                Amount = item.Product.Discount.Amount,
            //                DiscountFrom = item.Product.Discount.DiscountFrom,
            //                DiscountTo = item.Product.Discount.DiscountTo,
            //                MaxAmount = item.Product.Discount.MaxAmount,
            //                Percentage = item.Product.Discount.Percentage,
            //                Type = item.Product.Discount.Type
            //            };
            //            dicountId = item.Product.Discount.Id;
            //        }
            //        ProductCardModel productCardModel = new ProductCardModel()
            //        {
            //            Discount = discountCardModel,
            //            Count = item.Count,
            //            DiscountAmount = item.DiscountAmount,
            //            DiscountId = dicountId,
            //            ImageUrl = item.Product.ImageUrl,
            //            OrderItemId = item.Id,
            //            Name = item.Product.Name,
            //            Price = item.Price,
            //            PriceWithDiscount = item.PriceWithDiscount,
            //            Id = item.Product.Id
            //        };
            //        card.Add(new ShopCardModel()
            //        {
            //            OrderId = order.Id,
            //            OrderNumber = order.Number,
            //            Product = productCardModel,
            //            UserId = order.UserId,
            //        });
            //    }
            //}
            return card;
        }

        public Task<Order> GetOrderById(long entityId)
        {
            return GetById<Order>(entityId);
        }

        public Task<Order> GetOrderByFactorNumber(string facNumber)
        {
            return Table<Order>().FirstOrDefaultAsync(x => x.Number == facNumber);
        }

        public Task<OrderItem> GetOrderItemById(long entityId)
        {
            return GetById<OrderItem>(entityId);
        }

        public async Task<List<ShopCardModel>> InsertOrder(List<ShopCardModel> card, string address)
        {
            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
                return card;
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                    return card;
                }
            }
            async Task Act()
            {
                Order order = new Order()
                {
                    Number = DateTimeExtentions.PersianFullDate(),
                    Address = address,
                    CreatedAt = DateTime.Now,
                    Status = Core.Enums.StatusTypes.Registered,
                    UserId = _appContext.User.Id,
                    PayableAmount = card.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count)
                    + card.Select(x => x.Product).Sum(x => x.PriceIncrease * x.Count)
                };
                _context.Add(order);
                await _context.SaveChangesAsync();
                List<OrderItem> items = new List<OrderItem>();
                foreach (var prd in card.Select(x => x.Product))
                {
                    items.Add(new OrderItem()
                    {
                        Count = prd.Count,
                        DiscountAmount = prd.DiscountAmount,
                        OrderId = order.Id,
                        Price = prd.Price,
                        PriceWithDiscount = prd.PriceWithDiscount,
                        ProductId = prd.Id,
                        ProductTraitId = prd.ProductTraitId,
                        PriceIncrease = prd.PriceIncrease
                    });
                }
                _context.AddRange(items);
                await _context.SaveChangesAsync();

                foreach (var item in card)
                {
                    item.OrderId = order.Id;
                    item.Product.OrderItemId = items.FirstOrDefault(x => x.ProductId == item.Product.Id && x.ProductTraitId == item.Product.ProductTraitId).Id;
                    item.OrderNumber = order.Number;
                    item.UserId = _appContext.User.Id;
                }
            }

        }

        public async Task UpdateCardOrder(OrderItem item, Order order)
        {
            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                }
            }
            async Task Act()
            {
                if (item.Id > 0)
                {
                    _context.Update(item);
                }
                else
                {
                    _context.Add(item);
                }
                _context.Update(order);
                _context.SaveChanges();
            }
        }

        public async Task DeleteFromCardOrder(OrderItem item, Order order)
        {
            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                }
            }
            async Task Act()
            {
                _context.Remove(item);
                _context.Update(order);
                _context.SaveChanges();
            }
        }

        public Task UpdateOrderItem(OrderItem entity)
        {
            Update(entity);
            return Save();
        }
        public Task UpdateOrder(Order entity)
        {
            Update(entity);
            return Save();
        }

        public Task InsertOrderItem(OrderItem entity)
        {
            Insert(entity);
            return Save();
        }

        public Task DeleteOrder(Order entity)
        {
            Delete(entity);
            return Save();
        }

        public async Task<CardViewModel> GetCard()
        {
            CardViewModel model = new CardViewModel();
            List<ShopCardModel> cart = new List<ShopCardModel>();

            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                if (cart.Sum(x => x.Product.Count) > 0)
                {
                    model.IsFull = true;
                    bool status = true;
                    if (!String.IsNullOrEmpty(cart.ElementAt(0).OrderNumber))
                    {
                        model.FactorNumber = cart.ElementAt(0).OrderNumber;
                        model.IsFactor = true;
                        var order = await GetOrderById(cart.ElementAt(0).OrderId);
                        if (order == null)
                        {
                            _httpContext.HttpContext.Session.Remove("ShopCart");
                            return model;
                        }
                    }
                    List<string> message = new List<string>();
                    foreach (var item in cart)
                    {
                        var product = await _productService.GetProductById(item.Product.Id);
                        if (product == null)
                        {
                            int index = cart.FindIndex(x => x.Product.Id == product.Id);
                            if (index >= 0)
                            {
                                if (item.Product.OrderItemId > 0)
                                {
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                    await DeleteFromCardOrder(new OrderItem() { Id = item.Product.OrderItemId }, order);
                                }
                                cart.RemoveAt(index);
                                message.Add($"محصول {item.Product.Name} از سبد حذف شده است");
                            }
                        }
                        else
                        {
                            ProductCardModel productModel = Mapping.Mapper.Map<ProductCardModel>(product);
                            productModel.Count = item.Product.Count;
                            productModel.OrderItemId = item.Product.OrderItemId;
                            productModel.ProductTraitId = item.Product.ProductTraitId;
                            productModel.FillFields();
                            if (item.Product.DiscountAmount != productModel.DiscountAmount && item.Product.Price == productModel.Price || (item.Product.Price == productModel.Price && item.Product.DiscountId != productModel.DiscountId))
                            {
                                item.Product = productModel;
                                item.Product.FillFields();
                                if (item.Product.OrderItemId > 0)
                                {
                                    var orderItem = await GetOrderItemById(item.Product.OrderItemId);
                                    orderItem.Price = item.Product.Price;
                                    orderItem.DiscountAmount = item.Product.DiscountAmount;
                                    orderItem.PriceWithDiscount = item.Product.PriceWithDiscount;
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                    await UpdateCardOrder(orderItem, order);
                                }
                                message.Add($"میزان تخفیف محصول {item.Product.Name} تغییر کرده است");
                            }
                            if (item.Product.Price != productModel.Price)
                            {
                                item.Product = productModel;
                                item.Product.FillFields();
                                if (item.Product.OrderItemId > 0)
                                {
                                    var orderItem = await GetOrderItemById(item.Product.OrderItemId);
                                    orderItem.Price = item.Product.Price;
                                    orderItem.DiscountAmount = item.Product.DiscountAmount;
                                    orderItem.PriceWithDiscount = item.Product.PriceWithDiscount;
                                    //await UpdateOrderItem(orderItem);
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                    //await UpdateOrder(order);
                                    await UpdateCardOrder(orderItem, order);
                                }
                                message.Add($"قیمت محصول {item.Product.Name} تغییر کرده است");
                            }
                            if (item.Product.ProductTraitId.HasValue)
                            {
                                var trait = product.ProductTraits.FirstOrDefault(x => x.Id == item.Product.ProductTraitId.Value);
                                if (trait.MinimumCart > 0 && item.Product.Count < trait.MinimumCart)
                                {
                                    status = false;
                                    message.Add($"حداقل سبد خرید برای محصول  {item.Product.Name} {trait.MinimumCart} عدد است");
                                }
                                if (trait.MaximumCart > 0 && item.Product.Count > trait.MaximumCart)
                                {
                                    status = false;
                                    message.Add($"حداکثر سبد خرید برای محصول  {item.Product.Name} {trait.MaximumCart} عدد است");
                                }
                                if (item.Product.Count > trait.Inventory || trait.Inventory == 0)
                                {
                                    status = false;
                                    message.Add($"محصول {item.Product.Name} دارای موجودی کافی در انبار نمی باشد");
                                    item.Product.FillFields();
                                }
                            }
                            else
                            {
                                if (product.MinCart > 0 && item.Product.Count < product.MinCart)
                                {
                                    status = false;
                                    message.Add($"حداقل سبد خرید برای محصول  {item.Product.Name} {product.MinCart} عدد است");
                                }
                                if (product.MaxCart > 0 && item.Product.Count > product.MaxCart)
                                {
                                    status = false;
                                    message.Add($"حداکثر سبد خرید برای محصول  {item.Product.Name} {product.MaxCart} عدد است");
                                }
                                if (item.Product.Count > product.Inventory || product.Inventory == 0)
                                {
                                    status = false;
                                    message.Add($"محصول {item.Product.Name} دارای موجودی کافی در انبار نمی باشد");
                                    item.Product.FillFields();
                                }
                            }

                        }
                    }
                    model.Message = message;
                    model.Status = status;
                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                }
                else
                {
                    model.IsFull = false;
                }

            }
            else
            {
                model.IsFull = false;
            }

            model.Card = cart;

            return model;
        }

        public async Task<int> GetCardCount()
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();

            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                return cart.Sum(x => x.Product.Count);
            }
            return 0;
        }

        public async Task<int> AddToCard(long id)
        {
            try
            {
                var product = await _productService.GetProductById(id);
                List<ShopCardModel> cart = new List<ShopCardModel>();

                if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
                {
                    cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                }

                if (product.Price > 0)
                {
                    if (cart.Any(p => p.Product.Id == id))
                    {
                        int index = cart.FindIndex(p => p.Product.Id == id);
                        if (cart[index].Product.Count + 1 <= product.Inventory)
                        {
                            if (cart.ElementAt(0).OrderId > 0)
                            {
                                if (cart.ElementAt(0).UserId == _appContext.User.Id)
                                {
                                    cart[index].Product.Count += 1;
                                    var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                    cart[index].Product.FillFields();
                                    orderItem.Count = cart[index].Product.Count;
                                    orderItem.Price = cart[index].Product.Price;
                                    orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                    orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                    //await UpdateOrderItem(orderItem);
                                    //await UpdateOrder(order);
                                    await UpdateCardOrder(orderItem, order);
                                }

                            }
                            else
                            {
                                cart[index].Product.Count += 1;
                            }
                        }
                    }
                    else
                    {
                        ProductCardModel productCardModel = Mapping.Mapper.Map<ProductCardModel>(product);
                        productCardModel.Count = 1;
                        productCardModel.FillFields();
                        if (productCardModel.Count <= product.Inventory)
                        {
                            if (cart.Count() > 0 && cart.ElementAt(0).OrderId > 0)
                            {
                                if (cart.ElementAt(0).UserId == _appContext.User.Id)
                                {
                                    cart.Add(new ShopCardModel()
                                    {
                                        Product = productCardModel
                                    });
                                    OrderItem orderItem = new OrderItem()
                                    {
                                        Count = productCardModel.Count,
                                        OrderId = cart.ElementAt(0).OrderId,
                                        Price = productCardModel.Price,
                                        DiscountAmount = productCardModel.DiscountAmount,
                                        PriceWithDiscount = productCardModel.PriceWithDiscount,
                                        ProductId = productCardModel.Id
                                    };
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                    //await InsertOrderItem(orderItem);
                                    //await UpdateOrder(order);
                                    await UpdateCardOrder(orderItem, order);
                                }
                            }
                            else
                            {
                                cart.Add(new ShopCardModel()
                                {
                                    Product = productCardModel
                                });
                            }

                        }
                    }
                }

                _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);

                return cart.Sum(x => x.Product.Count);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<int> AddToCard(long id, long traitId)
        {
            try
            {
                var product = await _productService.GetProductById(id);
                var trait = product.ProductTraits.FirstOrDefault(x => x.Id == traitId);
                List<ShopCardModel> cart = new List<ShopCardModel>();

                if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
                {
                    cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                }

                if (product.Price > 0)
                {
                    if (cart.Any(p => p.Product.Id == id && p.Product.ProductTraitId == traitId))
                    {
                        int index = cart.FindIndex(p => p.Product.Id == id && p.Product.ProductTraitId == traitId);
                        if (cart[index].Product.Count + 1 <= trait.Inventory)
                        {
                            if (cart.ElementAt(0).OrderId > 0)
                            {
                                if (cart.ElementAt(0).UserId == _appContext.User.Id)
                                {
                                    cart[index].Product.Count += 1;
                                    var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                    cart[index].Product.FillFields();
                                    orderItem.Count = cart[index].Product.Count;
                                    orderItem.Price = cart[index].Product.Price;
                                    orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                    orderItem.PriceIncrease = cart[index].Product.PriceIncrease;
                                    orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => (x.PriceWithDiscount + x.PriceIncrease) * x.Count);
                                    await UpdateCardOrder(orderItem, order);
                                }

                            }
                            else
                            {
                                cart[index].Product.Count += 1;
                            }
                        }
                    }
                    else
                    {
                        ProductCardModel productCardModel = Mapping.Mapper.Map<ProductCardModel>(product);
                        productCardModel.Count = 1;
                        productCardModel.ProductTraitId = traitId;
                        productCardModel.PriceIncrease = trait.PriceIncrease;
                        productCardModel.FillFields();
                        //productCardModel.PriceWithDiscount += trait.PriceIncrease;
                        if (productCardModel.Count <= trait.Inventory)
                        {
                            if (cart.Count() > 0 && cart.ElementAt(0).OrderId > 0)
                            {
                                if (cart.ElementAt(0).UserId == _appContext.User.Id)
                                {
                                    cart.Add(new ShopCardModel()
                                    {
                                        Product = productCardModel
                                    });
                                    OrderItem orderItem = new OrderItem()
                                    {
                                        Count = productCardModel.Count,
                                        OrderId = cart.ElementAt(0).OrderId,
                                        Price = productCardModel.Price,
                                        DiscountAmount = productCardModel.DiscountAmount,
                                        PriceWithDiscount = productCardModel.PriceWithDiscount,
                                        PriceIncrease = productCardModel.PriceIncrease,
                                        ProductId = productCardModel.Id,
                                        ProductTraitId = traitId
                                    };
                                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                    order.PayableAmount = cart.Select(x => x.Product).Sum(x => (x.PriceWithDiscount + x.PriceIncrease) * x.Count);
                                    //await InsertOrderItem(orderItem);
                                    //await UpdateOrder(order);
                                    await UpdateCardOrder(orderItem, order);
                                }
                            }
                            else
                            {
                                cart.Add(new ShopCardModel()
                                {
                                    Product = productCardModel
                                });
                            }

                        }
                    }
                }

                _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);

                return cart.Sum(x => x.Product.Count);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> PlusToCard(long id)
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();
            var product = await _productService.GetProductById(id);
            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                int index = cart.FindIndex(x => x.Product.Id == id);
                if (index >= 0)
                {
                    if (cart[index].Product.Count + 1 <= product.Inventory)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count += 1;
                                var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                cart[index].Product.FillFields();
                                orderItem.Count = cart[index].Product.Count;
                                orderItem.Price = cart[index].Product.Price;
                                orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                //await UpdateOrderItem(orderItem);
                                //await UpdateOrder(order);
                                await UpdateCardOrder(orderItem, order);
                            }

                        }
                        else
                        {
                            cart[index].Product.Count += 1;
                        }
                    }
                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> PlusToCard(long id, long traitId)
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();
            var product = await _productService.GetProductById(id);
            var trait = product.ProductTraits.FirstOrDefault(x => x.Id == traitId);
            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                int index = cart.FindIndex(x => x.Product.Id == id && x.Product.ProductTraitId == traitId);
                if (index >= 0)
                {
                    if (cart[index].Product.Count + 1 <= trait.Inventory)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count += 1;
                                var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                cart[index].Product.FillFields();
                                orderItem.Count = cart[index].Product.Count;
                                orderItem.Price = cart[index].Product.Price;
                                orderItem.PriceIncrease = cart[index].Product.PriceIncrease;
                                orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => (x.PriceWithDiscount + x.PriceIncrease) * x.Count);
                                //await UpdateOrderItem(orderItem);
                                //await UpdateOrder(order);
                                await UpdateCardOrder(orderItem, order);
                            }

                        }
                        else
                        {
                            cart[index].Product.Count += 1;
                        }
                    }
                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> MinesToCard(long id)
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();
            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                int index = cart.FindIndex(x => x.Product.Id == id);
                if (index >= 0)
                {
                    if (cart[index].Product.Count > 1)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count -= 1;
                                var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                cart[index].Product.FillFields();
                                orderItem.Count = cart[index].Product.Count;
                                orderItem.Price = cart[index].Product.Price;
                                orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                await UpdateCardOrder(orderItem, order);
                            }
                        }
                        else
                        {
                            cart[index].Product.Count -= 1;
                        }
                    }
                    else if (cart[index].Product.Count == 1)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count -= 1;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                await DeleteFromCardOrder(new OrderItem() { Id = cart[index].Product.OrderItemId }, order);
                                cart.RemoveAt(index);
                                if (cart.Count == 0)
                                {
                                    await DeleteOrder(order);
                                }
                            }
                        }
                        else
                        {
                            cart.RemoveAt(index);
                        }
                    }

                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> MinesToCard(long id, long traitId)
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();
            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                int index = cart.FindIndex(x => x.Product.Id == id && x.Product.ProductTraitId == traitId);
                if (index >= 0)
                {
                    if (cart[index].Product.Count > 1)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count -= 1;
                                var orderItem = await GetOrderItemById(cart[index].Product.OrderItemId);
                                cart[index].Product.FillFields();
                                orderItem.Count = cart[index].Product.Count;
                                orderItem.Price = cart[index].Product.Price;
                                orderItem.PriceIncrease = cart[index].Product.PriceIncrease;
                                orderItem.DiscountAmount = cart[index].Product.DiscountAmount;
                                orderItem.PriceWithDiscount = cart[index].Product.PriceWithDiscount;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => (x.PriceWithDiscount + x.PriceIncrease) * x.Count);
                                await UpdateCardOrder(orderItem, order);
                            }
                        }
                        else
                        {
                            cart[index].Product.Count -= 1;
                        }
                    }
                    else if (cart[index].Product.Count == 1)
                    {
                        if (cart.ElementAt(0).OrderId > 0)
                        {
                            if (cart.ElementAt(0).UserId == _appContext.User.Id)
                            {
                                cart[index].Product.Count -= 1;
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => (x.PriceWithDiscount + x.PriceIncrease) * x.Count);
                                await DeleteFromCardOrder(new OrderItem() { Id = cart[index].Product.OrderItemId }, order);
                                cart.RemoveAt(index);
                                if (cart.Count == 0)
                                {
                                    await DeleteOrder(order);
                                }
                            }
                        }
                        else
                        {
                            cart.RemoveAt(index);
                        }
                    }

                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> RemoveFromCard(long id)
        {
            List<ShopCardModel> cart = new List<ShopCardModel>();
            if (_httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                cart = _httpContext.HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
                int index = cart.FindIndex(x => x.Product.Id == id);
                if (index >= 0)
                {
                    if (cart.ElementAt(0).OrderId > 0)
                    {
                        if (cart.ElementAt(0).UserId == _appContext.User.Id)
                        {
                            //await DeleteOrderItem(new OrderItem() { Id = cart[index].Product.OrderItemId });
                            var order = await GetOrderById(cart.ElementAt(0).OrderId);
                            order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                            //await UpdateOrder(order);
                            await DeleteFromCardOrder(new OrderItem() { Id = cart[index].Product.OrderItemId }, order);
                            cart.RemoveAt(index);
                            if (cart.Count == 0)
                            {
                                await DeleteOrder(order);
                            }
                        }
                    }
                    else
                    {
                        cart.RemoveAt(index);
                    }
                    _httpContext.HttpContext.Session.SetObjectAsJson("ShopCart", cart);
                    return true;
                }
            }
            return false;
        }

        public async Task<List<ShopCardModel>> GetCardOrder(long orderId, long userId)
        {
            List<ShopCardModel> card = new List<ShopCardModel>();
            var order = Table<Order>().Where(x => x.Id == orderId && x.UserId == userId).OrderByDescending(x => x.CreatedAt)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.Discount)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.ProductTraits)
                .Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.ColorTrait)
                .Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.WarrantyTrait)
                .FirstOrDefault();
            if (order != null)
            {
                foreach (var item in order.OrderItems)
                {
                    DiscountCardModel discountCardModel = new DiscountCardModel();
                    long? dicountId = null;
                    if (item.Product.Discount != null)
                    {
                        discountCardModel = new DiscountCardModel()
                        {
                            Amount = item.Product.Discount.Amount,
                            DiscountFrom = item.Product.Discount.DiscountFrom,
                            DiscountTo = item.Product.Discount.DiscountTo,
                            MaxAmount = item.Product.Discount.MaxAmount,
                            Percentage = item.Product.Discount.Percentage,
                            Type = item.Product.Discount.Type
                        };
                        dicountId = item.Product.Discount.Id;
                    }
                    ProductCardModel productCardModel = new ProductCardModel()
                    {
                        Discount = discountCardModel,
                        Count = item.Count,
                        DiscountAmount = item.DiscountAmount,
                        DiscountId = dicountId,
                        ImageUrl = item.Product.ImageUrl,
                        OrderItemId = item.Id,
                        Name = item.Product.Name,
                        Price = item.Price,
                        PriceWithDiscount = item.PriceWithDiscount,
                        Id = item.Product.Id,
                        ProductTraitId = item.ProductTraitId,
                        PriceIncrease = item.PriceIncrease
                    };
                    if (item.ProductTraitId.HasValue)
                    {
                        string text = string.Empty;
                        var productTrait = item.ProductTrait;
                        if (productTrait.ColorTrait != null)
                        {
                            text += $"رنگ {productTrait.ColorTrait.Title}";
                        }
                        if (productTrait.WarrantyTrait != null)
                        {
                            text += $" - گارانتی {productTrait.WarrantyTrait.Title}";
                        }
                        productCardModel.TraitText = text;
                        productCardModel.ProductTraits = Mapping.Mapper.Map<List<ProductTraitsModel>>(item.Product.ProductTraits.ToList());
                    }
                    card.Add(new ShopCardModel()
                    {
                        OrderId = order.Id,
                        OrderNumber = order.Number,
                        Product = productCardModel,
                        UserId = order.UserId
                    });
                }
            }
            return card;
        }

        public async Task<List<ShopCardModel>> GetCardOrder(long orderId)
        {
            List<ShopCardModel> card = new List<ShopCardModel>();
            long userId = _appContext.User.Id;
            var order = Table<Order>().Where(x => x.Id == orderId && x.UserId == _appContext.User.Id).OrderByDescending(x => x.CreatedAt)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.Discount)
                .Include(x => x.OrderItems).ThenInclude(x => x.Product).ThenInclude(x => x.ProductTraits)
                .Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.ColorTrait)
                .Include(x => x.OrderItems).ThenInclude(x => x.ProductTrait).ThenInclude(x => x.WarrantyTrait)
                .FirstOrDefault();
            if (order != null)
            {
                foreach (var item in order.OrderItems)
                {
                    DiscountCardModel discountCardModel = new DiscountCardModel();
                    List<ProductTraitsModel> traitsModel = new List<ProductTraitsModel>();
                    long? dicountId = null;
                    if (item.Product.Discount != null)
                    {
                        discountCardModel = new DiscountCardModel()
                        {
                            Amount = item.Product.Discount.Amount,
                            DiscountFrom = item.Product.Discount.DiscountFrom,
                            DiscountTo = item.Product.Discount.DiscountTo,
                            MaxAmount = item.Product.Discount.MaxAmount,
                            Percentage = item.Product.Discount.Percentage,
                            Type = item.Product.Discount.Type
                        };
                        dicountId = item.Product.Discount.Id;
                    }
                    ProductCardModel productCardModel = new ProductCardModel()
                    {
                        Discount = discountCardModel,
                        Count = item.Count,
                        DiscountAmount = item.DiscountAmount,
                        DiscountId = dicountId,
                        ImageUrl = item.Product.ImageUrl,
                        OrderItemId = item.Id,
                        Name = item.Product.Name,
                        Price = item.Price,
                        PriceWithDiscount = item.PriceWithDiscount,
                        Id = item.Product.Id,
                        ProductTraitId = item.ProductTraitId,
                        PriceIncrease = item.PriceIncrease
                    };
                    if (item.ProductTraitId.HasValue)
                    {
                        string text = string.Empty;
                        var productTrait = item.ProductTrait;
                        if (productTrait.ColorTrait != null)
                        {
                            text += $"رنگ {productTrait.ColorTrait.Title}";
                        }
                        if (productTrait.WarrantyTrait != null)
                        {
                            text += $" - گارانتی {productTrait.WarrantyTrait.Title}";
                        }
                        productCardModel.TraitText = text;
                        productCardModel.ProductTraits = Mapping.Mapper.Map<List<ProductTraitsModel>>(item.Product.ProductTraits.ToList());
                    }
                    card.Add(new ShopCardModel()
                    {
                        OrderId = order.Id,
                        OrderNumber = order.Number,
                        Product = productCardModel,
                        UserId = order.UserId
                    });
                }
            }
            return card;
        }

        public async Task<CardViewModel> VerifyCard(long orderId)
        {
            var cart = await GetCardOrder(orderId);

            CardViewModel model = new CardViewModel();
            if (cart.Sum(x => x.Product.Count) > 0)
            {
                model.IsFull = true;
                bool status = true;
                if (!String.IsNullOrEmpty(cart.ElementAt(0).OrderNumber))
                {
                    model.FactorNumber = cart.ElementAt(0).OrderNumber;
                    model.IsFactor = true;
                    var order = await GetOrderById(cart.ElementAt(0).OrderId);
                    if (order == null)
                    {
                        cart = new List<ShopCardModel>();
                        return model;
                    }
                }
                List<string> message = new List<string>();
                foreach (var item in cart)
                {
                    var product = await _productService.GetProductById(item.Product.Id);
                    if (product == null)
                    {
                        int index = cart.FindIndex(x => x.Product.Id == product.Id);
                        if (index >= 0)
                        {
                            if (item.Product.OrderItemId > 0)
                            {
                                await DeleteOrderItem(new OrderItem() { Id = item.Product.OrderItemId });
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                await UpdateOrder(order);
                            }
                            cart.RemoveAt(index);
                            message.Add($"محصول {item.Product.Name} از سبد حذف شده است");
                        }
                    }
                    else
                    {
                        ProductCardModel productModel = Mapping.Mapper.Map<ProductCardModel>(product);
                        productModel.Count = item.Product.Count;
                        productModel.OrderItemId = item.Product.OrderItemId;
                        if (item.Product.ProductTraitId.HasValue)
                        {
                            productModel.PriceIncrease = product.ProductTraits.FirstOrDefault(x => x.Id == item.Product.ProductTraitId.Value).PriceIncrease;
                            productModel.ProductTraitId = item.Product.ProductTraitId;
                        }
                        productModel.FillFields();
                        if (item.Product.DiscountAmount != productModel.DiscountAmount && item.Product.Price == productModel.Price || (item.Product.Price == productModel.Price && item.Product.DiscountId != productModel.DiscountId))
                        {
                            item.Product = productModel;
                            item.Product.FillFields();
                            if (item.Product.OrderItemId > 0)
                            {
                                var orderItem = await GetOrderItemById(item.Product.OrderItemId);
                                orderItem.Price = item.Product.Price;
                                orderItem.DiscountAmount = item.Product.DiscountAmount;
                                orderItem.PriceWithDiscount = item.Product.PriceWithDiscount;
                                await UpdateOrderItem(orderItem);
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                await UpdateOrder(order);
                            }
                            message.Add($"میزان تخفیف محصول {item.Product.Name} تغییر کرده است");
                        }
                        if (item.Product.Price != productModel.Price)
                        {
                            item.Product = productModel;
                            item.Product.FillFields();
                            if (item.Product.OrderItemId > 0)
                            {
                                var orderItem = await GetOrderItemById(item.Product.OrderItemId);
                                orderItem.Price = item.Product.Price;
                                orderItem.DiscountAmount = item.Product.DiscountAmount;
                                orderItem.PriceWithDiscount = item.Product.PriceWithDiscount;
                                await UpdateOrderItem(orderItem);
                                var order = await GetOrderById(cart.ElementAt(0).OrderId);
                                order.PayableAmount = cart.Select(x => x.Product).Sum(x => x.PriceWithDiscount * x.Count);
                                await UpdateOrder(order);
                            }
                            message.Add($"قیمت محصول {item.Product.Name} تغییر کرده است");
                        }
                        if (item.Product.ProductTraitId.HasValue)
                        {
                            var trait = product.ProductTraits.FirstOrDefault(x => x.Id == item.Product.ProductTraitId.Value);
                            if (trait.MinimumCart > 0 && item.Product.Count < trait.MinimumCart)
                            {
                                status = false;
                                message.Add($"حداقل سبد خرید برای محصول  {item.Product.Name} {trait.MinimumCart} عدد است");
                            }
                            if (trait.MaximumCart > 0 && item.Product.Count > trait.MaximumCart)
                            {
                                status = false;
                                message.Add($"حداکثر سبد خرید برای محصول  {item.Product.Name} {trait.MaximumCart} عدد است");
                            }
                            if (item.Product.Count > trait.Inventory || trait.Inventory == 0)
                            {
                                status = false;
                                message.Add($"محصول {item.Product.Name} دارای موجودی کافی در انبار نمی باشد");
                                item.Product.FillFields();
                            }
                        }
                        else
                        {
                            if (product.MinCart > 0 && item.Product.Count < product.MinCart)
                            {
                                status = false;
                                message.Add($"حداقل سبد خرید برای محصول  {item.Product.Name} {product.MinCart} عدد است");
                            }
                            if (product.MaxCart > 0 && item.Product.Count > product.MaxCart)
                            {
                                status = false;
                                message.Add($"حداکثر سبد خرید برای محصول  {item.Product.Name} {product.MaxCart} عدد است");
                            }
                            if (item.Product.Count > product.Inventory || product.Inventory == 0)
                            {
                                status = false;
                                message.Add($"محصول {item.Product.Name} دارای موجودی کافی در انبار نمی باشد");
                                item.Product.FillFields();
                            }
                        }

                        item.Product = productModel;
                    }
                }
                model.Message = message;
                model.Status = status;
            }
            else
            {
                model.IsFull = false;
            }
            model.Card = cart;
            return model;
        }

        public IQueryable<Order> GetAllOrderAsQueryable(long? userId, OrderSearchModel model)
        {
            var query = Table<Order>();
            if (userId.HasValue)
            {
                query = query.Where(x => x.UserId == userId.Value);
            }
            if (model.Status.HasValue)
            {
                query = query.Where(x => x.Status == model.Status.Value);
            }
            if (model.Shipping.HasValue)
            {
                query = query.Where(x => x.ShippingStatus.Value == model.Shipping.Value);
            }
            if (model.UserId.HasValue)
            {
                query = query.Where(x => x.UserId == userId.Value);
            }
            if (!String.IsNullOrEmpty(model.PayDate_Start))
            {
                var date = model.PayDate_Start.PersianDateToMiladi();
                query = query.Where(x => x.PaidAt.Value.Date >= date.Date);
            }  
            if (!String.IsNullOrEmpty(model.PayDate_End))
            {
                var date = model.PayDate_End.PersianDateToMiladi();
                query = query.Where(x => x.PaidAt.Value.Date <= date.Date);
            }
            return query.Include(x => x.User).OrderByDescending(x => x.CreatedAt);
        }

        public async Task UpdateOrderPayment(Order order)
        {

            if (_context.Database.CurrentTransaction != null)
            {
                await Act();
            }
            else
            {
                using (var transaction = await _context.Database.BeginTransactionAsync())
                {
                    await Act();
                    transaction.Commit();
                }
            }
            async Task Act()
            {
                bool allowSendToNikan = true;
                var orderItems = Table<OrderItem>().Include(x => x.Product).ThenInclude(x => x.NikanProductEquivalent).Where(x => x.OrderId == order.Id).ToList();
                List<Product> products = new List<Product>();
                List<ProductTrait> productTraits = new List<ProductTrait>();
                foreach (var item in orderItems)
                {
                    if (item.Product.NikanProductEquivalent == null)
                    {
                        allowSendToNikan = false;
                    }
                    if (item.ProductTraitId.HasValue)
                    {
                        var productTrait = _context.Set<ProductTrait>().FirstOrDefault(x => x.Id == item.ProductTraitId.Value);
                        productTrait.Inventory = productTrait.Inventory - item.Count;
                        productTraits.Add(productTrait);
                    }
                    else
                    {
                        var product = _context.Set<Product>().FirstOrDefault(x => x.Id == item.ProductId);
                        product.Inventory = product.Inventory - item.Count;
                        products.Add(product);
                    }
                }
                if (products.Any())
                {
                    _context.UpdateRange(products);
                }
                if (productTraits.Any())
                {
                    _context.UpdateRange(productTraits);
                }
                if (allowSendToNikan)
                {
                    try
                    {
                        var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
                        var nikanProducts = await _derakService.GetNikanProduct();
                        var user = _userManager.Users.Include(x => x.NikanCustomerEquivalent).FirstOrDefault(x => x.Id == order.UserId);
                        List<NikanCreateAccountingOperationDetailsModel> items = new List<NikanCreateAccountingOperationDetailsModel>();
                        foreach (var item in orderItems)
                        {
                            items.Add(new NikanCreateAccountingOperationDetailsModel()
                            {
                                DefCount = item.Count,
                                DefPrice = priceUnitSetting != null ? (priceUnitSetting.Unit == PriceUnitTypes.Tooman ? (long)(item.Price + item.PriceIncrease) * 10 : (long)(item.Price + item.PriceIncrease)) : (long)(item.Price + item.PriceIncrease),
                                DiscountAmount = priceUnitSetting != null ? (priceUnitSetting.Unit == PriceUnitTypes.Tooman ? (long)item.DiscountAmount * item.Count* 10 : (long)item.DiscountAmount* item.Count) : (long)item.DiscountAmount* item.Count,
                                ProductCode = item.Product.NikanProductEquivalent.NikanProductId,
                                DefUnit = nikanProducts.FirstOrDefault(x => x.LotsId == item.Product.NikanProductEquivalent.NikanProductId).DefUnit,
                            });
                        }
                        NikanCreateAccountingOperationHeaderModel headerModel = new NikanCreateAccountingOperationHeaderModel()
                        {
                            CustomerId = user.NikanCustomerEquivalent != null ? user.NikanCustomerEquivalent.NikanCustomerId : 0,
                            Items = items,
                            PaidByPos = true,
                            Type=6,
                            PaidByPosAmount = priceUnitSetting != null ? (priceUnitSetting.Unit == PriceUnitTypes.Tooman ? (long)order.PayableAmount * 10 : (long)order.PayableAmount) : (long)order.PayableAmount,
                        };
                        var result = await _derakService.CreateFactor(headerModel);
                        if (result.HFac_No > 0)
                        {
                            order.SendToNikan = true;
                        }
                    }
                    catch (Exception ex)
                    {

                        _logger.LogError($"خطای ارسال پیش فاکتور به نیکان. متن خطا: {ex.Message}");
                    }

                }
                _context.Update(order);
                _context.SaveChanges();
            }
        }


    }
}

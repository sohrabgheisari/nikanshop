﻿using Shop.Services.Dto.Nikan;
using Shop.Services.Dto.Setting;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IDerakService
    {
        Task<string> GetToken(DerakSettingModel setting);
        Task<List<NikanProduct>> GetNikanProduct();
        Task<List<NikanCustomer>> GetNikanCustomer();
        Task<NikanAccountingOperationResult> CreateFactor(NikanCreateAccountingOperationHeaderModel model);
    }
}

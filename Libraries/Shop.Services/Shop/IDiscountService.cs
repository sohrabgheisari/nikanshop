﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IDiscountService
    {
        IQueryable<Discount> GetAllDescountAsQueryable();
        IQueryable<Discount> GetAllValidDescountAsQueryable();
        Task InsertDiscount(Discount entity);
        Task UpdateDiscount(Discount entity);
        Task DeleteDiscount(Discount entity);
        Task<Discount> GetDiscountById(long entityId);
    }
}

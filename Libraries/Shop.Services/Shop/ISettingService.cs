﻿using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface ISettingService
    {
        Task<Setting> GetByKey(string key, long? userId);

        Task<T> GetByKey<T>(string key, long? userId, T defaultValue = default(T));

        T Load<T>(long? userId) where T : class, new();

        Task SaveAll(Dictionary<object, List<string>> settings, long? sourceId, bool includeKeyNamesInUpdate = true);

        void SaveSync(object setting, long? sourceId, bool includeKeyNamesInUpdate = true, params string[] keySelectors);

        Task<string> SaveWebsiteLogo(string prevLogoUrl,IFormFile WebsiteSetting_LogoUrl, IFormFile WebsiteSetting_IconUrl);
    }
}

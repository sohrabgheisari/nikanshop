﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;
using Shop.Data;
using Shop.Services.Infrastructure;
using Shop.Core.Exceptions;

namespace Shop.Services.Shop
{
    public class CategoryService : BaseService, ICategoryService
    {
        public CategoryService(AppDbContext context) : base(context)
        {
        }

        public async Task<bool> CategoryHasChild(long catId)
        {
            var cat = Table<Category>().Where(x => x.Parent_Id == catId).FirstOrDefault();
            return cat != null ? true : false;
        }

        public Task DeleteCategory(Category entity)
        {
           
            var cat = Table<Category>().FirstOrDefault(x => x.Parent_Id == entity.Id);
            if (cat != null)
            {
                throw new NikanException("دسته دارای زیر دسته می باشد و قابل حذف نمی باشد");
            }
            else
                {
                    var products = Table<Product>().Where(x => x.CategoryId == entity.Id);
                    if (products.Any())
                    {
                        throw new NikanException("برای این دسته مصول تعریف شده است و قابل حذف نمی باشد");
                    }
                }
            
            Delete(entity);
            return Save();
        }

        public IQueryable<Category> GetAllCategoryAsQueryable()
        {
            return Table<Category>();
        }

        public IQueryable<Category> GetAllCategoryChildsAsQueryable(long parentId)
        {
            return Table<Category>().Where(x => x.Parent_Id == parentId).OrderBy(x => x.Title);
        }

        public IQueryable<Category> GetAllCategoryAsQueryable(int level)
        {
            return Table<Category>().Where(x => x.Level == level).OrderBy(x => x.Title);
        }

        public IQueryable<Category> GetAllRootCategoryAsQueryable()
        {
            return Table<Category>().Where(x => x.Parent_Id == null).OrderBy(x => x.Title);
        }

        public Task<Category> GetCategoryById(long entityId)
        {
            return GetById<Category>(entityId);
        }

        public async Task<int> GetLevel(long parentId)
        {
            long level = 0;
            while (parentId != 0)
            {
                long parent_Id = Getparent(parentId);
                if (parent_Id > 0)
                {
                    level++;
                }
                parentId = parent_Id;
            }
            return (int)level + 1;
        }

        public Task InsertCategory(Category entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateCategory(Category entity)
        {
            Update(entity);
            return Save();
        }

        private long Getparent(long id)
        {
            var cat = GetById<Category>(id).Result;
            if (cat != null)
            {
                if (cat.Parent_Id.HasValue)
                {
                    return cat.Parent_Id.Value;
                }
            }
            return 0;
        }

        public IQueryable<Category> GetMenuCategoryAsQueryable()
        {
            return Table<Category>().Where(x => x.Level == 1 && x.AddToMenu==true).OrderBy(x => x.Title);

        }

        public async Task<string> GetFullPathName(long catId)
        {
            var cat =await GetCategoryById(catId);
            if (cat.Level == 1)
            {
                return cat.Title;
            }
            else if (cat.Level == 2)
            {
                string parent = Table<Category>().FirstOrDefault(x => x.Id == cat.Parent_Id.Value).Title;
                return parent + " / " + cat.Title;
            }
            else if (cat.Level == 3)
            {
                var parentL2 = Table<Category>().FirstOrDefault(x => x.Id == cat.Parent_Id.Value);
                string level2 = parentL2.Title;
                string level1 = Table<Category>().FirstOrDefault(x => x.Id == parentL2.Parent_Id.Value).Title;
                return level1 + " / " + level2 + " / " + cat.Title;
            }
            return "";
        }
    }
}

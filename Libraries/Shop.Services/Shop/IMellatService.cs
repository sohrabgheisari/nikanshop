﻿using Shop.Services.Dto.Getway;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IMellatService
    {
        Task<MellatPaymentResult> Payment(long orderId, string callBackUrl);
        Task<MellatPaymentResult> VerifyPayment(string RefId, string ResCode, string SaleOrderId, string SaleRefrenceId);
    }
}

﻿using NikanBase.Core.Domain.Shop;
using System;
using System.Linq;
using System.Threading.Tasks;
using Shop.Data;
using Shop.Services.Infrastructure;

namespace Shop.Services.Shop
{
    public class DiscountService : BaseService, IDiscountService
    {
        public DiscountService(AppDbContext context) : base(context)
        {
        }

        public Task DeleteDiscount(Discount entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<Discount> GetAllDescountAsQueryable()
        {
            return Table<Discount>();
        }

        public IQueryable<Discount> GetAllValidDescountAsQueryable()
        {
            return Table<Discount>().Where(x => (x.DiscountTo.HasValue && x.DiscountFrom.HasValue && DateTime.Now.Date >= x.DiscountFrom.Value.Date && DateTime.Now.Date <= x.DiscountTo.Value.Date)||
            (!x.DiscountTo.HasValue && !x.DiscountFrom.HasValue) ||(!x.DiscountTo.HasValue && x.DiscountFrom.HasValue && DateTime.Now.Date >= x.DiscountFrom.Value.Date));
        }

        public Task<Discount> GetDiscountById(long entityId)
        {
            return GetById<Discount>(entityId);
        }

        public Task InsertDiscount(Discount entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateDiscount(Discount entity)
        {
            Update(entity);
            return Save();
        }
    }
}

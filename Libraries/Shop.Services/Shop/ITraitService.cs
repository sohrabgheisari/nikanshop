﻿using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface ITraitService
    {
        IQueryable<Trait> GetAllTraitAsQueryable();
        IQueryable<Trait> GetAllTraitAsQueryable(TraitTypes type);
        Task InsertTrait(Trait entity);
        Task UpdateTrait(Trait entity);
        Task DeleteTrait(Trait entity);
        Task<Trait> GetTraitById(long entityId);
    }
}

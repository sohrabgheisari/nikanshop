﻿using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using Shop.Services.Dto.Brand;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IBrandService
    {
        IQueryable<Brand> GetAllAsQueryable();
        Task InsertBrand(BrandModel model, IFormFile ImageUrl);
        Task UpdateBrand(BrandModel model, IFormFile ImageUrl);
        Task DeleteBrand(long entityId);
        Task<Brand> GetBrandById(long entityId);
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Dto.Brand;
using Shop.Services.Infrastructure;
using Shop.Services.Infrastructure.Mapping;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class BrandService : BaseService, IBrandService
    {
        private readonly IHostingEnvironment _environment;
        private string ImagePath = "ShopImage/Brand";
        public BrandService(AppDbContext context, IHostingEnvironment environment) : base(context)
        {
            _environment = environment;
        }

        public async Task DeleteBrand(long entityId)
        {
            var entity = await GetBrandById(entityId);
            DeleteImage(entity);
            Delete(entity);
            await Save();
        }

        public IQueryable<Brand> GetAllAsQueryable()
        {
            return Table<Brand>();
        }

        public Task<Brand> GetBrandById(long entityId)
        {
            return GetById<Brand>(entityId);
        }

        public async Task InsertBrand(BrandModel model, IFormFile ImageUrl)
        {
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            //تصویر اصلی
            if (ImageUrl != null && ImageUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                    model.ImageUrl = "/" + ImagePath + "/" + newFilename;
                };
            }
            var entity = Mapping.Mapper.Map<Brand>(model);
            Insert(entity);
            await Save();
        }

        public async Task UpdateBrand(BrandModel model, IFormFile ImageUrl)
        {
            var entity = await GetBrandById(model.Id);
            var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
            if (!Directory.Exists(uploadsRootFolder))
            {
                Directory.CreateDirectory(uploadsRootFolder);
            }
            //تصویر اصلی
            if (ImageUrl != null && ImageUrl.Length > 0)
            {
                string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                using (var FileStream = new FileStream(FilePath, FileMode.Create))
                {
                    await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                    model.ImageUrl = "/" + ImagePath + "/" + newFilename;
                    DeleteImage(entity);
                };
            }
            else
            {
                model.ImageUrl = entity.ImageUrl;
            }
            var updatedEntity = Mapping.Mapper.Map<Brand>(model);
            Update(updatedEntity);
            await Save();
        }

        #region Utilities
        private void DeleteImage(Brand entity)
        {
            var mainPath = _environment.WebRootPath + entity.ImageUrl;
            if (File.Exists(mainPath))
            {
                File.Delete(mainPath);
            }
        }
        #endregion
    }
}

﻿using NikanBase.Core.Domain.Shop;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IGalleryService
    {
        IQueryable<Gallery> GetAllGalleries();
        Task InsertGallery(Gallery entity);
        Task DeleteGallery(Gallery entity);
        Task UpdateGallery(Gallery entity);
        Task<Gallery> GetGalleryById(long entityId);
    }
}

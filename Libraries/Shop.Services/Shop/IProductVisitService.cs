﻿using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IProductVisitService
    {
        Task InsertProductVisit(long userId, long productId);
    }
}

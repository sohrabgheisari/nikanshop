﻿using NikanBase.Core.Domain.Shop;
using Shop.Core.Enums;
using Shop.Services.Dto.Product;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public interface IOrderService
    {
        IQueryable<Order> GetAllOrderAsQueryable(long? userId, OrderSearchModel model);
        Task<List<ShopCardModel>> InsertOrder(List<ShopCardModel> card, string address);
        Task<List<ShopCardModel>> GetLastRegisterdOrder(long userId);
        Task<List<ShopCardModel>> GetCardOrder(long orderId);
        Task<List<ShopCardModel>> GetCardOrder(long orderId,long userId);
        Task DeleteOrderItem(OrderItem entity);
        Task DeleteOrder(Order entity);
        Task InsertOrderItem(OrderItem entity);
        Task<OrderItem> GetOrderItemById(long entityId);
        Task<Order> GetOrderById(long entityId);
        Task<Order> GetOrderByFactorNumber(string facNumber);
        Task<CardViewModel> GetCard();
        Task<CardViewModel> VerifyCard(long orderId);
        Task<int> GetCardCount();
        Task<int> AddToCard(long id);
        Task<int> AddToCard(long id,long traitId);
        Task<bool> PlusToCard(long id);
        Task<bool> PlusToCard(long id, long traitId);
        Task<bool> MinesToCard(long id);
        Task<bool> MinesToCard(long id, long traitId);
        Task<bool> RemoveFromCard(long id);
        Task UpdateOrderItem(OrderItem entity);
        Task UpdateOrder(Order entity);
        Task UpdateCardOrder(OrderItem item, Order order);
        Task DeleteFromCardOrder(OrderItem item, Order order);
        Task UpdateOrderPayment(Order order);
    }
}

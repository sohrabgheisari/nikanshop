﻿using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class ProductVisitService : BaseService, IProductVisitService
    {
        public ProductVisitService(AppDbContext context) : base(context)
        {
        }

        public Task InsertProductVisit(long userId, long productId)
        {
            var visit = _context.Set<ProductVisit>().FirstOrDefault(x => x.UserId == userId && x.ProductId == productId);
            if (visit!=null)
            {
                visit.LastVisitDate = DateTime.Now;
                Update(visit);
                return Save();
            }
            else
            {
                Insert(new ProductVisit() { ProductId=productId,UserId=userId,LastVisitDate=DateTime.Now});
                return Save();
            }
        }
    }
}

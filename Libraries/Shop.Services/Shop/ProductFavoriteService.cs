﻿using Microsoft.EntityFrameworkCore;
using NikanBase.Core.Domain.Shop;
using Shop.Core.Infrastructure;
using Shop.Data;
using Shop.Services.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class ProductFavoriteService : BaseService, IProductFavoriteService
    {
        private readonly IAppContext _appContext;
        public ProductFavoriteService(AppDbContext context, IAppContext appContext) : base(context)
        {
            _appContext = appContext;
        }

        public Task DeleteFav(long entityId)
        {
            Delete(new ProductFavorite() { Id = entityId });
            return Save();
        }

        public IQueryable<ProductFavorite> GetAllAsQueryable()
        {
            return Table<ProductFavorite>().Where(x => x.UserId == _appContext.User.Id)
                .Include(x => x.Product).ThenInclude(x=>x.Category)
                .Include(x => x.Product).ThenInclude(x=>x.Brand)
                .Include(x => x.Product).ThenInclude(x=>x.Discount)
                .Include(x => x.Product).ThenInclude(x=>x.ProductTraits);
        }

        public async Task<long> HasFav(long productId)
        {
            var fav = Table<ProductFavorite>().FirstOrDefault(x => x.ProductId == productId && x.UserId == _appContext.User.Id);
            return fav!=null ? fav.Id : 0;
        }

        public Task InsertFav(ProductFavorite entity)
        {
            Insert(entity);
            return Save();
        }
    }
}

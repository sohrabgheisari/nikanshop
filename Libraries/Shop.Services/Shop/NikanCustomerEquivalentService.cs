﻿using NikanBase.Core.Domain.Shop;
using Shop.Core.Exceptions;
using Shop.Data;
using Shop.Services.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class NikanCustomerEquivalentService : BaseService, INikanCustomerEquivalentService
    {
        public NikanCustomerEquivalentService(AppDbContext context) : base(context)
        {
        }

        public async Task<NikanCustomerEquivalent> GetCustomerEquivalentById(long userId)
        {
            return Table<NikanCustomerEquivalent>().FirstOrDefault(x => x.ShopUserId == userId);
        }

        public Task InsertCustomerEquivalent(NikanCustomerEquivalent entity)
        {
            var eq = Table<NikanCustomerEquivalent>().FirstOrDefault(x => x.ShopUserId == entity.ShopUserId || x.NikanCustomerId == entity.NikanCustomerId);
            if (eq == null)
            {
                Insert(entity);
                return Save();
            }
            throw new NikanException("معال سازی این کاربر قبلا انجام گردیده است");
        }

        public Task UpdateCustomerEquivalent(NikanCustomerEquivalent entity)
        {
            var eq = Table<NikanCustomerEquivalent>().FirstOrDefault(x => x.Id != entity.Id && (x.ShopUserId == entity.ShopUserId || x.NikanCustomerId == entity.NikanCustomerId));
            if (eq == null)
            {
                Update(entity);
                return Save();
            }
            throw new NikanException("معال سازی این محصول قبلا انجام گردیده است");
        }
    }
}

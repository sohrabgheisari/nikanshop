﻿using NikanBase.Core.Domain.Shop;
using Shop.Data;
using Shop.Services.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class PropertyCategoryService : BaseService, IPropertyCategoryService
    {
        public PropertyCategoryService(AppDbContext context) : base(context)
        {
        }

        public Task DeletePropertyCategory(PropertyCategory entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<PropertyCategory> GetAllAsQueryable()
        {
            return Table<PropertyCategory>();
        }

        public Task<PropertyCategory> GetPropertyCategoryById(long entityId)
        {
            return GetById<PropertyCategory>(entityId);
        }

        public Task InsertPropertyCategory(PropertyCategory entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdatePropertyCategory(PropertyCategory entity)
        {
            Update(entity);
            return Save();
        }
    }
}

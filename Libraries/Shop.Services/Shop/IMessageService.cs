﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.Core.Domain.Shop;

namespace Shop.Services.Shop
{
   public interface IMessageService
    {
        IQueryable<Message> GetAllMessagesAsQueryable();
        Task<List<Message>> GetAllMessages();
        Task<Message> GetMessageById(long entityId);
        Task InsertMessage(Message entity);
        Task UpdateMessage(Message entity);
        Task DeleteMessage(Message entity);

        Task ArchiveMessage(long entityId, Boolean flag);
        Task MessageIsRead(long entityId);
        Task MessageIsReply(long entityId);
    }
}

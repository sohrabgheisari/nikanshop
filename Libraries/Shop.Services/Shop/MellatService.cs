﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Data;
using Shop.Services.Dto.Getway;
using Shop.Services.Dto.Setting;
using Shop.Services.Infrastructure;
using System;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class MellatService : BaseService, IMellatService
    {
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;
        private readonly IAppContext _appContext;
        private readonly ILogger _logger;
        private readonly IHttpContextAccessor _httpContext;

        public MellatService(AppDbContext context,
            ISettingService settingService,
            IOrderService orderService,
             IAppContext appContext,
             IHttpContextAccessor httpContext,
              ILogger<MellatService> logger) : base(context)
        {
            _settingService = settingService;
            _orderService = orderService;
            _appContext = appContext;
            _logger = logger;
            _httpContext = httpContext;
        }

        public async Task<MellatPaymentResult> Payment(long orderId, string callBackUrl)
        {
            MellatPaymentResult result = new MellatPaymentResult() { IsSuccess = false };
            var order = await _orderService.GetOrderById(orderId);
            if (order == null)
            {
                result.IsSuccess = false;
                result.ResCode = "802";
                result.Message = MellatMessageDictionary.GetMessage(802);
                return result;
            }
            if (order.UserId != _appContext.User.Id)
            {
                result.IsSuccess = false;
                result.ResCode = "801";
                result.Message = MellatMessageDictionary.GetMessage(801);
                return result;
            }
            if (order.Status == StatusTypes.SuccessfulPayment)
            {
                result.IsSuccess = false;
                result.ResCode = "803";
                result.Message = MellatMessageDictionary.GetMessage(803);
                return result;
            }
            var setting = _settingService.Load<MellatGetwaySettingModel>(null);
            if (setting!= null  && !String.IsNullOrEmpty(setting.Password) && !String.IsNullOrEmpty(setting.UserName) && !String.IsNullOrEmpty(setting.TerminalId))
            {
                var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
                int amount = priceUnitSetting != null ? (priceUnitSetting.Unit == PriceUnitTypes.Tooman ? (int)order.PayableAmount * 10 : (int)order.PayableAmount) : ((int)order.PayableAmount);
                try
                {
                    BypassCertificateError();
                    Shaparak.PaymentGatewayClient bp = new Shaparak.PaymentGatewayClient();
                    var res = await bp.bpPayRequestAsync(Int64.Parse(setting.TerminalId), setting.UserName, setting.Password, order.Id, amount, SetDefaultDate(), SetDefaultTime(), "", callBackUrl, "0");
                    string[] resCodes = res.Body.@return.Split(',');
                    if (resCodes[0] == "0")
                    {
                        result.IsSuccess = true;
                        result.ResCode = resCodes[0];
                        result.RefNumber = resCodes[1];
                        result.Message = MellatMessageDictionary.GetMessage(int.Parse(resCodes[0]));
                        return result;
                    }
                    else
                    {
                        result.IsSuccess = false;
                        result.ResCode = resCodes[0];
                        result.Message = MellatMessageDictionary.GetMessage(int.Parse(resCodes[0]));
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError($"خطا در ارتباط با بانک ملت message:{ex.Message}");
                    result.IsSuccess = false;
                    result.ResCode = "804";
                    result.Message = MellatMessageDictionary.GetMessage(804);
                    return result;
                }
       
            }
            else
            {
                result.IsSuccess = false;
                result.ResCode = "800";
                result.Message = MellatMessageDictionary.GetMessage(800);
                return result;
            }
        }

        public async Task<MellatPaymentResult> VerifyPayment(string RefId, string ResCode, string SaleOrderId, string SaleRefrenceId)
        {
            MellatPaymentResult result = new MellatPaymentResult() { IsSuccess = false };
            var setting = _settingService.Load<MellatGetwaySettingModel>(null);
            var order = _context.Set<Order>().FirstOrDefault(x => x.Id == long.Parse(SaleOrderId));
            if (setting==null || String.IsNullOrEmpty(setting.Password) || String.IsNullOrEmpty(setting.UserName) || String.IsNullOrEmpty(setting.TerminalId))
            {
                result.ResCode = "800";
                result.Message = MellatMessageDictionary.GetMessage(800);
                return result;
            }
            try
            {
                if (ResCode == "0")
                {
                    BypassCertificateError();
                    Shaparak.PaymentGatewayClient bp = new Shaparak.PaymentGatewayClient();
                    var res = await bp.bpVerifyRequestAsync(Int64.Parse(setting.TerminalId), setting.UserName, setting.Password, long.Parse(SaleOrderId), long.Parse(SaleOrderId), long.Parse(SaleRefrenceId));
                    string[] resCodes = res.Body.@return.Split(',');
                    if (resCodes[0] == "0")
                    {
                        if (order!=null)
                        {
                            if (_appContext.User.Id==order.UserId)
                            {
                                order.Status = StatusTypes.SuccessfulPayment;
                                order.TransactionCode = RefId;
                                order.TransactionNumber = SaleRefrenceId;
                                order.PaidAt = DateTime.Now;
                                order.Getway = BankGatewayTypes.Mellat;
                                order.ShippingStatus = ShippingSatusTypes.NotShipped;
                                try
                                {
                                    await _orderService.UpdateOrderPayment(order);
                                }
                                catch (Exception ex)
                                {
                                    var msg = $"Mellat VerifyPayment Method(OrderId-{order.Id}) : Verification Succeeded," +
                                            $"StatusCode-{res.Body.@return},SaleRefrenceId-{SaleRefrenceId},RefId={ RefId}";
                                    _logger.LogError(msg + $".But PayOrder method Failed With Message-{ex.Message}");
                                }
                                result.IsSuccess = true;
                                result.ResCode = resCodes[0];
                                result.Message = MellatMessageDictionary.GetMessage(int.Parse(resCodes[0]));
                                result.TransactionNumber = SaleRefrenceId;
                                result.RefNumber = RefId;
                                _httpContext.HttpContext.Session.Remove("ShopCart");
                                return result;
                            }
                            else
                            {
                                result.ResCode = "801";
                                result.Message = MellatMessageDictionary.GetMessage(801);
                                return result;
                            }
                        }
                        else
                        {
                            result.ResCode = "802";
                            result.Message = MellatMessageDictionary.GetMessage(802);
                            return result;
                        }
                    }
                    else
                    {
                        result.ResCode = resCodes[0];
                        result.Message = MellatMessageDictionary.GetMessage(int.Parse(resCodes[0]));
                        return result;
                    }
                }
                else
                {
                    order.Status = StatusTypes.UnsuccessfulPayment;
                    order.Getway = BankGatewayTypes.Mellat;
                    await _orderService.UpdateOrder(order);
                    result.ResCode = ResCode;
                    result.Message = MellatMessageDictionary.GetMessage(int.Parse(ResCode));
                    return result;
                }
            }
            catch (Exception ex)
            {

                _logger.LogError($"خطا در پرداخت بانک ملت message:{ex.Message}");
                result.ResCode = "804";
                result.Message = MellatMessageDictionary.GetMessage(804);
                return result;
            }
        }


        #region Utilities

        private  void BypassCertificateError()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate (
                    Object sender1,
                    X509Certificate certificate,
                    X509Chain chain,
                    SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
        }

        private string SetDefaultDate()
        {
            return DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0');

        }
        private string SetDefaultTime()
        {
            return DateTime.Now.Hour.ToString().PadLeft(2, '0') + DateTime.Now.Minute.ToString().PadLeft(2, '0') + DateTime.Now.Second.ToString().PadLeft(2, '0');
        }
        #endregion
    }
}

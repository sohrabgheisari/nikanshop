﻿using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using Shop.Data;
using Shop.Services.Infrastructure;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class TraitService : BaseService, ITraitService
    {
        public TraitService(AppDbContext context) : base(context)
        {
        }

        public Task DeleteTrait(Trait entity)
        {
            Delete(entity);
            return Save();
        }

        public IQueryable<Trait> GetAllTraitAsQueryable()
        {
            return Table<Trait>();
        }

        public IQueryable<Trait> GetAllTraitAsQueryable(TraitTypes type)
        {
            return Table<Trait>().Where(x=>x.Type==type);
        }

        public Task<Trait> GetTraitById(long entityId)
        {
            return GetById<Trait>(entityId);
        }

        public Task InsertTrait(Trait entity)
        {
            Insert(entity);
            return Save();
        }

        public Task UpdateTrait(Trait entity)
        {
            Update(entity);
            return Save();
        }
    }
}

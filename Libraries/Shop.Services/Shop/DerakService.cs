﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;
using Shop.Core.Exceptions;
using Shop.Data;
using Shop.Services.Dto.Derak;
using Shop.Services.Dto.Nikan;
using Shop.Services.Dto.Setting;
using Shop.Services.Infrastructure;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Shop.Services.Shop
{
    public class DerakService : BaseService, IDerakService
    {
        private readonly ISettingService _settingService;
        private readonly ILogger _logger;

        public DerakService(AppDbContext context,
            ISettingService settingService,
             ILogger<DerakService> logger) : base(context)
        {
            _settingService = settingService;
            _logger = logger;
        }

        public async Task<NikanAccountingOperationResult> CreateFactor(NikanCreateAccountingOperationHeaderModel model)
        {
            var setting = _settingService.Load<DerakSettingModel>(null);
            if (setting != null)
            {
                if (!String.IsNullOrEmpty(setting.BaseUrl) && !String.IsNullOrEmpty(setting.PublicKey))
                {
                    try
                    {
                        var derakToken = await GetToken(setting);
                        var client = new RestClient(setting.BaseUrl + "/api/v1.0/create-factor");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Authorization", "Bearer " + derakToken);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddHeader("live-data", "true");
                        request.AddJsonBody(model);
                        IRestResponse response = client.Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var content = response.Content;
                            var result = JsonConvert.DeserializeObject<DerakResult<NikanAccountingOperationResult>>(content);
                            if (result != null)
                            {
                                if (result.IsSuccess)
                                {
                                    return result.Data;
                                }
                                _logger.LogError($"ارسال فاکتور به نیکان با خطا مواجه گردید. کد خطا:{result.ErrorCode} متن خطا:{result.ErrorMessage}");
                                throw new NikanException($"ارسال فاکتور به نیکان با خطا مواجه گردید. کد خطا:{result.ErrorCode} متن خطا:{result.ErrorMessage}");
                            }
                            _logger.LogError("ارسال فاکتور به نیکان با خطا مواجه گردید");
                            throw new NikanException("ارسال فاکتور به نیکان با خطا مواجه گردید");
                        }
                        _logger.LogError("ارسال فاکتور به نیکان با خطا مواجه گردید");
                        throw new NikanException("ارسال فاکتور به نیکان با خطا مواجه گردید");
                    }
                    catch (Exception ex)
                    {

                        _logger.LogError($"ارسال فاکتور به نیکان با خطا مواجه گردید. متن خطا:{ex.Message}");
                        throw new NikanException($"ارسال فاکتور به نیکان با خطا مواجه گردید. متن خطا:{ex.Message}");
                    }
                }
                else
                {
                    _logger.LogError("تنظیمات دراک انجام نشده است");
                    throw new NikanException("تنظیمات دراک انجام نشده است");
                }
            }
            else
            {
                _logger.LogError("تنظیمات دراک انجام نشده است");
                throw new NikanException("تنظیمات دراک انجام نشده است");
            }
        }

        public async Task<List<NikanCustomer>> GetNikanCustomer()
        {
            var setting = _settingService.Load<DerakSettingModel>(null);
            if (setting != null)
            {
                if (!String.IsNullOrEmpty(setting.BaseUrl) && !String.IsNullOrEmpty(setting.PublicKey))
                {
                    try
                    {
                        var derakToken = await GetToken(setting);
                        var client = new RestClient(setting.BaseUrl + "/api/v1.0/get-customers");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Authorization", "Bearer " + derakToken);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddHeader("live-data", "true");
                        request.AddJsonBody(new { });
                        IRestResponse response = client.Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var content = response.Content;
                            var result = JsonConvert.DeserializeObject<DerakResult<List<NikanCustomer>>>(content);
                            if (result != null)
                            {
                                if (result.IsSuccess)
                                {
                                    return result.Data;
                                }
                                _logger.LogError($"دریافت مشتریان نیکان با خطا مواجه گردید. کد خطا:{result.ErrorCode} - متن خطا: {result.ErrorMessage}");
                                return new List<NikanCustomer>();
                            }
                            _logger.LogError("دریافت مشتریان نیکان با خطا مواجه گردید.");
                            return new List<NikanCustomer>();
                        }
                        _logger.LogError("دریافت مشتریان نیکان با خطا مواجه گردید.");
                        return new List<NikanCustomer>();
                    }
                    catch (Exception ex)
                    {

                        _logger.LogError($"دریافت مشتریان نیکان با خطا مواجه گردید. متن خطا:{ex.Message}");
                        return new List<NikanCustomer>();
                    }
                }
                else
                {
                    _logger.LogError("تنظیمات دراک انجام نشده است");
                    return new List<NikanCustomer>();
                }
            }
            else
            {
                _logger.LogError("تنظیمات دراک انجام نشده است");
                return new List<NikanCustomer>();
            }
        }

        public async Task<List<NikanProduct>> GetNikanProduct()
        {
            var setting = _settingService.Load<DerakSettingModel>(null);
            if (setting != null)
            {
                if (!String.IsNullOrEmpty(setting.BaseUrl) && !String.IsNullOrEmpty(setting.PublicKey))
                {
                    try
                    {
                        var derakToken = await GetToken(setting);
                        var client = new RestClient(setting.BaseUrl + "/api/v1.0/get-products");
                        var request = new RestRequest(Method.POST);
                        request.AddHeader("Authorization", "Bearer " + derakToken);
                        request.AddHeader("Content-Type", "application/json");
                        request.AddHeader("live-data", "true");
                        request.AddJsonBody(new { });
                        IRestResponse response = client.Execute(request);
                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var content = response.Content;
                            var result = JsonConvert.DeserializeObject<DerakResult<List<NikanProduct>>>(content);
                            if (result != null)
                            {
                                if (result.IsSuccess)
                                {
                                    return result.Data;
                                }
                                _logger.LogError($"دریافت محصولات نیکان با خطا مواجه گردید. کد خطا:{result.ErrorCode} - متن خطا: {result.ErrorMessage}");
                                return new List<NikanProduct>();
                            }
                            _logger.LogError("دریافت محصولات نیکان با خطا مواجه گردید.");
                            return new List<NikanProduct>();
                        }
                        _logger.LogError("دریافت محصولات نیکان با خطا مواجه گردید.");
                        return new List<NikanProduct>();
                    }
                    catch (Exception ex)
                    {

                        _logger.LogError($"دریافت محصولات نیکان با خطا مواجه گردید. متن خطا:{ex.Message}");
                        return new List<NikanProduct>();
                    }
                }
                else
                {
                    _logger.LogError("تنظیمات دراک انجام نشده است");
                    return new List<NikanProduct>();
                }
            }
            else
            {
                _logger.LogError("تنظیمات دراک انجام نشده است");
                return new List<NikanProduct>();
            }
 
        }

        public async Task<string> GetToken(DerakSettingModel setting)
        {
            if (setting!=null)
            {
                if (!String.IsNullOrEmpty(setting.BaseUrl)&& !String.IsNullOrEmpty(setting.PublicKey))
                {
                    try
                    {
                        var content = new FormUrlEncodedContent(new[] { new KeyValuePair<string, string>("apikey", setting.PublicKey) });
                        var client = new HttpClient();
                        client.BaseAddress = new Uri(setting.BaseUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var apiResponse = await client.PostAsync("api/token", content);
                        var result = JsonConvert.DeserializeObject<DerakResult<DerakTokenData>>(await apiResponse.Content.ReadAsStringAsync());
                        if (result.IsSuccess)
                        {
                            return result.Data.AccessToken;
                        }
                        _logger.LogError($"دریافت توکن دراک با خطا مواجه گردید. کد خطا:{result.ErrorCode} - متن خطا: {result.ErrorMessage}");
                        throw new NikanException($"دریافت توکن دراک با خطا مواجه گردید. کد خطا:{result.ErrorCode} - متن خطا: {result.ErrorMessage}");
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"دریافت توکن دراک با خطا مواجه گردید. متن خطا:{ex.Message}");
                        throw new NikanException($"دریافت توکن دراک با خطا مواجه گردید. متن خطا:{ex.Message}");
                    }
                }
                else
                {
                    _logger.LogError("تنظیمات دراک انجام نشده است");
                    throw new NikanException("تنظیمات دراک انجام نشده است");
                }
            }
            else
            {
                _logger.LogError("تنظیمات دراک انجام نشده است");
                throw new NikanException("تنظیمات دراک انجام نشده است");
            }

        }
    }
}

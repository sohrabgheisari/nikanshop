﻿namespace Shop.Core.Domain.Users
{
    public partial class UserRoles
    {
        public long UserId { get; set; }
        public long RoleId { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}

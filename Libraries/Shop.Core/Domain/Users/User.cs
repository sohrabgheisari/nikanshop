﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using NikanBase.Core.Domain.Shop;

namespace Shop.Core.Domain.Users
{
    public class User : IdentityUser<long>
    {
        public User()
        {
            UserRoles = new List<IdentityUserRole<long>>();
            UserRoleNames = new List<string>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public bool IsActive { get; set; }
        public virtual List<IdentityUserRole<long>> UserRoles { get; set; }
        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<ProductVisit> ProductVisits { get; set; }
        public virtual NikanCustomerEquivalent NikanCustomerEquivalent { get; set; }
        public virtual ICollection<ProductFavorite> Favorites { get; set; }

        [NotMapped]
        public List<string> UserRoleNames { get; set; }

        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

    }
}

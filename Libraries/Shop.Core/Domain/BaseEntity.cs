﻿namespace Shop.Core.Domain
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}

﻿using System;

namespace Shop.Core.Domain.Shop
{
    public partial class Message:BaseEntity
    {
        public Message()
        {
        }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public Boolean IsRead { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime? ReadDate { get; set; }
        public Boolean? IsArchived { get; set; }
    }
}

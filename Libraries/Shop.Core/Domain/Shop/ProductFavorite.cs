﻿using Shop.Core.Domain;
using Shop.Core.Domain.Users;

namespace NikanBase.Core.Domain.Shop
{
    public class ProductFavorite:BaseEntity
    {
        public long UserId { get; set; }
        public long ProductId { get; set; }
        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
    }
}

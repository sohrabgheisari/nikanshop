﻿using System.Collections.Generic;
using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class Category:BaseEntity
    {
        public Category()
        {

        }
        public long? Parent_Id { get; set; }
        public int Level { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public bool AddToMenu { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}

﻿using NikanBase.Core.Enums;
using Shop.Core.Domain;
using Shop.Core.Domain.Users;
using Shop.Core.Enums;
using System;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class Order:BaseEntity
    {
        public Order()
        {

        }
        public string Number { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? PaidAt { get; set; }
        public decimal PayableAmount { get; set; }
        public long UserId { get; set; }
        public StatusTypes Status { get; set; }
        public BankGatewayTypes? Getway { get; set; }
        public ShippingSatusTypes? ShippingStatus { get; set; }
        public string TransactionNumber { get; set; }
        public string TransactionCode { get; set; }
        public string Address { get; set; }
        public bool SendToNikan { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
    }
}

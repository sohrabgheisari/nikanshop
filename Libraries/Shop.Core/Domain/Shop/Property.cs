﻿using Shop.Core.Domain;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class Property:BaseEntity
    {
        public Property()
        {

        }
        public string Name { get; set; }
        public long PropertCategoryId { get; set; }
        public virtual PropertyCategory PropertyCategory { get; set; }
        public virtual ICollection<ProductProperty> ProductProperties { get; set; }
    }
}

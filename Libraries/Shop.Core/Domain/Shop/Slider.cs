﻿using Shop.Core.Domain;
using Shop.Core.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace NikanBase.Core.Domain.Shop
{
    public class Slider:BaseEntity
    {
        public Slider()
        {

        }
        public string SliderUrl { get; set; }
        public int Order { get; set; }
        public SliderTypes Type { get; set; }
    }
}

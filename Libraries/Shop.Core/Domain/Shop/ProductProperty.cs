﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class ProductProperty:BaseEntity
    {
        public ProductProperty()
        {

        }
        public long ProductId { get; set; }
        public long PropertyId { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
        public virtual Product Product { get; set; }
        public virtual Property Property{ get; set; }
    }
}

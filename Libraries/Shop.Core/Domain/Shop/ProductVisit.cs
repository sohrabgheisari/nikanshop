﻿using Shop.Core.Domain;
using Shop.Core.Domain.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace NikanBase.Core.Domain.Shop
{
    public class ProductVisit:BaseEntity
    {
        public ProductVisit()
        {

        }
        public long UserId { get; set; }
        public long ProductId { get; set; }
        public DateTime LastVisitDate { get; set; }
        public virtual User User { get; set; }
        public virtual Product Product { get; set; }
    }
}

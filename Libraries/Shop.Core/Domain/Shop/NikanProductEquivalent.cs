﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class NikanProductEquivalent:BaseEntity
    {
        public long ShopProductId { get; set; }
        public int NikanProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}

﻿using Shop.Core.Domain;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class Brand : BaseEntity
    {
        public Brand()
        {

        }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}

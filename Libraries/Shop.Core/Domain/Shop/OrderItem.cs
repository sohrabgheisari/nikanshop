﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class OrderItem:BaseEntity
    {
        public OrderItem()
        {

        }
        public long ProductId { get; set; }
        public long OrderId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal PriceWithDiscount { get; set; }
        public decimal TraitPrice { get; set; }
        public decimal PriceIncrease { get; set; }
        public long? ProductTraitId { get; set; }
        public virtual Product Product { get; set; }
        public virtual Order Order { get; set; }
        public virtual ProductTrait ProductTrait { get; set; }
    }
}

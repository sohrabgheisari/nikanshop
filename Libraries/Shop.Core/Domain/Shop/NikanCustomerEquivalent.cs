﻿using Shop.Core.Domain;
using Shop.Core.Domain.Users;

namespace NikanBase.Core.Domain.Shop
{
    public class NikanCustomerEquivalent : BaseEntity
    {
        public long ShopUserId { get; set; }
        public int NikanCustomerId { get; set; }
        public virtual User User { get; set; }
    }
}

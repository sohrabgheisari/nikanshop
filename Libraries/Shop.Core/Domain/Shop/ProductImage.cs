﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class ProductImage:BaseEntity
    {
        public ProductImage()
        {

        }
        public long ProductId { get; set; }
        public string ImageUrl { get; set; }
        public int Order { get; set; }
        public virtual Product Product { get; set; }
    }
}

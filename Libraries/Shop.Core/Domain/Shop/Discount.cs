﻿using System;
using System.Collections.Generic;
using Shop.Core.Domain;
using Shop.Core.Enums;

namespace NikanBase.Core.Domain.Shop
{
    public class Discount:BaseEntity
    {
        public Discount()
        {

        }
        public string Tittle { get; set; }
        public DiscountTypes Type { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Percentage { get; set; }
        public DateTime? DiscountFrom { get; set; }
        public DateTime? DiscountTo { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}

﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class Gallery:BaseEntity
    {
        public Gallery()
        {

        }
        public string MainImageUrl { get; set; }
        public string MobileImageUrl { get; set; }
        public int DisplayOrder { get; set; }
        public long? ProductId { get; set; }
        public virtual Product Product { get; set; }
    }
}

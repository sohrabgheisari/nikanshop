﻿using System;
using System.Collections.Generic;
using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class Product:BaseEntity
    {
        public Product()
        {

        }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public bool Publish { get; set; }
        public bool SpecialProduct { get; set; }
        public bool NewProduct { get; set; }
        public decimal Price { get; set; }
        public int Inventory { get; set; }
        public string ImageUrl { get; set; }
        public DateTime InsertDate { get; set; }
        public long? DiscountId { get; set; }
        public int MinCart { get; set; }
        public int MaxCart { get; set; }
        public string BarCode { get; set; }
        public long? BrandId { get; set; }
        public decimal Score { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Category Category { get; set; }
        public virtual Discount Discount { get; set; }
        public virtual ICollection<ProductImage> ProductImages { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual ICollection<ProductProperty> ProductProperties { get; set; }
        public virtual ICollection<ProductVisit> ProductVisits { get; set; }
        public virtual ICollection<SimilarProduct> SimilarProduct_Product { get; set; }
        public virtual ICollection<SimilarProduct> SimilarProduct_Similar { get; set; }
        public virtual ICollection<ProductTrait> ProductTraits { get; set; }
        public virtual ICollection<Gallery> Galleries { get; set; }
        public virtual ICollection<ProductFavorite> Favorites { get; set; }
        public virtual NikanProductEquivalent NikanProductEquivalent { get; set; }

    }
}

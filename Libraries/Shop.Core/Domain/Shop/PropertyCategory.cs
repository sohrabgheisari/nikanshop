﻿using Shop.Core.Domain;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class PropertyCategory:BaseEntity
    {
        public PropertyCategory()
        {

        }
        public string Name { get; set; }
        public virtual ICollection<Property> Properties { get; set; }
    }
}

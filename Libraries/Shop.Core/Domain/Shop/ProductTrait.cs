﻿using Shop.Core.Domain;
using System;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class ProductTrait:BaseEntity
    {
        public long ProductId { get; set; }
        public long? ColorTriatId { get; set; }
        public long? WarrantyTriatId { get; set; }
        public DateTime? WarrantyDate { get; set; }
        public int Inventory { get; set; }
        public decimal PriceIncrease { get; set; }
        public bool IsActive { get; set; }
        public int MinimumCart { get; set; }
        public int MaximumCart { get; set; }
        public string BarCode { get; set; }
        public virtual Product Product { get; set; }
        public virtual Trait ColorTrait { get; set; }
        public virtual Trait WarrantyTrait { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }

    }
}

﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class SimilarProduct:BaseEntity
    {
        public SimilarProduct()
        {

        }
        public long ProductId { get; set; }
        public long SimilarProductId { get; set; }
        public virtual Product Product { get; set; }
        public virtual Product ProductSimilar { get; set; }
    }
}

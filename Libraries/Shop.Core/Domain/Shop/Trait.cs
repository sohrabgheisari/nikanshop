﻿using NikanBase.Core.Enums;
using Shop.Core.Domain;
using System.Collections.Generic;

namespace NikanBase.Core.Domain.Shop
{
    public class Trait:BaseEntity
    {
        public Trait()
        {

        }
        public TraitTypes Type { get; set; }
        public string Title { get; set; }
        public virtual ICollection<ProductTrait> ProductTraits_Color { get; set; }
        public virtual ICollection<ProductTrait> ProductTraits_Warranty { get; set; }

    }
}

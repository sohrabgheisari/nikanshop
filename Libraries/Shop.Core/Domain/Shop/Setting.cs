﻿using Shop.Core.Domain;

namespace NikanBase.Core.Domain.Shop
{
    public class Setting:BaseEntity
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public long? SourceId { get; set; }
    }
}

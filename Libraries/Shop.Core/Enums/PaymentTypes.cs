﻿using System.ComponentModel.DataAnnotations;

namespace NikanBase.Core.Enums
{
    public enum PaymentTypes : byte
    {
        [Display(Name = "زرین پال")]
        ZarinPal = 1,

        [Display(Name = "ملت")]
        Mellat = 2,
    }
}

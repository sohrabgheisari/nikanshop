﻿using System.ComponentModel.DataAnnotations;

namespace NikanBase.Core.Enums
{
    public enum PriceUnitTypes : byte
    {
        [Display(Name = "ریال")]
        Rial = 1,
        [Display(Name = "تومان")]
        Tooman = 2,
    }
}

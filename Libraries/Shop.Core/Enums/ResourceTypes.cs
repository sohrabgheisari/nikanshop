﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Enums
{
    public enum ResourceTypes : byte
    {
        [Display(Name = "جدیدترین محصولات")]
        New = 1,

        [Display(Name = "محصولات ویژه")]
        Special = 2,

        [Display(Name = "پرفروش ترین ها بر اساس تعداد")]
        BestSellersCount = 3,

        [Display(Name = "پرفروش ترین ها بر اساس قیمت")]
        BestSellersPrice = 4,

        [Display(Name = "آخرین بازدید ها")]
        LastVisit = 5,
    }
}

﻿namespace Shop.Core.Enums
{
    public class UserRoleNames
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}

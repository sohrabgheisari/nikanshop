﻿using System.ComponentModel.DataAnnotations;

namespace NikanBase.Core.Enums
{
    public enum TraitTypes : byte
    {
        [Display(Name = "رنگ")]
        Color = 1,

        [Display(Name = "گارانتی")]
        Warranty = 2,
    }
}

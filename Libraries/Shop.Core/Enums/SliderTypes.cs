﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Enums
{
    public enum SliderTypes : byte
    {
        [Display(Name = "دوتکه-راست")]
        TwoSlideRight= 1,

        [Display(Name = "دوتکه-چپ")]
        TwoSlideLeft = 2, 
        
        [Display(Name = "دوتکه-بالا")]
        TwoSlideUp= 3,

        [Display(Name = "دوتکه-پایین")]
        TwoSlideDown = 4,

        [Display(Name = "تبلیغاتی")]
        AdvertisingSlider = 5,
    }
}

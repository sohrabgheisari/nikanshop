﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Enums
{
    public enum StatusTypes : byte
    {
        [Display(Name = "باز")]
        Registered = 1,

        [Display(Name = "پرداخت ناموفق")]
        UnsuccessfulPayment = 2,
        
        [Display(Name = "پرداخت موفق")]
        SuccessfulPayment = 3,

        [Display(Name = "لغو شده")]
        Deleted = 4,
    }
}

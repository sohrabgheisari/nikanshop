﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Enums
{
    public enum ShippingSatusTypes : byte
    {
        [Display(Name = "ارسال نشده")]
        NotShipped = 1,

        [Display(Name = "ارسال شده")]
        Shipped = 2,
    }
}

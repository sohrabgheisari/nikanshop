﻿using System.ComponentModel.DataAnnotations;

namespace NikanBase.Core.Enums
{
    public enum ZarinPalTypes : byte
    {
        [Display(Name = "زرین پال")]
        ZarinPal = 1,
        [Display(Name = "تست زرین پال")]
        ZarinPalSandBox = 2,
    }
}

﻿using System.ComponentModel.DataAnnotations;

namespace Shop.Core.Enums
{
    public enum DiscountTypes : byte
    {
        [Display(Name = "درصدی")]
        Percentage= 1,

        [Display(Name = "مبلغی")]
        Amount= 2,
    }
}

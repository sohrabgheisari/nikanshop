﻿using System.ComponentModel.DataAnnotations;

namespace NikanBase.Core.Enums
{
    public enum SortTypes : byte
    {
        [Display(Name = "جدیدترین")]
        Newest = 1,

        [Display(Name = "ارزان ترین")]
        Cheapest = 2,

        [Display(Name = "گران ترین")]
        Expensive = 3,

        [Display(Name = "پربازدید ترین")]
        MostVisited = 4,

        [Display(Name = "محبوب ترین")]
        Popular = 5,
        
        [Display(Name = "پرفروشترین")]
        BestSelling = 6,
    }
}

﻿using System.Data.Common;
using System.Data.SqlClient;
using Shop.Core.Data;

namespace Shop.Core.Exceptions
{
    public class NikanDbException : NikanException
    {
        public NikanDbException() : base()
        {

        }

        public NikanDbException(string message) : base(message)
        {

        }

        public NikanDbException(string message, DbException innerException) : base(message, innerException)
        {
            if (innerException is SqlException ex)
            {
                HResult = ex.Number;
            }
            else
            {
                HResult = innerException.ErrorCode;
            }
        }

        public NikanDbException(string message, int errorCode) : base(message)
        {
            HResult = errorCode;
        }

        public DatabaseErrorTypes ErrorCode
        {
            get => (DatabaseErrorTypes)HResult;
            set => HResult = (int)value;
        }
    }
}

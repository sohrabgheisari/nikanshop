﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;

namespace Shop.Core.Infrastructure
{
    public static class CommonHelper
    {
        public static string NormalizeString(params string[] strings)
        {
            strings = strings?.Select(x => x?.ToUpper().Trim())
                              .Where(x => !string.IsNullOrWhiteSpace(x))
                              .ToArray();

            if (strings?.Length > 0)
            {
                return string.Join(" ", strings);
            }

            return string.Empty;
        }

        public static string RemoveNewLine(this string value)
        {
          return  Regex.Replace(value, @"\t|\n|\r", "");
        }

        public static T ParseEnum<T>(string value, T defaultValue) where T : struct, IConvertible
        {
            if (!typeof(T).IsEnum)
            {
                throw new ArgumentException("T must be an enumerated type");
            }

            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            foreach (T item in Enum.GetValues(typeof(T)))
            {
                if (item.ToString().ToLower().Equals(value.Trim().ToLower())) return item;
            }

            return defaultValue;
        }
        public static string ToDescription(this Enum value)
        {
            var attributes = (DescriptionAttribute[])value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString();
        }
        public static string EnumToDescription(this Enum value)
        {

            return value.GetType()
                       .GetMember(value.ToString())
                       .First()
                       .GetCustomAttribute<DisplayAttribute>()
                       .GetName();
        }
        public static bool AreCharsNumber(string str, params char[] exceptions)
        {
            if (string.IsNullOrEmpty(str))
            {
                return false;
            }

            exceptions = exceptions ?? new char[] { };

            foreach (var c in str)
            {
                if (!char.IsDigit(c) && !exceptions.Any(x => x == c))
                {
                    return false;
                }
            }

            return true;
        }

        public static string GetRandomAlphanumericString(int length)
        {
            const string alphanumericCharacters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
                "abcdefghijklmnopqrstuvwxyz" +
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        public static string GetRandomNumericString(int length)
        {
            const string alphanumericCharacters =
                "0123456789";
            return GetRandomString(length, alphanumericCharacters);
        }

        public static string GetRandomString(int length, IEnumerable<char> characterSet)
        {
            if (length < 0)
                throw new ArgumentException("length must not be negative", "length");
            if (length > int.MaxValue / 8) // 250 million chars ought to be enough for anybody
                throw new ArgumentException("length is too big", "length");
            if (characterSet == null)
                throw new ArgumentNullException("characterSet");
            var characterArray = characterSet.Distinct().ToArray();
            if (characterArray.Length == 0)
                throw new ArgumentException("characterSet must not be empty", "characterSet");

            var bytes = new byte[length * 8];
            new RNGCryptoServiceProvider().GetBytes(bytes);
            var result = new char[length];
            for (int i = 0; i < length; i++)
            {
                ulong value = BitConverter.ToUInt64(bytes, i * 8);
                result[i] = characterArray[value % (uint)characterArray.Length];
            }
            return new string(result);
        }

        public static string CleanUrl(this string tittle)
        {
            string clean = "";
            string[] words = tittle.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                if (i == words.Length - 1 && words[i].Length > 0)
                {
                    clean += words[i].ToLower();
                }
                else if (words[i].Length > 0)
                {
                    clean += words[i].ToLower() + "-";
                }
            }
            return clean;
        }

        public static string PersianToEnglish(this string persianStr)
        {
            Dictionary<string, string> LettersDictionary = new Dictionary<string, string>
            {
                ["۰"] = "0",
                ["۱"] = "1",
                ["۲"] = "2",
                ["٢"] = "2",
                ["۳"] = "3",
                ["۴"] = "4",
                ["۵"] = "5",
                ["۶"] = "6",
                ["۷"] = "7",
                ["۸"] = "8",
                ["۹"] = "9"
            };
            return LettersDictionary.Aggregate(persianStr, (current, item) =>
                         current.Replace(item.Key, item.Value)).ToString(CultureInfo.CreateSpecificCulture("en-US"));
        }

        public static string GatherErrors(this IdentityResult result, IStringLocalizer localizer, bool useHtmlNewLine = false)
        {
            var results = new StringBuilder();
            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    var errorDescription = localizer[error.Code].Value;// error.Description;
                    if (string.IsNullOrWhiteSpace(errorDescription))
                    {
                        continue;
                    }

                    if (!useHtmlNewLine)
                    {
                        results.AppendLine(errorDescription);
                    }
                    else
                    {
                        results.AppendLine($"{errorDescription}<br/>");
                    }
                }
            }
            return results.ToString();
        }

        public static bool IsInteger(decimal number)
        {
            return number == Math.Truncate(number);
        }  
        public static bool IsInteger(double number)
        {
            return number == Math.Truncate(number);
        }

        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }     

    }
}

﻿using Shop.Core.Domain.Users;

namespace Shop.Core.Infrastructure
{
    public interface IAppContext
    {
        User User { get; }

        bool TryGetGuestUserId(out long guestId);

        void EnsureGuestCookieRemoved();

        void ClearCache();
    }
}

﻿using MD.PersianDateTime.Core;
using System;
using System.Globalization;

namespace Shop.Core.Infrastructure
{
    public static class DateTimeExtentions
    {
        public static string ToPersianTime(this TimeSpan ts)
        {
            return ts.Hours.ToString().PadLeft(2, '0') + ":" + ts.Minutes.ToString().PadLeft(2, '0');
        }
        public static string ToPersianTime(this TimeSpan? ts)
        {
            return ts.HasValue ? ToPersianTime(ts.Value) : "";
        }
        public static string ToPersianDate(this DateTime dateTime)
        {
            PersianCalendar pc = new PersianCalendar();
            try
            {
                return string.Format("{0}/{1}/{2}", pc.GetYear(dateTime).ToString().PadLeft(4, '0'),
                    pc.GetMonth(dateTime).ToString().PadLeft(2, '0'),
                    pc.GetDayOfMonth(dateTime).ToString().PadLeft(2, '0'));
            }
            catch
            {
                return "";
            }
        }
        public static string ToPersianDateTime(this DateTime dateTime)
        {
            try
            {
                return string.Format("{0} {1}:{2}", dateTime.ToPersianDate(), dateTime.Hour.ToString().PadLeft(2, '0'),
                    dateTime.Minute.ToString().PadLeft(2, '0'));
            }
            catch
            {
                return "";
            }
        }
        public static string ToPersianDate(this DateTime? dateTime)
        {
            if (dateTime != null)
            {
                return ToPersianDate(dateTime.Value);
            }
            return string.Empty;
        }
        public static string ToPersianDateTime(this DateTime? dateTime)
        {
            if (dateTime != null)
            {
                return ToPersianDateTime(dateTime.Value);
            }
            return string.Empty;
        }
        public static string PersianFullDate()
        {
            PersianCalendar pc = new PersianCalendar();
            DateTime dt = DateTime.Now;
            try
            {
                return string.Format("{0}{1}{2}{3}{4}{5}{6}", pc.GetYear(dt).ToString().PadLeft(4, '0'),
                    pc.GetMonth(dt).ToString().PadLeft(2, '0'),
                    pc.GetDayOfMonth(dt).ToString().PadLeft(2, '0'),
                    pc.GetHour(dt).ToString().PadLeft(2, '0'),
                    pc.GetMinute(dt).ToString().PadLeft(2, '0'),
                    pc.GetSecond(dt).ToString().PadLeft(2, '0'),
                    pc.GetMilliseconds(dt).ToString().PadLeft(4, '0'));
            }
            catch
            {
                return "";
            }
        }
        public static DateTime? ToGregorianDateTime(this string persianDate)
        {
            if (string.IsNullOrEmpty(persianDate))
                return null;
            try
            {
                var pc = new PersianCalendar();

                var arrPersianDateTime = persianDate.Split(' ');
                var arrPersianDate = arrPersianDateTime[0].Split('/');
                var arrPersianTime = new string[] { "0", "0", "0" };

                if (arrPersianDateTime.Length > 1)
                {
                    arrPersianTime = arrPersianDateTime[1].Split(':');
                }

                var year = int.Parse(arrPersianDate[0]);
                var month = short.Parse(arrPersianDate[1]);
                var day = short.Parse(arrPersianDate[2]);

                var hour = short.Parse(arrPersianTime[0]);
                var minute = short.Parse(arrPersianTime[1]);
                var second = arrPersianTime.Length == 3 ? short.Parse(arrPersianTime[2]) : 0;

                return pc.ToDateTime(year, month, day, hour, minute, second, 0);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DateTime PersianDateToMiladi(this string persiandate)
        {
            PersianDateTime persianDate = PersianDateTime.Parse(persiandate);
            DateTime miladiDate = persianDate.ToDateTime();
            return miladiDate;
        }
        public static string MiladiToPersianDate(this DateTime date)
        {
            PersianDateTime persianDate = new PersianDateTime(date);
            return persianDate.ToString("yyyy/MM/dd").PersianToEnglish();
        }
        public static string MiladiToPersianDate(this DateTime? date)
        {
            if (date != null)
            {
                return MiladiToPersianDate(date.Value);
            }
            return string.Empty;
        }
        public static DateTime? ToMiladi(this string persiandate)
        {
            if (!string.IsNullOrEmpty(persiandate))
            {
                PersianDateTime persianDate = PersianDateTime.Parse(persiandate);
                DateTime miladiDate = persianDate.ToDateTime();
                return miladiDate;
            }
            return null;
        }
        public static string PersianFullDateTime()
        {
            var persianDateTime = new PersianDateTime(DateTime.Now);
            return persianDateTime.ToString("yyyyMMddHHmmssffff");
        }
        public static string PersianDateNow()
        {
            var persianDateTime = new PersianDateTime(DateTime.Now);
            return persianDateTime.ToString("yyyy");
        }

    }
}

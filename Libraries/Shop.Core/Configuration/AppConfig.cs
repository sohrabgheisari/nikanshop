﻿namespace Shop.Core.Configuration
{
    public class AppConfig
    {
        public string JwtSecretKey { get; set; }
        public int CacheLifeTimeMinutes { get; set; }

        public string Mobile { get; set; }

        public bool IsActive { get; set; }
    }
}

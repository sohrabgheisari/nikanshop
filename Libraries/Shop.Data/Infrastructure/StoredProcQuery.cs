﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Data.Infrastructure
{
    public class StoredProcQuery
    {
        public virtual AppDbContext Context { get; set; }

        public virtual string Name { get; set; }

        public virtual List<DbParameter> Parameters { get; } = new List<DbParameter>();

        public override string ToString()
        {
            if (string.IsNullOrEmpty(Name))
            {
                throw new System.Exception("StoredProc name is not provided");
            }

            string param = string.Join(", ", Parameters.Where(x => x.Direction != System.Data.ParameterDirection.ReturnValue).Select(x => $"@{x.ParameterName} {(x.Direction != System.Data.ParameterDirection.Input ? "OUTPUT" : string.Empty)}"));
            return $"{Name} {param}".Trim();
        }

        public virtual IQueryable<TEntity> FromSql<TEntity>()
            where TEntity : class
        {
            return Context.Set<TEntity>().FromSql(ToString(), Parameters.OfType<object>().ToArray()).AsNoTracking();
        }

        public virtual IQueryable<TEntity> FromQuery<TEntity>()
            where TEntity : class
        {
            return Context.Query<TEntity>().FromSql(ToString(), Parameters.OfType<object>().ToArray()).AsNoTracking();
        }

        public virtual Task<int> ExecuteSqlCommandAsync()
        {
            return Context.Database.ExecuteSqlCommandAsync(ToString(), Parameters.OfType<object>().ToArray());
        }
    }
}

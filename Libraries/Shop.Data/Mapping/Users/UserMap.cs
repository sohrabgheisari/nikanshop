﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;
using Shop.Core.Domain.Users;

namespace Shop.Data.Mapping.Users
{
    class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> entity)
        {
         
            entity.Property(e => e.Email)
                .HasMaxLength(256);

            entity.Property(e => e.EmailConfirmed);

            entity.Property(e => e.FirstName)
                .HasMaxLength(50);

            entity.Property(e => e.LastName)
              .IsRequired()
              .HasMaxLength(50);

            entity.Property(e => e.LockoutEnabled);

            entity.Property(e => e.LockoutEnd);

            entity.Property(e => e.NormalizedEmail)
                .HasMaxLength(256);

            entity.Property(e => e.NormalizedUserName)
                .HasMaxLength(256);

            entity.Property(e => e.PhoneNumber)
                .IsRequired(false);

            entity.Property(e => e.PhoneNumberConfirmed);

          
            entity.Property(e => e.UserName)
                .IsRequired()
                .HasMaxLength(256);

            entity.Property(e => e.Address)
             .HasMaxLength(1000);

            entity.Property(e => e.IsActive);

            entity.HasOne(x => x.NikanCustomerEquivalent)
             .WithOne(x => x.User)
             .HasForeignKey<NikanCustomerEquivalent>(x => x.ShopUserId)
             .OnDelete(DeleteBehavior.Restrict);

        }
    }
}

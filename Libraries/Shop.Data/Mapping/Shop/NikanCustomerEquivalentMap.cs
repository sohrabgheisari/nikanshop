﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class NikanCustomerEquivalentMap : IEntityTypeConfiguration<NikanCustomerEquivalent>
    {
        public void Configure(EntityTypeBuilder<NikanCustomerEquivalent> entity)
        {
            entity.ToTable("NikanCustomerEquivalent");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => x.NikanCustomerId).IsUnique();
            entity.HasIndex(x => x.ShopUserId).IsUnique();
        }
    }
}

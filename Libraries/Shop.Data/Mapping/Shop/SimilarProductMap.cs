﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class SimilarProductMap : IEntityTypeConfiguration<SimilarProduct>
    {
        public void Configure(EntityTypeBuilder<SimilarProduct> entity)
        {
            entity.ToTable("SimilarProduct");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => new { x.ProductId, x.SimilarProductId }).IsUnique();

            entity.HasOne(x => x.Product)
                .WithMany(x => x.SimilarProduct_Product)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(x => x.ProductSimilar)
                .WithMany(x => x.SimilarProduct_Similar)
                .HasForeignKey(x => x.SimilarProductId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

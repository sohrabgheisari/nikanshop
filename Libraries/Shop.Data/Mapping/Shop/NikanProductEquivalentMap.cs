﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class NikanProductEquivalentMap : IEntityTypeConfiguration<NikanProductEquivalent>
    {
        public void Configure(EntityTypeBuilder<NikanProductEquivalent> entity)
        {
            entity.ToTable("NikanProductEquivalent");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => x.NikanProductId).IsUnique();
            entity.HasIndex(x => x.ShopProductId).IsUnique();
        }
    }
}

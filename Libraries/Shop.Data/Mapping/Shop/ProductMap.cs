﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class ProductMap : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> entity)
        {
            entity.ToTable("Product");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.Name)
                .HasMaxLength(100);

            entity.Property(x => x.EnglishName)
                .HasMaxLength(100);

            entity.Property(x => x.ImageUrl)
                .HasMaxLength(100);

            entity.Property(x => x.BarCode)
                .HasMaxLength(50);

            entity.HasOne(x => x.Category)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.CategoryId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(x => x.Brand)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.BrandId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(x => x.Discount)
                .WithMany(x => x.Products)
                .HasForeignKey(x => x.DiscountId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(x => x.NikanProductEquivalent)
                .WithOne(x => x.Product)
                .HasForeignKey<NikanProductEquivalent>(x => x.ShopProductId)
                .OnDelete(DeleteBehavior.Restrict);

        }
    }
}

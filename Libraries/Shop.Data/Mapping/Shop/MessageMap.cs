﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Shop.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class MessageMap: IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> entity)
        {
            entity.ToTable("Message");

            entity.HasKey(e => e.Id);


            entity.Property(e => e.FullName)
                .HasMaxLength(100)
                .IsRequired(true);

            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .IsRequired(true);

            entity.Property(e => e.Subject)
                .HasMaxLength(200)
                .IsRequired(true);


            entity.Property(e => e.Description)
                .HasMaxLength(1000)
                .IsRequired(true);

            entity.Property(e => e.Phone)
                .HasMaxLength(15)
                .IsRequired(false);


            entity.Property(e => e.ReadDate)
                .IsRequired(false);

            entity.Property(e => e.IsRead);

            entity.Property(e => e.IsArchived)
                .IsRequired(false);
        }
    }
}

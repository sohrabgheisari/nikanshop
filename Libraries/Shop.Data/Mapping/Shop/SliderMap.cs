﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class SliderMap : IEntityTypeConfiguration<Slider>
    {
        public void Configure(EntityTypeBuilder<Slider> entity)
        {
            entity.ToTable("Slider");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.SliderUrl)
                .HasMaxLength(200);
        }
    }
}

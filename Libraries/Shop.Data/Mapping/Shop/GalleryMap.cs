﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class GalleryMap : IEntityTypeConfiguration<Gallery>
    {
        public void Configure(EntityTypeBuilder<Gallery> entity)
        {
            entity.ToTable("Gallery");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.MainImageUrl)
                .HasMaxLength(200);

            entity.Property(x => x.MobileImageUrl)
                .HasMaxLength(200);

            entity.HasOne(x => x.Product)
                .WithMany(x => x.Galleries)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class TraitMap : IEntityTypeConfiguration<Trait>
    {
        public void Configure(EntityTypeBuilder<Trait> entity)
        {
            entity.ToTable("Trait");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.Title)
                .HasMaxLength(100);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class ProductTraitMap : IEntityTypeConfiguration<ProductTrait>
    {
        public void Configure(EntityTypeBuilder<ProductTrait> entity)
        {
            entity.ToTable("ProductTrait");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => new { x.ProductId, x.ColorTriatId, x.WarrantyTriatId }).IsUnique();

            entity.Property(x => x.BarCode)
                .HasMaxLength(50);

            entity.HasOne(x => x.Product)
                .WithMany(x => x.ProductTraits)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(x => x.ColorTrait)
                .WithMany(x => x.ProductTraits_Color)
                .HasForeignKey(x => x.ColorTriatId)
                .OnDelete(DeleteBehavior.Restrict);

            entity.HasOne(x => x.WarrantyTrait)
                .WithMany(x => x.ProductTraits_Warranty)
                .HasForeignKey(x => x.WarrantyTriatId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

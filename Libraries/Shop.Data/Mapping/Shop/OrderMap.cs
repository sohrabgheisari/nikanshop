﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class OrderMap : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> entity)
        {
            entity.ToTable("Order");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => x.Number).IsUnique();

            entity.Property(x => x.TransactionNumber)
                .HasMaxLength(50);

            entity.Property(x => x.TransactionCode)
                .HasMaxLength(50);

            entity.Property(e => e.Address)
               .HasMaxLength(1000);

            entity.HasOne(x => x.User)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

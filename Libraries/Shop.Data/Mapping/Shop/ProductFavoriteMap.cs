﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class ProductFavoriteMap : IEntityTypeConfiguration<ProductFavorite>
    {
        public void Configure(EntityTypeBuilder<ProductFavorite> entity)
        {
            entity.ToTable("ProductFavorite");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => new { x.UserId, x.ProductId }).IsUnique();

            entity.HasOne(x => x.User)
                .WithMany(x => x.Favorites)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(x => x.Product)
                .WithMany(x => x.Favorites)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class BrandMap : IEntityTypeConfiguration<Brand>
    {
        public void Configure(EntityTypeBuilder<Brand> entity)
        {
            entity.ToTable("Brand");

            entity.Property(x => x.Title)
                .HasMaxLength(100);

            entity.Property(x => x.ImageUrl)
                .HasMaxLength(200);
        }
    }
}

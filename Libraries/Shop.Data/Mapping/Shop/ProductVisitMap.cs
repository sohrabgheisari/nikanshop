﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;
using System;
using System.Collections.Generic;
using System.Text;

namespace Shop.Data.Mapping.Shop
{
    internal class ProductVisitMap : IEntityTypeConfiguration<ProductVisit>
    {
        public void Configure(EntityTypeBuilder<ProductVisit> entity)
        {
            entity.ToTable("ProductVisit");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => new { x.UserId, x.ProductId }).IsUnique();

            entity.HasOne(x => x.User)
                .WithMany(x => x.ProductVisits)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(x => x.Product)
                .WithMany(x => x.ProductVisits)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

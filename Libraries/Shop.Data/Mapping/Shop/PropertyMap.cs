﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class PropertyMap : IEntityTypeConfiguration<Property>
    {
        public void Configure(EntityTypeBuilder<Property> entity)
        {
            entity.ToTable("Property");

            entity.HasKey(x => x.Id);

            entity.Property(x => x.Name)
                .HasMaxLength(50);

            entity.HasOne(x => x.PropertyCategory)
                .WithMany(x => x.Properties)
                .HasForeignKey(x => x.PropertCategoryId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

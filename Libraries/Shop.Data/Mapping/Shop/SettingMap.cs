﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class SettingMap : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> entity)
        {
            entity.ToTable("Setting");

            entity
                .Property(x => x.Key)
                 .HasMaxLength(100)
                .IsRequired(true);

            entity
                .Property(x => x.Value)
                .IsRequired(true);
        }
    }
}

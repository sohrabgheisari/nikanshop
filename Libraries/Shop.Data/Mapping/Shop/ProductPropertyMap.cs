﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data.Mapping.Shop
{
    internal class ProductPropertyMap : IEntityTypeConfiguration<ProductProperty>
    {
        public void Configure(EntityTypeBuilder<ProductProperty> entity)
        {
            entity.ToTable("ProductProperty");

            entity.HasKey(x => x.Id);

            entity.HasIndex(x => new { x.ProductId, x.PropertyId }).IsUnique();

            entity.Property(x => x.Value)
                .HasMaxLength(100);

            entity.HasOne(x => x.Product)
                .WithMany(x => x.ProductProperties)
                .HasForeignKey(x => x.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            entity.HasOne(x => x.Property)
                .WithMany(x => x.ProductProperties)
                .HasForeignKey(x => x.PropertyId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}

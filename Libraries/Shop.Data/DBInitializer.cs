﻿using NikanBase.Core.Domain.Shop;
using Shop.Core.Domain.Shop;
using Shop.Core.Domain.Users;
using System;
using System.Linq;

namespace Shop.Data
{
    public class DBInitializer
    {
        public static void Initialize(AppDbContext context)
        {
            DBInitializer initializer = new DBInitializer();
            initializer.Seed(context);
        }

        public void Seed(AppDbContext context)
        {
            context.Database.EnsureCreated();

            if (context.Roles.Any())
            {
                // DB has been seeded
                return;
            }

            using (var transaction = context.Database.BeginTransaction())
            {

                #region Category
                Category cat = new Category()
                {
                    Title = "دسته بندی ها",
                    Parent_Id=null,
                    Level=0
                };

                context.Set<Category>().Add(cat);

                #endregion

                var adminrole = new Role
                {
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                };
                var userrole = new Role
                {
                    Name = "User",
                    NormalizedName = "USER",
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                };
                context.Set<Role>().Add(adminrole);
                context.Set<Role>().Add(userrole);
                context.SaveChanges();

                User user = new User
                {
                    ConcurrencyStamp = Guid.NewGuid().ToString(),
                    PasswordHash = "AQAAAAEAACcQAAAAEC5mbvNHbg2ctI8aMJj5ZNn6t7tu05Gw6iTTqXZLlAblXRNdD4xm3LmnamgqtDn1Tw==",
                    SecurityStamp = "5440e31d-b341-4046-b8f2-628f3ef54747",
                    UserName = "Admin",
                    NormalizedUserName = "Admin".Normalize().ToUpperInvariant(),
                    FirstName = "Admin",
                    LastName = "Admin",
                    Email = "admin@nikan.com",
                    NormalizedEmail = "admin@nikan.com".Normalize().ToUpperInvariant(),
                    IsActive = true,
                };
                context.Set<User>().Add(user);
                context.SaveChanges();

                context.UserRoles.Add(new Microsoft.AspNetCore.Identity.IdentityUserRole<long>
                {
                    RoleId = adminrole.Id,
                    UserId = user.Id
                });
                context.SaveChanges();

                transaction.Commit();
            }
        }
    }
}

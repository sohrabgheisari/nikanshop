﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class EditIndexOfOrderItemTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OrderItem_OrderId_ProductId",
                table: "OrderItem");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_OrderId_ProductId_ProductTraitId",
                table: "OrderItem",
                columns: new[] { "OrderId", "ProductId", "ProductTraitId" },
                unique: true,
                filter: "[ProductTraitId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_OrderItem_OrderId_ProductId_ProductTraitId",
                table: "OrderItem");

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_OrderId_ProductId",
                table: "OrderItem",
                columns: new[] { "OrderId", "ProductId" },
                unique: true);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class MakeNullableFieldInProductTraitTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductTrait_ProductId_ColorTriatId_WarrantyTriatId",
                table: "ProductTrait");

            migrationBuilder.AlterColumn<long>(
                name: "WarrantyTriatId",
                table: "ProductTrait",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<DateTime>(
                name: "WarrantyDate",
                table: "ProductTrait",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<long>(
                name: "ColorTriatId",
                table: "ProductTrait",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.CreateIndex(
                name: "IX_ProductTrait_ProductId_ColorTriatId_WarrantyTriatId",
                table: "ProductTrait",
                columns: new[] { "ProductId", "ColorTriatId", "WarrantyTriatId" },
                unique: true,
                filter: "[ColorTriatId] IS NOT NULL AND [WarrantyTriatId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProductTrait_ProductId_ColorTriatId_WarrantyTriatId",
                table: "ProductTrait");

            migrationBuilder.AlterColumn<long>(
                name: "WarrantyTriatId",
                table: "ProductTrait",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "WarrantyDate",
                table: "ProductTrait",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "ColorTriatId",
                table: "ProductTrait",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductTrait_ProductId_ColorTriatId_WarrantyTriatId",
                table: "ProductTrait",
                columns: new[] { "ProductId", "ColorTriatId", "WarrantyTriatId" },
                unique: true);
        }
    }
}

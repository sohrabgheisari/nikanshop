﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class CreateNikanCustomerEquivalentTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NikanCustomerEquivalent",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShopUserId = table.Column<long>(nullable: false),
                    NikanCustomerId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NikanCustomerEquivalent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NikanCustomerEquivalent_AspNetUsers_ShopUserId",
                        column: x => x.ShopUserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NikanCustomerEquivalent_NikanCustomerId",
                table: "NikanCustomerEquivalent",
                column: "NikanCustomerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NikanCustomerEquivalent_ShopUserId",
                table: "NikanCustomerEquivalent",
                column: "ShopUserId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NikanCustomerEquivalent");
        }
    }
}

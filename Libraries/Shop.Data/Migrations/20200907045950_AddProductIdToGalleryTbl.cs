﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class AddProductIdToGalleryTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProductId",
                table: "Gallery",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Gallery_ProductId",
                table: "Gallery",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Gallery_Product_ProductId",
                table: "Gallery",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Gallery_Product_ProductId",
                table: "Gallery");

            migrationBuilder.DropIndex(
                name: "IX_Gallery_ProductId",
                table: "Gallery");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "Gallery");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class removeDiscountFromProductTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DiscountFrom",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "DiscountTo",
                table: "Product");

            migrationBuilder.DropColumn(
                name: "DiscountType",
                table: "Product");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DiscountFrom",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DiscountTo",
                table: "Product",
                nullable: true);

            migrationBuilder.AddColumn<byte>(
                name: "DiscountType",
                table: "Product",
                nullable: true);
        }
    }
}

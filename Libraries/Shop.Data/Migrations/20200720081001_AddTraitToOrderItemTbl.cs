﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class AddTraitToOrderItemTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ProductTraitId",
                table: "OrderItem",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TraitPrice",
                table: "OrderItem",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateIndex(
                name: "IX_OrderItem_ProductTraitId",
                table: "OrderItem",
                column: "ProductTraitId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderItem_ProductTrait_ProductTraitId",
                table: "OrderItem",
                column: "ProductTraitId",
                principalTable: "ProductTrait",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderItem_ProductTrait_ProductTraitId",
                table: "OrderItem");

            migrationBuilder.DropIndex(
                name: "IX_OrderItem_ProductTraitId",
                table: "OrderItem");

            migrationBuilder.DropColumn(
                name: "ProductTraitId",
                table: "OrderItem");

            migrationBuilder.DropColumn(
                name: "TraitPrice",
                table: "OrderItem");
        }
    }
}

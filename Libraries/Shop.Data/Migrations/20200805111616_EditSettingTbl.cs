﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class EditSettingTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AppName",
                table: "Setting");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Setting");

            migrationBuilder.AddColumn<long>(
                name: "SourceId",
                table: "Setting",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SourceId",
                table: "Setting");

            migrationBuilder.AddColumn<string>(
                name: "AppName",
                table: "Setting",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Setting",
                nullable: true);
        }
    }
}

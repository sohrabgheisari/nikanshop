﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class CreateProductTraitTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProductTrait",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProductId = table.Column<long>(nullable: false),
                    ColorTriatId = table.Column<long>(nullable: false),
                    WarrantyTriatId = table.Column<long>(nullable: false),
                    WarrantyDate = table.Column<DateTime>(nullable: false),
                    Inventory = table.Column<int>(nullable: false),
                    PriceIncrease = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductTrait", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductTrait_Trait_ColorTriatId",
                        column: x => x.ColorTriatId,
                        principalTable: "Trait",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ProductTrait_Product_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProductTrait_Trait_WarrantyTriatId",
                        column: x => x.WarrantyTriatId,
                        principalTable: "Trait",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductTrait_ColorTriatId",
                table: "ProductTrait",
                column: "ColorTriatId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTrait_WarrantyTriatId",
                table: "ProductTrait",
                column: "WarrantyTriatId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductTrait_ProductId_ColorTriatId_WarrantyTriatId",
                table: "ProductTrait",
                columns: new[] { "ProductId", "ColorTriatId", "WarrantyTriatId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductTrait");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class CreateNikanProductEquivalentTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NikanProductEquivalent",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ShopProductId = table.Column<long>(nullable: false),
                    NikanProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NikanProductEquivalent", x => x.Id);
                    table.ForeignKey(
                        name: "FK_NikanProductEquivalent_Product_ShopProductId",
                        column: x => x.ShopProductId,
                        principalTable: "Product",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_NikanProductEquivalent_NikanProductId",
                table: "NikanProductEquivalent",
                column: "NikanProductId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NikanProductEquivalent_ShopProductId",
                table: "NikanProductEquivalent",
                column: "ShopProductId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "NikanProductEquivalent");
        }
    }
}

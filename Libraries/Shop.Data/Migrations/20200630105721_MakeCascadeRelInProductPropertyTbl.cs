﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shop.Data.Migrations
{
    public partial class MakeCascadeRelInProductPropertyTbl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductProperty_Product_ProductId",
                table: "ProductProperty");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductProperty_Product_ProductId",
                table: "ProductProperty",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductProperty_Product_ProductId",
                table: "ProductProperty");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductProperty_Product_ProductId",
                table: "ProductProperty",
                column: "ProductId",
                principalTable: "Product",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

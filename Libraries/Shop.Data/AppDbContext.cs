﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Shop.Core.Domain.Users;
using Shop.Data.Infrastructure;
using System.Data.Common;
using Shop.Core.Domain.Shop;
using NikanBase.Core.Domain.Shop;

namespace Shop.Data
{

    public class AppDbContext :
      IdentityDbContext<
          User,
          Role,
          long,
          IdentityUserClaim<long>,
          IdentityUserRole<long>,
          IdentityUserLogin<long>,
          IdentityRoleClaim<long>,
          IdentityUserToken<long>>
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        {
        }

        #region Sets

        public DbSet<Message> Messages { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        public DbSet<Setting> Settings { get; set; }


        #endregion

        public virtual StoredProcQuery CreateStoredProcQuery()
        {
            return new StoredProcQuery();
        }

        public virtual DbCommand CreateCommand()
        {
            return Database.GetDbConnection().CreateCommand();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyAllConfigurations();
        }
    }
}


﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Enums;
using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.Product;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Product;
using Shop.Services.Dto.Property;
using Shop.Services.Dto.Setting;
using Shop.Services.Shop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class ProductController : BasePublicController
    {
        #region Fields
        private readonly ILogger logger;
        private readonly IMapper _mapper;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IProductVisitService _productVisitService;
        private readonly IAppContext _appContext;
        private readonly ISettingService _settingService;
        private readonly IBrandService _brandService;
        private readonly IProductFavoriteService _productFavoriteService;
        private readonly ITraitService _traitService;
        private readonly IPropertyService _propertyService;
        #endregion

        #region Constructors
        public ProductController(ILogger<ProductController> logger, IMapper mapper,
            IProductService productService
            , ICategoryService categoryService,
            IProductVisitService productVisitService,
            IBrandService brandService, IProductFavoriteService productFavoriteService,
            IAppContext appContext, ISettingService settingService,
            IPropertyService propertyService
            , ITraitService traitService)
        {
            this.logger = logger;
            _mapper = mapper;
            _productService = productService;
            _categoryService = categoryService;
            _productVisitService = productVisitService;
            _appContext = appContext;
            _settingService = settingService;
            _brandService = brandService;
            _productFavoriteService = productFavoriteService;
            _traitService = traitService;
            _propertyService = propertyService;
        }
        #endregion

        #region Actions

        [HttpGet]
        [Route("product")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("category/{categoryId}/{tittle}")]
        public async Task<IActionResult> SubCategory(long categoryId,[FromQuery(Name = "brand")]List<long> brand,
            [FromQuery(Name = "color")] List<long> color, [FromQuery(Name = "warranty")] List<long> warranty,
            [FromQuery(Name = "property")] List<PropertyFilter> property ,[FromQuery]int page=1,[FromQuery] SortTypes sort=SortTypes.Newest,
            [FromQuery] bool available=false,[FromQuery] decimal? minPrice=null,[FromQuery] decimal? maxPrice=null)
        {
            var websiteSetting = _settingService.Load<WebsiteSettingModel>(null);
            int pageCount = (websiteSetting != null && websiteSetting.PageCount != 0) ? websiteSetting.PageCount : 4;
            List<string> brandQuery = brand.Select(x =>"brand="+x).ToList();
            ViewBag.BrandQueryString = String.Join('&', brandQuery);
            ViewBag.BrandQuery = brand;
            
            List<string> colorQuery = color.Select(x => "color=" + x).ToList();
            ViewBag.ColorQueryString = String.Join('&', colorQuery);
            ViewBag.ColorQuery = color;
            
            List<string> warrantyQuery = warranty.Select(x => "warranty=" + x).ToList();
            ViewBag.WarrantyQueryString = String.Join('&', warrantyQuery);
            ViewBag.WarrantyQuery = warranty; 
            
            List<string> propertyQuery = property.Select((x, index) => $"property[{index}].Id={x.Id}&property[{index}].Value={x.Value}").ToList();
            ViewBag.PropertyQueryString = String.Join('&', propertyQuery);
            ViewBag.PropertyQuery = property;

            ViewBag.Brands = GetBrandsSelectList();
            ViewBag.Colores = GetTraitSelectList(TraitTypes.Color);
            ViewBag.Warranties = GetTraitSelectList(TraitTypes.Warranty);
            ViewBag.Properies =await _propertyService.GetPropertyFilters();
            var cat = await _categoryService.GetCategoryById(categoryId);
            ViewBag.FullPath = await _categoryService.GetFullPathName(categoryId);
            ViewBag.CatId = categoryId;
            ViewBag.CatName = cat.Title;
            ViewBag.CleanName = cat.Title.CleanUrl();
            ViewBag.Sort = sort;
            ViewBag.Available = available;
            if (minPrice.HasValue)
            {
                ViewBag.MinPrice = minPrice.Value;
            }
            else
            {
                ViewBag.MinPrice =null;
            } 
            if (maxPrice.HasValue)
            {
                ViewBag.MaxPrice = maxPrice.Value;
            }
            else
            {
                ViewBag.MaxPrice = null;
            }
            var entity = _categoryService.GetAllCategoryChildsAsQueryable(categoryId).ToList();
            var model = _mapper.Map<List<CategoryModel>>(entity);

            int skip = (page - 1) * pageCount;
            int count = await _productService.GetAllProductInCategoryCount(categoryId, brand,color,warranty,property, available,minPrice,maxPrice);
            ViewBag.pageID = page;
            int pageRemain = count % pageCount;
            if (pageRemain < pageCount && pageRemain != 0)
            {
                pageRemain = 1;
            }
            else pageRemain = 0;
            ViewBag.PageCount = count / pageCount + pageRemain;
            ViewBag.pageID = page;

            var products = _productService.GetAllProductAsQueryable(categoryId,skip,pageCount,sort, brand,color,warranty,property, available,minPrice,maxPrice).ToList();
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            var models = _mapper.Map<List<ProductShopIndexModel>>(products);
             models.ForEach(x => x.FillFields());
            ViewBag.Models = models;
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit != 0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";

            if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_Products", models);
            }
            return View(model);
        }

        [HttpGet]
        [Route("daily-discount")]
        public async Task<IActionResult> DailyDiscount([FromQuery]  int page = 1)
        {
            var websiteSetting = _settingService.Load<WebsiteSettingModel>(null);
            int pageCount = (websiteSetting != null && websiteSetting.PageCount != 0) ? websiteSetting.PageCount : 4;    

            int skip = (page - 1) * pageCount;
            int count = await _productService.GetAllDailyDiscountProductCount();
            ViewBag.pageID = page;
            int pageRemain = count % pageCount;
            if (pageRemain < pageCount && pageRemain != 0)
            {
                pageRemain = 1;
            }
            else pageRemain = 0;
            ViewBag.PageCount = count / pageCount + pageRemain;
            ViewBag.pageID = page;

            var products = _productService.GetAllDailyDiscountProductAsQueryable(skip, pageCount).ToList();
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            var models = _mapper.Map<List<ProductShopIndexModel>>(products);
            models.ForEach(x => x.FillFields());
            ViewBag.Models = models;
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit != 0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";

            if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_DailyDiscount", models);
            }
            return View(models);
        }

        [HttpGet]
        [Route("products/{categoryId}/{tittle}")]
        public async Task<IActionResult> Products(long categoryId)
        {
            var category = await _categoryService.GetCategoryById(categoryId);
            ViewBag.FullPath = await _categoryService.GetFullPathName(categoryId);
            ViewBag.CategoryName = category.Title;
            ViewBag.CategorId = categoryId;
            return View();
        }


        [HttpGet]
        [Route("fav-products")]
        [Authorize(Roles = UserRoleNames.User)]
        public async Task<IActionResult> FavProducts()
        {
            var faves = _productFavoriteService.GetAllAsQueryable().ToList();
            var products = faves.Select(x => x.Product).ToList();
            var models = _mapper.Map<List<ProductShopIndexModel>>(products);
            models.ForEach(x => x.FillFields());
            return View(models);
        }

        [HttpGet]
        [Route("brands/{brandId}/{tittle}")]
        public async Task<IActionResult> Brands(long brandId)
        {
            var brand = await _brandService.GetBrandById(brandId);
            ViewBag.BrandName = brand.Title;
            ViewBag.BrandId = brandId;
            ViewBag.FullPath = brand.Title;
            return View();
        }

        [HttpGet]
        [Route("product/{productId}/{tittle}")]
        public async Task<IActionResult> Product(long productId)
        {
            if (User.Identity.IsAuthenticated && User.IsInRole(UserRoleNames.User))
            {
                await _productVisitService.InsertProductVisit(_appContext.User.Id, productId);
            }
            var product = await _productService.GetProductById(productId);
            var model = _mapper.Map<ProductShopIndexModel>(product);
            model.FillFields();
            model.ProductProperties.ForEach(x => x.FillFields());
            ViewBag.Traits = model.ProductTraits.Select(x => new SelectListItem()
            {
                Value = x.Id.ToString(),
                Text = GetTraitText(x)
            }).ToList();
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit != 0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";
            ViewBag.FullPath = await _categoryService.GetFullPathName(product.CategoryId);
            if (User.IsInRole(UserRoleNames.User))
            {
                var favId = await _productFavoriteService.HasFav(productId);
                ViewBag.Fav = favId > 0 ? true : false;
            }
            else
            {
                ViewBag.Fav = false;
            }
            return View(model);
        }
        #endregion

        #region Utilities
        private string GetTraitText(ProductTraitsModel model)
        {
            string text = string.Empty;
            if (model.ColorTrait != null)
            {
                text += $"رنگ {model.ColorTrait.Title}";
            }
            if (model.WarrantyTrait != null)
            {
                text += $" - گارانتی {model.WarrantyTrait.Title}";
            }
            if (model.PriceIncrease > 0)
            {
                text += $"  [+ {model.PriceIncrease.ToString("N0")} تومان]";
            }
            return text;
        }
        private List<SelectListItem> GetBrandsSelectList()
        {
            var discounts = _brandService.GetAllAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var dis in discounts)
            {
                selectList.Add(new SelectListItem
                {
                    Text = dis.Title,
                    Value = dis.Id.ToString()
                });
            }
            return selectList;
        }
        private List<SelectListItem> GetTraitSelectList(TraitTypes type)
        {
            var entities = _traitService.GetAllTraitAsQueryable(type).ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in entities)
            {
                selectList.Add(new SelectListItem
                {
                    Text = en.Title,
                    Value = en.Id.ToString()
                });
            }
            return selectList;
        }
        #endregion
    }
}

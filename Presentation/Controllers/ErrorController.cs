﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shop.Areas.Admin.Models;
using Shop.Core.Domain.Shop;
using Shop.Core.Infrastructure;
using Shop.Models.Message;
using Shop.Services.Shop;
using Shop.ViewModels;
namespace Shop.Controllers
{
    public class ErrorController : BasePublicController
    {
        #region Fields
        private readonly ILogger logger;
        #endregion

        #region Constructors
        public ErrorController(ILogger<ErrorController> logger)
        {
            this.logger = logger;
        }
        #endregion

        #region Actions

        [HttpGet("error-404")]
        public IActionResult Error404()
        {
            return View();
        }

        [HttpGet("error-400")]
        public IActionResult Error400()
        {
            return View();
        }

        [HttpGet("error-500")]
        public IActionResult Error500()
        {
            return View();
        }

        [HttpGet("error/{code}")]
        public IActionResult Index(int code)
        {
            ViewBag.ErrorCode = code;
            if (code == 400)
            {
                return LocalRedirect("/error-400");
            }
            else if (code == 404)
            {
                return LocalRedirect("/error-404");
            }
            else if (code == 500)
            {
                return LocalRedirect("/error-500");
            }

            return View();
        }


        #endregion
    }
}

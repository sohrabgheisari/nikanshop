﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shop.Core.Domain.Shop;
using Shop.Core.Infrastructure;
using Shop.Models.Message;
using Shop.Services.Dto.Getway;
using Shop.Services.Dto.Setting;
using Shop.Services.Shop;

namespace Shop.Controllers
{
    public class HomeController : BasePublicController
    {
        #region Fields
        private readonly ILogger logger;
        private readonly IMapper _mapper;
        private readonly IAppContext _appContext;
        private readonly IMessageService _messageService;
        private readonly IHostingEnvironment _environment;
        private readonly ISettingService _settingService;
        private readonly IOrderService _orderService;
        private readonly IZarinPalService _zarinPalService;
        private readonly IMellatService _mellatService;
        #endregion

        #region Constructors
        public HomeController(ILogger<HomeController> logger, IMapper mapper, IHostingEnvironment environment,
          IAppContext appContext, IMessageService messageService,
          ISettingService settingService, IOrderService orderService,
          IZarinPalService zarinPalService, IMellatService mellatService)
        {
            this.logger = logger;
            _mapper = mapper;
            _appContext = appContext;
            _messageService = messageService;
            _environment = environment;
            _settingService = settingService;
            _orderService = orderService;
            _zarinPalService = zarinPalService;
            _mellatService = mellatService;
        }
        #endregion

        #region Actions

        [HttpGet]
        public async Task<IActionResult> Index()
        {
           
            return View();
        }

        [HttpGet]
        public IActionResult Error(int? id)
        {
            var logBuilder = new System.Text.StringBuilder();

            var statusCodeReExecuteFeature = HttpContext.Features.Get<IStatusCodeReExecuteFeature>();
            logBuilder.AppendLine($"Error {id} for {Request.Method} {statusCodeReExecuteFeature?.OriginalPath ?? Request.Path.Value}{Request.QueryString.Value}\n");

            var exceptionHandlerFeature = this.HttpContext.Features.Get<IExceptionHandlerFeature>();
            if (exceptionHandlerFeature?.Error != null)
            {
                var exception = exceptionHandlerFeature.Error;
                logBuilder.AppendLine($"<h1>Exception: {exception.Message}</h1>{exception.StackTrace}");
            }

            foreach (var header in Request.Headers)
            {
                var headerValues = string.Join(",", value: header.Value);
                logBuilder.AppendLine($"{header.Key}: {headerValues}");
            }
            logger.LogError(logBuilder.ToString());

            if (id == null)
            {
                return View("Error");
            }

            switch (id.Value)
            {
                case 401:
                case 403:
                    return View("AccessDenied");
                case 404:
                    return View("NotFound");

                default:
                    return View("Error");
            }
        }

        [Route("about-us")]
        [HttpGet]
        public IActionResult About()
        {
            var setting = _settingService.Load<WebsiteSettingModel>(null);

            return View(setting);
        }

        [Route("contact-us")]
        [HttpGet]
        public async Task<IActionResult> ContactUs()
        {
            ViewBag.success = TempData["success"];
            ViewBag.error = TempData["error"];
            var setting = _settingService.Load<WebsiteSettingModel>(null);
            if (setting!=null)
            {
                ViewBag.ShopAddress = setting.ShopAddress;
                ViewBag.Phones = setting.ShopPhones;
            }
            else
            {
                ViewBag.ShopAddress = "";
                ViewBag.Phones = "";
            }
            var model = new MessageModel() { };
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("contact-us")]
        public async Task<IActionResult> ContactUs(MessageModel model)
        {
            if (ModelState.IsValid)
            {
                var entity = _mapper.Map<Message>(model);
                entity.IsArchived = false;
                entity.IsRead = false;
                entity.SentDate = DateTime.Now;
                await _messageService.InsertMessage(entity);
                TempData["success"] = "پیام شما با موفقیت ارسال گردید";
                return LocalRedirect("/contact-us");
            }

            TempData["error"] = "ارسال پیام با شکست مواجه گردید";
            return LocalRedirect("/contact-us");
        }



        [HttpPost]
        [Route("file-upload")]
        public async Task<IActionResult> UploadImage(IFormFile upload, string CKEditorFuncNum, string CKEditor, string langCode)
        {
            if (upload.Length <= 0) return null;

            var fileName = Guid.NewGuid() + Path.GetExtension(upload.FileName).ToLower();
            var path = Path.Combine(_environment.WebRootPath, "ckEditorImages");
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var filePath = Path.Combine(path, fileName);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                await upload.CopyToAsync(fileStream).ConfigureAwait(false);
            }
            var url = $"{"/ckEditorImages/"}{fileName}";
            return Json(new { uploaded = true, url });
        }


        [Route("payment/{factorNumber}")]
        [HttpGet]
        public async Task<IActionResult> OnlinePayment(string factorNumber)
        {
            try
            {
                string status = HttpContext.Request.Query["Status"].ToString();
                string authority = HttpContext.Request.Query["Authority"].ToString();
                var result = await _zarinPalService.VerifyPayment(factorNumber, status, authority);
                return View(result);
            }
            catch (Exception ex)
            {
                logger.LogError(ex.Message);
                return View(new ZarinPalResult() { IsSuccess = false, Message = ex.Message, StatusCode = 0, TransactionNumber = "" });
            }

        }


        [Route("mellatpay")]
        [HttpPost]
        public async Task<IActionResult> MellatPayment(/*string RefId, string ResCode, string SaleOrderId, string SaleRefrenceId*/)
        {
            string refId = HttpContext.Request.Query["RefId"].ToString();
            string resCode = HttpContext.Request.Query["ResCode"].ToString();
            string saleOrderId = HttpContext.Request.Query["SaleOrderId"].ToString();
            string saleReferenceId = HttpContext.Request.Query["SaleReferenceId"].ToString();
            var result = await _mellatService.VerifyPayment(refId, resCode, saleOrderId, saleReferenceId);
            return View(result);
        }
        #endregion
    }
}

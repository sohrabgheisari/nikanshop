﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Models.ShopCard;
using Shop.Services.Dto.Product;
using Shop.Services.Shop;
using Shop.Services.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Controllers
{
    public class ShopCardController : BasePublicController
    {
        #region Fields
        private readonly ILogger logger;
        private readonly IMapper _mapper;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IAppContext _appContext;
        private readonly IOrderService _orderService;
        private readonly UserManager _userManager;
        private readonly IZarinPalService _zarinPalService;
        private readonly IMellatService _mellatService;
        #endregion

        #region Constructors
        public ShopCardController(ILogger<ProductController> logger, IMapper mapper, IProductService productService
            , ICategoryService categoryService, IAppContext appContext,
            IOrderService orderService, UserManager userManager,
            IZarinPalService zarinPalService,
             IMellatService mellatService)
        {
            this.logger = logger;
            _mapper = mapper;
            _productService = productService;
            _categoryService = categoryService;
            _appContext = appContext;
            _orderService = orderService;
            _userManager = userManager;
            _zarinPalService = zarinPalService;
            _mellatService = mellatService;
        }
        #endregion

        #region Actions

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var shopCard = await _orderService.GetCard();
            ViewBag.IsFull = shopCard.IsFull;
            ViewBag.Messages = shopCard.Message;
            ViewBag.Status = shopCard.Status;
            ViewBag.FactorNumber = shopCard.FactorNumber;
            ViewBag.IsFacor = shopCard.IsFactor;
            if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                return PartialView("_Cart", shopCard.Card);
            }
            return View(shopCard.Card);
        }

        public async Task<int> ShopCardCount()
        {
            return await _orderService.GetCardCount();
        }

        [HttpGet]
        [Route("AddToCard/{id}")]
        public async Task<int> AddToCard(long id)
        {
            return await _orderService.AddToCard(id);
        }
        [HttpGet]
        [Route("AddToCard/{id}/{traitId}")]
        public async Task<int> AddToCard(long id, long traitId)
        {
            return await _orderService.AddToCard(id, traitId);
        }

        [HttpGet]
        [Route("PlusToCard/{id}")]
        public async Task<bool> PlusToCard(long id)
        {
            return await _orderService.PlusToCard(id);
        }

        [HttpGet]
        [Route("PlusToCard/{id}/{traitId}")]
        public async Task<bool> PlusToCard(long id, long traitId)
        {
            return await _orderService.PlusToCard(id, traitId);
        }

        [HttpGet]
        [Route("MinesToCard/{id}")]
        public async Task<bool> MinesToCard(long id)
        {
            return await _orderService.MinesToCard(id);
        }

        [HttpGet]
        [Route("MinesToCard/{id}/{traitId}")]
        public async Task<bool> MinesToCard(long id, int traitId)
        {
            return await _orderService.MinesToCard(id, traitId);
        }

        [HttpGet]
        [Route("RemoveFromShopCard/{id}")]
        public async Task<bool> RemoveFromShopCard(long id)
        {
            return await _orderService.RemoveFromCard(id);
        }

        [Authorize(Roles = UserRoleNames.User)]
        [HttpGet]
        [Route("shipping")]
        public async Task<IActionResult> Shipping()
        {
            List<ShopCardModel> card = new List<ShopCardModel>();
            if (HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") != null)
            {
                card = HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart");
            }
            var order = await _orderService.InsertOrder(card, _appContext.User.Address);
            HttpContext.Session.SetObjectAsJson("ShopCart", order);
            return RedirectToAction("index");
        }

        [Authorize(Roles = UserRoleNames.User)]
        [HttpGet]
        public async Task<IActionResult> ConfirmOrder(string factorNumber)
        {
            var order = await _orderService.GetOrderByFactorNumber(factorNumber);
            var cardOrder = await _orderService.VerifyCard(order.Id);
            if (!cardOrder.Status)
            {
                HttpContext.Session.SetObjectAsJson("ShopCart", cardOrder.Card);
                return RedirectToAction("index");
            }
            ShippingModel model = new ShippingModel()
            {
                Address = _appContext.User.Address,
                Card = cardOrder.Card,
                OrderId = order.Id
            };
            ViewBag.Messages = cardOrder.Message;
            ViewBag.Payments = GetPaymentTypesSelectList();
            return View(model);
        }


        [Authorize(Roles = UserRoleNames.User)]
        [HttpPost]
        public async Task<IActionResult> ConfirmOrder(ShippingModel model)
        {
            var user = _appContext.User;
            user.Address = model.Address;
            var order = await _orderService.GetOrderById(model.OrderId);
            order.Address = model.Address;
            await _orderService.UpdateOrder(order);
            await _userManager.UpdateAsync(user);
            return model.PaymentTypes == PaymentTypes.ZarinPal ? RedirectToAction("Payment", new { factorNumber = order.Number })
                : RedirectToAction("MellatPayment", new { factorNumber = order.Number });
        }


        [Authorize(Roles = UserRoleNames.User)]
        [HttpGet]
        public async Task<IActionResult> Payment(string factorNumber)
        {
            var order = await _orderService.GetOrderByFactorNumber(factorNumber);

            string callbackUrl = $"{Request.Scheme}://{Request.Host}/payment/{factorNumber}";

            var result = await _zarinPalService.Payment(order.Id, callbackUrl);
            if (result.IsSuccess)
            {
                return Redirect(result.RedirectUrl);
            }
            return View(result);
        }

        [Authorize(Roles = UserRoleNames.User)]
        [HttpGet]
        public async Task<IActionResult> MellatPayment(string factorNumber)
        {
            var order = await _orderService.GetOrderByFactorNumber(factorNumber);

            string callbackUrl = $"{Request.Scheme}://{Request.Host}/mellatpay";

            var result = await _mellatService.Payment(order.Id, callbackUrl);
            if (result.IsSuccess)
            {
                return Content(RedirectToMellat(result.RefNumber));
                //return Content("<form action='https://bpm.shaparak.ir/pgwchannel/startpay.mellat' id='frmPayMellat' method='post'><input type='hidden' name='RefId' id='RefId' value='" + result.RefNumber + "' /></form><script>document.getElementById('frmPayMellat').submit();</script>");
            }
            return View(result);
        }
        #endregion

        #region Utilities
        private List<SelectListItem> GetPaymentTypesSelectList()
        {
            Array values = Enum.GetValues(typeof(PaymentTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }

        private string RedirectToMellat(string refId)
        {
            StringBuilder pgwSite = new StringBuilder();
            pgwSite.Append("<html>");
            pgwSite.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
            pgwSite.AppendFormat("<form name='form' action='{0}' method='post'>", "https://bpm.shaparak.ir/pgwchannel/startpay.mellat");
            pgwSite.AppendFormat("<input type='hidden' name='{0}' id='{1}' value='{2}' />", "RefId", "RefId", refId);
            pgwSite.Append("</form></body></html>");
            return pgwSite.ToString();
        }
        #endregion
    }
}

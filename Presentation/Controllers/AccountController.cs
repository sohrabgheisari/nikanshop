﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using NikanSms.Shared;
using Shop.Areas.Admin.Models;
using Shop.Core.Domain.Users;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Models.Account;
using Shop.Services.Dto.Product;
using Shop.Services.Dto.Setting;
using Shop.Services.Shop;
using Shop.Services.Users;

namespace Shop.Controllers
{
    public class AccountController : BasePublicController
    {
        #region Fields

        private readonly SignInManager signInManager;
        private readonly UserManager _userManager;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer localizer;
        private readonly INikanSmsSender _smsService;
        private readonly IOrderService _orderService;
        private readonly ISettingService _settingService;
        #endregion

        #region Constructors

        public AccountController(SignInManager signInManager, IStringLocalizer<LoginModel> localizer
            , UserManager userManager, IMapper mapper, 
            INikanSmsSender smsService, 
            IOrderService orderService, ISettingService settingService)
        {
            this.signInManager = signInManager;
            this.localizer = localizer;
            _userManager = userManager;
            _mapper = mapper;
            _smsService = smsService;
            _orderService = orderService;
            _settingService = settingService;
        }

        #endregion

        #region Actions

        public async Task<IActionResult> Login(string returnUrl = null)
        {
            TempData["ReturnUrl"] = returnUrl;
            var setting = _settingService.Load<WebsiteSettingModel>(null);
            string logoUrl = "/images/logo.png";
            if (setting != null)
            {
                logoUrl = !String.IsNullOrEmpty(setting.LogoUrl) ? setting.LogoUrl : "/images/logo.png";
            }
            ViewBag.LogoUrl = logoUrl;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Login(LoginModel model)
        {
            string returnUrl = TempData.Peek("ReturnUrl") == null ? "" : TempData.Peek("ReturnUrl").ToString();
            DefaultResponseState response = new DefaultResponseState();
            if (ModelState.IsValid)
            {
                var user = await signInManager.UserManager.FindByNameAsync(model.Username.Trim());
                if (user != null)
                {
                    if (!user.IsActive)
                    {
                        response.Success = false;
                        response.Message = "حساب شما غیرفعال است";
                        return Json(response);
                    }
                    var result = await signInManager.PasswordSignInAsync(user, model.Password, model.RememberMe, lockoutOnFailure: false);
                    if (result.Succeeded)
                    {
                      
                        if (signInManager.UserManager.IsInRoleAsync(user, UserRoleNames.Admin).Result)
                        {
                            response.Success = true;
                            response.Message = !string.IsNullOrEmpty(returnUrl)? returnUrl : "/admin/home/index";
                            return Json(response);
                        }
                        else if (signInManager.UserManager.IsInRoleAsync(user, UserRoleNames.User).Result)
                        {
                            var card = await _orderService.GetLastRegisterdOrder(user.Id);
                            if (HttpContext.Session.GetObjectFromJson<List<ShopCardModel>>("ShopCart") == null && card.Count>0)
                            {
                                HttpContext.Session.SetObjectAsJson("ShopCart", card);
                            }
                            response.Success = true;
                            response.Message = !string.IsNullOrEmpty(returnUrl) ? returnUrl : "/home";
                            return Json(response);
                        }
                    }
                }

                response.Success = false;
                response.Message = "نام کاربری یا رمز عبور اشتباه است";
                return Json(response);
            }
            string error = JoinErrors();
            response.Success = false;
            response.Message = error;
            return Json(response);
        }


        public async Task<IActionResult> Register()
        {
            var setting = _settingService.Load<WebsiteSettingModel>(null);
            string logoUrl = "/images/logo.png";
            if (setting != null)
            {
                logoUrl = !String.IsNullOrEmpty(setting.LogoUrl) ? setting.LogoUrl : "/images/logo.png";
            }
            ViewBag.LogoUrl = logoUrl;
            return View();
        }

        [HttpPost]
        public async Task<JsonResult> Register(RegisterModel model)
        {
            DefaultResponseState response = new DefaultResponseState();
            if (ModelState.IsValid)
            {
                var user = _mapper.Map<User>(model);
                user.IsActive = true;
                var result = await _userManager.Insert(user, model.Password, new List<string> { UserRoleNames.User });
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    response.Success = true;
                    response.Message = "ثبت نام با موفقیت انجام شد";
                    return Json(response);
                }
                else
                {
                    response.Success = false;
                    response.Message = result.GatherErrors(localizer);
                    return Json(response);
                }
            }
            string error = JoinErrors();
            response.Success = false;
            response.Message = error;
            return Json(response);
        }

        public async Task<IActionResult> ForgetPassword()
        {
            return View();
        }

        public async Task<IActionResult> ResetPassword([FromQuery] string userName)
        {
            var user = await signInManager.UserManager.FindByNameAsync(userName.Trim());
            string code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);
            INikanSmsSendResult smsResult = await _smsService.SendSms(long.Parse(user.PhoneNumber),$"کد شما برای تغییر رمز: \\n {code}");
            ResetPasswordModel model = new ResetPasswordModel()
            {
                UserName = userName
            };
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> ResetPassword(ResetPasswordModel model)
        {
            DefaultResponseState response = new DefaultResponseState();
            if (ModelState.IsValid)
            {
                var user = await signInManager.UserManager.FindByNameAsync(model.UserName.Trim());
                var valid = await _userManager.VerifyChangePhoneNumberTokenAsync(user, model.Code, user.PhoneNumber);
                if (valid)
                {
                    var hashPassword = _userManager.PasswordHasher.HashPassword(user, model.NewPassword);
                    user.PasswordHash = hashPassword;
                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        await signInManager.RefreshSignInAsync(user);
                        response.Success = true;
                        response.Message = "ثبت نام با موفقیت انجام شد";
                        return Json(response);
                    }
                    else
                    {
                        response.Success = false;
                        response.Message = result.GatherErrors(localizer);
                        return Json(response);
                    }
                }
                response.Success = false;
                response.Message = "کد وارد شده معتبر نمی باشد";
                return Json(response);
            }
            string error = JoinErrors();
            response.Success = false;
            response.Message = error;
            return Json(response);
        }


        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            HttpContext.Session.Remove("ShopCart");
            return LocalRedirect("/home");
        }

        public async Task<IActionResult> LogoutUser()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            HttpContext.Session.Remove("ShopCart");
            return LocalRedirect("/home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public async Task<JsonResult> CheckUserName([FromQuery] string userName)
        {
            DefaultResponseState response = new DefaultResponseState();
            var user = await signInManager.UserManager.FindByNameAsync(userName.Trim());
            if (user == null)
            {
                response.Success = false;
                response.Message = "کاربری یافت  نشد";
                return Json(response);
            }
            response.Success = true;
            response.Message = "موفق";
            return Json(response);
        }
        #endregion

        #region Utilities

        #endregion
    }
}

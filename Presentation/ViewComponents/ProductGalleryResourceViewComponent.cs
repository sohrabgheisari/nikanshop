﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Areas.Admin.Models.Product;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Setting;

namespace Shop.ViewComponents
{
    public class ProductGalleryResourceViewComponent : ViewComponent
    {
        private readonly IMapper _mapper;
        private readonly IProductService _productService;
        private readonly ISettingService _settingService;

        public ProductGalleryResourceViewComponent(IMapper mapper,
            IProductService productService, ISettingService settingService)
        {
            _mapper = mapper;
            _productService = productService;
            _settingService = settingService;
        }
        [HttpGet]
        public  IViewComponentResult Invoke(ResourceTypes type)
        {          
            var products = _productService.GetAllProductAsQueryable(type).ToList();
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            var models = _mapper.Map<List<ProductShopIndexModel>>(products);
            models.ForEach(x => x.FillFields());
            ViewBag.Tittle = type.EnumToDescription();
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit!=0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";
            return View(models);
        }
    }
}

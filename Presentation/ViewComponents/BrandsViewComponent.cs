﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Services.Dto.Brand;

namespace Shop.ViewComponents
{
    public class BrandsViewComponent : ViewComponent
    {
        private readonly IBrandService _brandService;
        private readonly IMapper _mapper;

        public BrandsViewComponent(IBrandService brandService, IMapper mapper)
        {
            _brandService = brandService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke()
        {
            var entity = _brandService.GetAllAsQueryable().ToList();
            var model = _mapper.Map<List<BrandModel>>(entity);
            return View(model);
        }
    }
}

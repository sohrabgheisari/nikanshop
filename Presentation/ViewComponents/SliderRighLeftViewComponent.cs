﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Services.Dto.Slider;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;

namespace Shop.ViewComponents
{
    public class SliderRighLeftViewComponent : ViewComponent
    {
        private readonly ISliderService _sliderService;
        private readonly IMapper _mapper;

        public SliderRighLeftViewComponent(ISliderService sliderService,
            IMapper mapper)
        {
            _sliderService = sliderService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke()
        {
            var slider = _sliderService.GetSliderByType(Core.Enums.SliderTypes.TwoSlideRight).ToList();
            var model = _mapper.Map<List<SliderModel>>(slider);
            return View(model);
        }
    }
}

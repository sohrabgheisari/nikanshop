﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Areas.Admin.Models.Category;

namespace Shop.ViewComponents
{
    public class SubCategoryViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public SubCategoryViewComponent(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke(long categoryId)
        {
            var entity = _categoryService.GetAllCategoryChildsAsQueryable(categoryId).ToList();
            var model = _mapper.Map<List<CategoryModel>>(entity);
            ViewBag.CatId = categoryId;
            return View(model);
        }
    }
}

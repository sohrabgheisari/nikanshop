﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Shop.Areas.Admin.Models.Product;
using Shop.Services.Shop;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Setting;

namespace Shop.ViewComponents
{
    public class ProductCardsViewComponent : ViewComponent
    {
        private readonly ISettingService _settingService;

        public ProductCardsViewComponent(ISettingService settingService)
        {
            _settingService = settingService;

        }
        [HttpGet]
        public  IViewComponentResult Invoke(List<ProductShopIndexModel> models)
        {
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit != 0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";
            return View(models);
        }
    }
}

﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Services.Dto.Slider;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;

namespace Shop.ViewComponents
{
    public class CarouselViewComponent:ViewComponent
    {
        private readonly IGalleryService _galleryService;
        private readonly ISliderService _sliderService;
        private readonly IMapper _mapper;

        public CarouselViewComponent(IGalleryService galleryService,
            ISliderService sliderService,
            IMapper mapper)
        {
            _galleryService = galleryService;
            _sliderService = sliderService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke()
        {
            var entity = _galleryService.GetAllGalleries().ToList();
            var slider = _sliderService.GetSliderByType(Core.Enums.SliderTypes.TwoSlideUp).ToList();
            var sliderModel = _mapper.Map<List<SliderModel>>(slider);
            ViewBag.Slider = sliderModel;
            var model = _mapper.Map<List<GalleryModel>>(entity);
            return View(model);
        }
    }
}

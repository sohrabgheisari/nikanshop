﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Areas.Admin.Models.Category;

namespace Shop.ViewComponents
{
    public class CategoryL2ViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public CategoryL2ViewComponent(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke(long categoryId)
        {
            var entities = _categoryService.GetAllCategoryChildsAsQueryable(categoryId).ToList();
            var entity = _categoryService.GetCategoryById(categoryId).Result;
            ViewBag.ImageUrl = entity.ImageUrl;
            ViewBag.CatTitle = entity.Title;
            var models = _mapper.Map<List<CategoryModel>>(entities);
            return View(models);
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Shop.Areas.Admin.Models.Product;
using Shop.Services.Shop;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Setting;

namespace Shop.ViewComponents
{
    public class BaseCardViewComponent : ViewComponent
    {
        private readonly ISettingService _settingService;

        public BaseCardViewComponent(ISettingService settingService)
        {
            _settingService = settingService;

        }
        [HttpGet]
        public  IViewComponentResult Invoke(ProductShopIndexModel model)
        {
            var priceUnitSetting = _settingService.Load<PriceUnitSettingModel>(null);
            ViewBag.Unit = (priceUnitSetting != null && priceUnitSetting.Unit != 0) ? priceUnitSetting.Unit.EnumToDescription() : "ریال";
            return View(model);
        }
    }
}

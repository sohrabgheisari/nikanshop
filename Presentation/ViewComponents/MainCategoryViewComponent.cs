﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Areas.Admin.Models.Category;

namespace Shop.ViewComponents
{
    public class MainCategoryViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;

        public MainCategoryViewComponent(ICategoryService categoryService, IMapper mapper)
        {
            _categoryService = categoryService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke()
        {
            var entity = _categoryService.GetAllCategoryAsQueryable(1).ToList();
            var model = _mapper.Map<List<CategoryModel>>(entity);
            return View(model);
        }
    }
}

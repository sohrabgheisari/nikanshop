﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Shop;
using System.Collections.Generic;
using System.Linq;
using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.Product;

namespace Shop.ViewComponents
{
    public class AllCategoryProductViewComponent : ViewComponent
    {
        private readonly ICategoryService _categoryService;
        private readonly IMapper _mapper;
        private readonly IProductService _productService;

        public AllCategoryProductViewComponent(ICategoryService categoryService, IMapper mapper,
            IProductService productService)
        {
            _categoryService = categoryService;
            _mapper = mapper;
            _productService = productService;
        }
        [HttpGet]
        public  IViewComponentResult Invoke(long categoryId)
        {
            var catChildIds = _categoryService.GetAllCategoryChildsAsQueryable(categoryId).ToList().Select(x=>x.Id).ToList();
            catChildIds.Add(categoryId);
            var products = _productService.GetAllProductAsQueryable(catChildIds).ToList();
            var models = _mapper.Map<List<ProductShopIndexModel>>(products);
            models.ForEach(x => x.FillFields());
            return View(models);
        }
    }
}

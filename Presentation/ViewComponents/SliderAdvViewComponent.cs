﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Shop.Services.Dto.Slider;
using Shop.Services.Shop;
using System.Linq;

namespace Shop.ViewComponents
{
    public class SliderAdvViewComponent : ViewComponent
    {
        private readonly ISliderService _sliderService;
        private readonly IMapper _mapper;

        public SliderAdvViewComponent(ISliderService sliderService,
            IMapper mapper)
        {
            _sliderService = sliderService;
            _mapper = mapper;
        }
        [HttpGet]
        public  IViewComponentResult Invoke()
        {
            var slider = _sliderService.GetSliderByType(Core.Enums.SliderTypes.AdvertisingSlider).FirstOrDefault();
            var model = _mapper.Map<SliderModel>(slider);
            return View(model);
        }
    }
}

﻿function AddToCart(id) {
    $.ajax({
        url: "/AddToCard/" + id,
        type:"Get"
    }).done(function(res) {
        $("#shopcart").html(res);
    });
}

function MinesToCart(id) {
    $.ajax({
        url: "/MinesToCard/" + id,
        type: "Get"
    }).done(function (res) {
        if (res) {
            $("#shopcart").load("/ShopCard/ShopCardCount");
            $("#showcart").load("/ShopCard/index");
        }
    });   
}

function PlusToCart(id) {
    $.ajax({
        url: "/PlusToCard/" + id,
        type: "Get"
    }).done(function (res) {
        if (res) {
            $("#shopcart").load("/ShopCard/ShopCardCount");
            $("#showcart").load("/ShopCard/index");
        }
    }); 
}

function RemoveFromCart(id) {
    $.ajax({
        url: "/RemoveFromShopCard/" + id,
        type: "Get"
    }).done(function (res) {
        if (res) {
            $("#shopcart").load("/ShopCard/ShopCardCount");
            $("#showcart").load("/ShopCard/index");
        }
    }); 
}

$(document).ready(function() {
    $("#shopcart").load("/ShopCard/ShopCardCount");
});
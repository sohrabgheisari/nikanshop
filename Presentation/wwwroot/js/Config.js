﻿/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    // config.uiColor = '#AADC6E';
    config.contentsLangDirection = 'rtl';
    config.language = 'fa';
    config.extraPlugins = 'bt_table';
    config.filebrowserImageUploadUrl = '/file-upload';
    config.contentsCss = '/libs/ckeditor/contents.css';
    //the next line add the new font to the combobox in CKEditor
    config.font_names = 'IRANSansWeb/IRANSansWeb;' + config.font_names;

};
﻿function Purchase(Key,path) {
    $.ajax({
        url: "/purchase/" + Key + "/" + path,
        type: "Get"
    }).done(function (res) {
        $('#ProductModal').modal({
                backdrop: true,
                keyboard: true
            },
            'show');
        $("#modalbody").html(res);
    });
}
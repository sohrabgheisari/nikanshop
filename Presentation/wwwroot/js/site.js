﻿//custom file browser bs4 setting to show file name
$('#ResumeUrl').on('change', function () {
    var fileName = $(this).val();
    $(this).next('.custom-file-label').html(fileName);
});



//our property on hover change img
var $defaultSrc = "/Images/property/property.png";
$(".ourproperty > div").hover(function () {
    var $src = "/Images/property/property" + ($(this).index() + 1) + ".png";
    $(".property-img > img").attr({
        "src": $src
    });
}, function () {
    $(".property-img > img").attr({
        "src": $defaultSrc
    });
});


//collapse menu btn animation
$('.first-button').on('click', function () {
    $('.animated-icon').toggleClass('open');
});


$('.btnsearch').on('click', function () {
    $('.search-input').toggleClass('show-search');
});


//responsive svg on windows resize && set scroll point for fixmenu
var $topHeaderHeight = $("#topHeader").innerHeight()+1.5;


$(window).resize(function () {
    var $width = $("#contact-section").width();
    var $dtop = "M0 0 L0 40 L" + $width + " 40 Z";
    var $dbottom = "M" + $width + " 0 L" + $width + " 40 L0 0 Z";
    $("#svgtoppath").attr({
        "d": $dtop
    });
    $("#svgbottompath").attr({
        "d": $dbottom
    });

    $topHeaderHeight = $("#topHeader").innerHeight() + 1.5;
});

$(document).ready(function () {
    


    //set svg path
    var $width = $("#contact-section").width();
    var $dtop = "M0 0 L0 40 L" + $width + " 40 Z";
    var $dbottom = "M" + $width + " 0 L" + $width + " 40 L0 0 Z";
    $("#svgtoppath").attr({
        "d": $dtop
    });
    $("#svgbottompath").attr({
        "d": $dbottom
    });
    

    //show menu on scroll
   
    $(window).scroll(function () {
        //console.log($(this).scrollTop());
        if ($(this).scrollTop() <= $topHeaderHeight) {
            //$(".menu-link .nav-link").css("line-height", 80 + 'px');
            $("#carousel").css("margin-top", '0px');
        }
        if ($(this).scrollTop() > $topHeaderHeight) {

            $('#navbar-Menu').addClass('fixmenu');
            //var height = 80 - ($(this).scrollTop() - 40);

            //if (height >= 70) {
            //    $(".menu-link .nav-link").css("line-height", height + 'px');
            //}
            //if ($(this).scrollTop() > 81) {
            //    $(".menu-link .nav-link").css("line-height", 70 + 'px');
            //}
            /*set carousel margin top*/
            var $topheaderlm = $("#topHeader").height();
            var $navbarelm = $(".fixmenu").height();
            var $reaminheight = $navbarelm - $topheaderlm - 6;
           // $("#carousel").css("margin-top", $topheaderlm + $reaminheight + 'px');
            $("#title-bar").css("margin-top", $topheaderlm + $reaminheight + 'px');
        }
        else if ($(this).scrollTop() <= $topHeaderHeight) {
            $('#navbar-Menu').removeClass('fixmenu');
            $("#carousel").css("margin-top", '0px');
            $("#title-bar").css("margin-top", '0px');
        }

    });
    

    //back to top button
    if ($('#back-to-top').length) {
        var scrollTrigger = 400, // px
            backToTop = function () {
                var scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $('#back-to-top').addClass('show');
                } else if (scrollTop < 600) {
                    $('#back-to-top').removeClass('show');
                }
            };
        backToTop();
        $(window).on('scroll', function () {
            backToTop();
        });
        $('#back-to-top').on('click', function (e) {
            e.preventDefault();
            $('html,body').animate({
                scrollTop: 0
            }, 1000);
        });
    }


});
/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
   
    //config.resize_enabled = false;
    config.contentsLangDirection = 'rtl';
    config.language = 'fa';
    config.extraPlugins = 'bt_table';
    config.filebrowserImageUploadUrl = '/file-upload';
    config.contentsCss = '/libs/ckeditor/contents.css';
    //the next line add the new font to the combobox in CKEditor
	
    config.font_names = 'IRANSansWeb/IRANSansWeb;' + config.font_names;

};

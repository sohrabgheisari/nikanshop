﻿using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Shop.Infrastructure.Framework;
using Shop.Infrastructure.Security;
using Shop.Services.Security;
using Shop.Services.Users;
using Shop.Core.Infrastructure;
using Microsoft.AspNetCore.Http;
using Shop.Services.Caching;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Shop.Services.Shop;

namespace Shop.Infrastructure
{
    public class DependencyRegistrar
    {
        public void Register(IServiceCollection services)
        {
            // App
            services.AddSingleton<IActionContextAccessor, ActionContextAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IStaticCacheManager, MemoryCacheManager>();
            services.AddScoped<ICacheManager, MemoryCacheManagerWrapper>();
            services.AddScoped<IAppContext, AppContext>();
            services.AddScoped<ScriptManager>();
            services.AddScoped<StyleManager>();
            services.AddScoped<UserManager>();
            services.AddScoped<SignInManager>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddScoped<JwtTokenService>();
            services.AddScoped<IEncryptionService, EncryptionService>();
            services.AddSingleton<PartialViewResultExecutor>();
          
           services.AddScoped(typeof(IMessageService), typeof(MessageService));
           services.AddSingleton<IEmailSender, EmailSender>();
           services.AddScoped(typeof(IGalleryService), typeof(GalleryService));
           services.AddScoped(typeof(ICategoryService), typeof(CategoryService));
           services.AddScoped(typeof(IProductService), typeof(ProductService));
           services.AddScoped(typeof(IDiscountService), typeof(DiscountService));
           services.AddScoped(typeof(IProductImageService), typeof(ProductImageService));
           services.AddScoped(typeof(IOrderService), typeof(OrderService));
           services.AddScoped(typeof(ISettingService), typeof(SettingService));
           services.AddScoped(typeof(IZarinPalService), typeof(ZarinPalService));
           services.AddScoped(typeof(IMellatService), typeof(MellatService));
           services.AddScoped(typeof(ISliderService), typeof(SliderService));
           services.AddScoped(typeof(IPropertyCategoryService), typeof(PropertyCategoryService));
           services.AddScoped(typeof(IPropertyService), typeof(PropertyService));
           services.AddScoped(typeof(IProductVisitService), typeof(ProductVisitService));
           services.AddScoped(typeof(ITraitService), typeof(TraitService));
           services.AddScoped(typeof(IDerakService), typeof(DerakService));
           services.AddScoped(typeof(IBrandService), typeof(BrandService));
           services.AddScoped(typeof(INikanCustomerEquivalentService), typeof(NikanCustomerEquivalentService));
           services.AddScoped(typeof(IProductFavoriteService), typeof(ProductFavoriteService));
        }
    }
}

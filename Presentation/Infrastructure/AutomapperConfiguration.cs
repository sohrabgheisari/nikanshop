﻿using AutoMapper;
using Shop.Areas.Admin.Models.User;
using Shop.Core.Domain.Users;
using Shop.Core.Infrastructure;
using System;
using Shop.Core.Domain.Shop;
using Shop.Models.Message;
using Shop.Areas.Admin.Models.Message;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.Product;
using Shop.Areas.Admin.Models.Discount;
using Shop.Areas.Admin.Models.ProductImage;
using Shop.Models.Account;
using Shop.Services.Dto.Product;
using Shop.Areas.Admin.Models.Order;
using Shop.Areas.Admin.Models.PropertyCategory;
using Shop.Areas.Admin.Models.Property;
using Shop.Areas.Admin.Models.ProductProperty;
using Shop.Areas.Admin.Models.NikanProductEquivalent;
using Shop.Areas.Admin.Models.NikanCustomerEquivalent;
using Shop.Services.Dto.Slider;
using Shop.Services.Dto.Brand;

namespace Shop.Infrastructure
{
    public class DateTimeToPersianDateConverter : ITypeConverter<DateTime, string>
    {
        public string Convert(DateTime source, string destination, ResolutionContext context)
        {
            return source.ToPersianDate();
        }
    }
    public class NullableDateTimeToPersianDateConverter : ITypeConverter<DateTime?, string>
    {
        public string Convert(DateTime? source, string destination, ResolutionContext context)
        {
            return source.ToPersianDate();
        }
    }
    public class TimeSpanToStringConverter : ITypeConverter<TimeSpan, string>
    {
        public string Convert(TimeSpan source, string destination, ResolutionContext context)
        {
            return source.ToPersianTime();
        }
    }
    public class NullableTimeSpanToStringConverter : ITypeConverter<TimeSpan?, string>
    {
        public string Convert(TimeSpan? source, string destination, ResolutionContext context)
        {
            return source.ToPersianTime();
        }
    }
    public class AutomapperConfiguration : Profile
    {

        public AutomapperConfiguration()
        {
            //convert null to empty string
            CreateMap<string, string>().ConvertUsing(s => s ?? string.Empty);

            // این تنظیم سراسری هست و به تمام خواص زمانی اعمال می‌شود
            CreateMap<DateTime, string>().ConvertUsing(new DateTimeToPersianDateConverter());
            CreateMap<DateTime?, string>().ConvertUsing(new NullableDateTimeToPersianDateConverter());
            CreateMap<TimeSpan, string>().ConvertUsing(new TimeSpanToStringConverter());
            CreateMap<TimeSpan?, string>().ConvertUsing(new NullableTimeSpanToStringConverter());

            #region Message
            CreateMap<Message, MessageModel>().ReverseMap();
            CreateMap<Message, MessageViewModel>().ReverseMap();
            #endregion

            #region User

            CreateMap<User, UserModel>().ReverseMap();
            CreateMap<User, RegisterModel>().ReverseMap();
            CreateMap<User, UserIndexModel>().ReverseMap();
            CreateMap<User, EditUserModel>().ReverseMap();

            #endregion

            #region Gallery
            CreateMap<Gallery, GalleryModel>().ReverseMap();
            #endregion

            #region Category
            CreateMap<Category, CategoryModel>().ReverseMap();
            #endregion

            #region Product
            CreateMap<Product, ProductModel>().ReverseMap();
            CreateMap<Product, ProductShopIndexModel>().ReverseMap();
            CreateMap<Product, ProductIndexModel>().ReverseMap();
            CreateMap<Product, ProductCardModel>().ReverseMap();
            #endregion

            #region ProductImage
            CreateMap<ProductImage, ProductImageModel>().ReverseMap();
            #endregion

            #region Discount
            CreateMap<Discount, DiscountCardModel>();
            CreateMap<Discount, DiscountModel>();
            CreateMap<Discount, DiscountIndexModel>().ReverseMap();

            CreateMap<DiscountModel, Discount>()
           .ForMember(d => d.DiscountFrom, m => m.MapFrom(s => s.DiscountFrom.ToMiladi()))
           .ForMember(d => d.DiscountTo, m => m.MapFrom(s => s.DiscountTo.ToMiladi()));
            #endregion

            #region Order
            CreateMap<Order, OrderIndexModel>()
           .ForMember(d => d.PaidAt, m => m.MapFrom(s => s.PaidAt.ToPersianDateTime()))
           .ForMember(d => d.CreatedAt, m => m.MapFrom(s => s.CreatedAt.ToPersianDateTime()));
            #endregion

            #region Slider
            CreateMap<Slider, SliderModel>().ReverseMap();
            #endregion

            #region PropertyCategory
            CreateMap<PropertyCategory, PropertyCategoryModel>().ReverseMap();
            #endregion 
            
            #region PropertyCategory
            CreateMap<Property, PropertyModel>().ReverseMap();
            CreateMap<Property, PropertyIndexModel>().ReverseMap();
            #endregion

            #region ProductProperty
            CreateMap<ProductProperty, ProductPropertyIndexModel>().ReverseMap();
            #endregion

            #region Trait
            CreateMap<Trait, TraitModel>().ReverseMap();
            CreateMap<ProductTrait, ProductTraitsModel>().ReverseMap();
            #endregion

            #region NikanProductEquivalent
            CreateMap<NikanProductEquivalent, NikanProductEquivalentModel>().ReverseMap();
            #endregion

            #region NikanCustomerEquivalent
            CreateMap<NikanCustomerEquivalent, NikanCustomerEquivalentModel>().ReverseMap();
            #endregion
            
            #region Brand
            CreateMap<Brand, BrandModel>().ReverseMap();
            #endregion
        }
    }
}

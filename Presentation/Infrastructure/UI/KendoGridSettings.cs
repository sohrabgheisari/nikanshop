﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Infrastructure.UI
{
    public static class KendoGridSetting
    {
        public static int[] PageSizes = new int[] { 5, 10, 20, 30, 50, 100 };
        public static int DefaultPageSize = 10;
    }
}

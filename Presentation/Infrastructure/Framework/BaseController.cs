﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Shop.Infrastructure.Framework
{
    public class BaseController : Controller
    {
        private const string SUCCESS = "success";
        private const string ERROR = "error";

        /// <summary>
        /// Display success notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void NotifySuccess(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(SUCCESS, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void NotifyError(string message, bool persistForTheNextRequest = true)
        {
            AddNotification(ERROR, message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display error notification
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        /// <param name="logException">A value indicating whether exception should be logged</param>
        protected virtual void NotifyError(Exception exception, bool persistForTheNextRequest = true)
        {
            AddNotification(ERROR, exception.Message, persistForTheNextRequest);
        }
        /// <summary>
        /// Display notification
        /// </summary>
        /// <param name="type">Notification type</param>
        /// <param name="message">Message</param>
        /// <param name="persistForTheNextRequest">A value indicating whether a message should be persisted for the next request</param>
        protected virtual void AddNotification(string type, string message, bool persistForTheNextRequest)
        {
            string dataKey = string.Format("nk.notify.{0}", type);
            if (persistForTheNextRequest)
            {
                if (TempData[dataKey] == null)
                {
                    TempData[dataKey] = new List<string>();
                }

                if (TempData[dataKey] is string[])
                {
                    var arr = TempData[dataKey] as string[];
                    var list = new List<string>(arr);
                    TempData[dataKey] = list;
                }

                ((List<string>)TempData[dataKey]).Add(message);
            }
            else
            {
                if (ViewData[dataKey] == null)
                {
                    ViewData[dataKey] = new List<string>();
                }

                if (ViewData[dataKey] is string[])
                {
                    var arr = ViewData[dataKey] as string[];
                    var list = new List<string>(arr);
                    ViewData[dataKey] = list;
                }

                ((List<string>)ViewData[dataKey]).Add(message);
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Builder;

namespace Shop.Infrastructure.Localizarions
{
    public static class JsonLocalizerMiddlewareExtensions
    {
        public static IApplicationBuilder UseJsonLocalizer(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JsonLocalizerMiddleware>();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using NikanSms.Shared;
using Shop.Core.Configuration;
using Shop.Core.Domain.Users;
using Shop.Core.Exceptions;
using Shop.Data;
using Shop.Infrastructure;
using Shop.Infrastructure.Framework;
using Shop.Infrastructure.Localizarions;
using Shop.Infrastructure.Security;
using Shop.Services.Users;
using Swashbuckle.AspNetCore.Swagger;
using WebMarkupMin.AspNetCore2;

namespace Shop
{
    public class Startup
    {
        #region Fields

        private IServiceProvider _serviceProvider;
        private readonly IHostingEnvironment _environment;

        #endregion

        #region Constructors

        public Startup(IHostingEnvironment env, IHostingEnvironment environment)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            _environment = environment;
            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        #endregion

        #region Properties

        public IConfigurationRoot Configuration { get; }

        #endregion

        #region Configure

        public void ConfigureServices(IServiceCollection services)
        {
            var keysFolder = Path.Combine(_environment.ContentRootPath, "Keys");
            services.AddDataProtection()
                    .PersistKeysToFileSystem(new DirectoryInfo(keysFolder))
                    .SetApplicationName("Shop")
                    .SetDefaultKeyLifetime(TimeSpan.FromDays(90));

            services.AddAutoMapper();
            services.AddKendo();
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
            services.Configure<AppConfig>(options => Configuration.GetSection("AppConfig").Bind(options));

            services.AddDistributedMemoryCache();
            services.AddSession(options=> {
                options.IdleTimeout = TimeSpan.FromDays(30);
                options.Cookie.HttpOnly = true;
                options.Cookie.IsEssential = true;
            });

            services.AddWebMarkupMin(option => {
                option.AllowCompressionInDevelopmentEnvironment = true;
                option.AllowMinificationInDevelopmentEnvironment = true;
            })
             .AddHtmlMinification()
             .AddHttpCompression();

            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddNikanSms(setupAction: options => Configuration.GetSection("NikanSms").Bind(options),
                                 activeSmsPanel: Configuration.GetValue<string>("NikanSms:ActiveSmsPanel"));
            services.AddIdentity<User, Role>(options =>
                {

                    options.Password.RequireDigit = false;
                    options.Password.RequiredLength = 5;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireUppercase = false;
                    options.User.RequireUniqueEmail = false;
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                })
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultTokenProviders();

            services.AddDataProtection(configure =>
            {
                configure.ApplicationDiscriminator = "nikan.Base";
            });

            services.AddJsonLocalization(options =>
            {
                options.ResourcesPath = "Resources";
                options.GlobalResourceFileName = "global";
                options.AreasResourcePrefix = "areas";
            });

            services.Configure<RequestLocalizationOptions>(options =>
            {
                T Conf<T>(T cult) where T : CultureInfo
                {
                    cult.NumberFormat.NumberDecimalSeparator = ".";
                    cult.NumberFormat.NumberGroupSeparator = ",";
                    cult.NumberFormat.CurrencyDecimalSeparator = ".";
                    cult.DateTimeFormat.AMDesignator = "AM";
                    cult.DateTimeFormat.PMDesignator = "PM";
                    cult.DateTimeFormat.FullDateTimePattern = "yyyy/MM/dd HH:mm:ss.fff";

                    return cult;
                }

                var supportedCultures = new List<CultureInfo>
                {
                    Conf(new CustomPersianCultureInfo("fa-IR")),
                    Conf(new CustomPersianCultureInfo("fa")),
                    Conf(new CultureInfo("en-US")),
                    Conf(new CultureInfo("en-GB")),
                    Conf(new CultureInfo("en"))
                };

                options.DefaultRequestCulture = new RequestCulture("fa");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
            });


           // services.AddMemoryCache();
            services.AddMvc()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .AddViewLocalization()
                .AddDataAnnotationsLocalization()
                .AddFluentValidation(fvc =>
                    fvc.RegisterValidatorsFromAssemblyContaining<Startup>());


            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "1",
                    Title = "Base Project",
                });
            });

            AddAuthentication(services);

            //JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            //{
            //    Formatting = Formatting.None,
            //    ContractResolver = new DefaultContractResolver()
            //};

            new DependencyRegistrar().Register(services);
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IServiceProvider serviceProvider, IOptions<RequestLocalizationOptions> localizationOptions)
        {
            this._serviceProvider = serviceProvider;

            #region Errors

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/error");
                app.UseStatusCodePagesWithReExecute("/error/{0}");
            }
            //app.UseExceptionHandler("/error");
            //app.UseStatusCodePagesWithReExecute("/error/{0}");
            #endregion

            #region Localization

            app.UseJsonLocalizer();
            // app.UseRequestLocalization(localizationOptions.Value);
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            var cookieProvider = options.Value.RequestCultureProviders
                .OfType<CookieRequestCultureProvider>()
                .First();
            var urlProvider = options.Value.RequestCultureProviders
                .OfType<QueryStringRequestCultureProvider>().First();
            cookieProvider.Options.DefaultRequestCulture = new RequestCulture("fa-IR");
            urlProvider.Options.DefaultRequestCulture = new RequestCulture("fa-IR");
            cookieProvider.CookieName = "UserCulture";
            options.Value.RequestCultureProviders.Clear();
            options.Value.RequestCultureProviders.Add(cookieProvider);
            options.Value.RequestCultureProviders.Add(urlProvider);
            urlProvider.Options.DefaultRequestCulture.Culture.NumberFormat.NumberDecimalSeparator = ".";
            app.UseRequestLocalization(options.Value);
            #endregion

            app.UseWebMarkupMin();

            app.UseSession();

            UseAthentication(app);

            #region Static Files

            var provider = new FileExtensionContentTypeProvider();
            provider.Mappings[".apk"] = "application/vnd.android.package-archive";
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    const int durationInSeconds = 60 * 60 * 24 * 7;
                    var cachePeriod = env.IsDevelopment() ? 600 : durationInSeconds;
                    ctx.Context.Response.Headers[HeaderNames.CacheControl] = "public,max-age=" + cachePeriod;
                },
                ContentTypeProvider = provider
            });

            #endregion

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                  name: "areas",
                  template: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
                );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");


                routes.MapRoute(
                    name: "apis",
                    template: "api/{controller}/{action}/{id?}");

            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            //app.Run(async (context) =>
            //{
            //    context.Response.StatusCode = 404;
            //    await context.Response.WriteAsync("404 - Nothing found.");
            //});
        }

        #endregion

        #region Authentication

        private SymmetricSecurityKey signingKey;

        private void AddAuthentication(IServiceCollection services)
        {
            services.AddAuthentication()
                    .AddCookie()
                    .AddJwtBearer(options =>
                    {
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = GetSigningKey(),
                            ValidateIssuer = true,
                            ValidIssuer = "http://nikan.com",
                            ValidateAudience = true,
                            ValidAudience = "api-users",
                            ValidateLifetime = false,
                            ClockSkew = TimeSpan.Zero
                        };
                    });

            services.ConfigureApplicationCookie(options =>
            {
                options.SlidingExpiration = true;
                options.ExpireTimeSpan = TimeSpan.FromDays(90);
                options.LoginPath = new PathString("/account/login");
                options.AccessDeniedPath = new PathString("/account/login");
                options.LogoutPath = new PathString("/account/logout");
                options.Cookie.Name = "ShopCookie";//change this name for every project
            });
        }

        private void UseAthentication(IApplicationBuilder app)
        {
            #region Token Provider

            async Task<ClaimsIdentity> GetIdentity(string username, string password)
            {
                //if we try to login after changing password,"CheckPasswordAsync" method returns false ,because EFCore caches data.(if we reastart iis it will work!!!)
                //so we need new instance of Dbcontext and create new instance of Usermanager on the new created context
                using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
                {
                    var um = serviceScope.ServiceProvider.GetService<UserManager>();
                    var rm = serviceScope.ServiceProvider.GetService<RoleManager<Role>>();
                    if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                    {
                        var user = await um.FindByNameAsync(username);
                        if (user != null)
                        {
                            var valid = await um.CheckPasswordAsync(user, password);
                            if (valid)
                            {
                                if (!user.IsActive)
                                    throw new NikanException("حساب کاربری شما غیر فعال شده است");

                                var jwtGenerator = new JwtTokenService(um, rm);
                                var claimIdentity = await jwtGenerator.GetUserClaims(user);
                                return claimIdentity;
                            }
                        }
                    }
                }

                // Credentials are invalid, or account doesn't exist
                return null;
            }

            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/api/token",
                Audience = "api-users",
                Issuer = "http://nikan.com",
                Expiration = TimeSpan.FromDays(365),
                SigningCredentials = new SigningCredentials(GetSigningKey(), SecurityAlgorithms.HmacSha256),
                IdentityResolver = GetIdentity,
            });

            #endregion

            app.UseAuthentication();
        }

        private SymmetricSecurityKey GetSigningKey()
        {
            if (signingKey != null)
            {
                return signingKey;
            }

            var secretKey = Configuration.AsEnumerable().FirstOrDefault(x => x.Key == "AppConfig:JwtSecretKey").Value;
            if (!string.IsNullOrEmpty(secretKey))
            {
                signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
                return signingKey;
            }
            throw new Exception("SecretKey is not available");
        }

        #endregion
    }
}

﻿using System.ComponentModel.DataAnnotations;
using Shop.Areas.Admin.Models;

namespace Shop.Models.Message
{
    public class MessageModel:BaseModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }

        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public string Phone { get; set; }
    }
}

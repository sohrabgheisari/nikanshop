﻿using NikanBase.Core.Enums;
using Shop.Services.Dto.Product;
using System.Collections.Generic;

namespace Shop.Models.ShopCard
{
    public class ShippingModel
    {
        public string Address { get; set; }
        public long OrderId { get; set; }
        public PaymentTypes PaymentTypes { get; set; }
        public List<ShopCardModel> Card { get; set; }
    }
}

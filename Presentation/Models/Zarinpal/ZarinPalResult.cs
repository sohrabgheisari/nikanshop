﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.Models.Zarinpal
{
    public class ZarinPalResult
    {
        public int StatusCode { get; set; }
        public string TransactionNumber { get; set; }
        public string RefNumber { get; set; }
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}

﻿namespace Shop.Models.Account
{
    public class ResetPasswordModel
    {
        public string UserName { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string Code { get; set; }
    }
}

﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Shop.Core.Infrastructure;
using Shop.Models.Account;
using Shop.Services.Users;

namespace Shop.Validators.Account
{
    public class RegisterModelValidator : AbstractValidator<RegisterModel>
    {
        private readonly UserManager userManager;

        public RegisterModelValidator(IStringLocalizer<RegisterModel> localizer, UserManager userManager)
        {
            this.userManager = userManager;

            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.UserName)
              .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Username"].Value));

            RuleFor(x => x.LastName)
               .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.LastName"].Value));

            RuleFor(x => x.FirstName)
           .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.FirstName"].Value));

            RuleFor(x => x.Address)
           .NotEmpty().WithMessage("آدرس اجباری است")
           .MaximumLength(1000).WithMessage("حداکثر کاراکتر مجاز برای آدرس 1000 کاراکتر می باشد");

            RuleFor(x => x.PhoneNumber)
           .NotEmpty().WithMessage("شماره موبایل اجباری است");

            RuleFor(x => x.PhoneNumber.PersianToEnglish())
              .Matches("09(0[1-9]|1[0-9]|3[0-9]|2[0-9])-?[0-9]{3}-?[0-9]{4}")
              .WithMessage("شماره موبایل صحیح نمی باشد");


            RuleFor(x => x)
                .Must(x => userManager.FindByNameAsync(x.UserName).Result == null)
                  .WithMessage(localizer["user.msg.DuplicateUserName"].Value)
                  .Must(x => userManager.GetByPhoneNumber(x.PhoneNumber).Result == null)
                  .WithMessage("شماره موبایل تکراری می باشد");

            RuleFor(x => x.Password)
             .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Password"].Value));

            RuleFor(x => x.ConfirmPassword)
                .Equal(x => x.Password).WithMessage(localizer["user.changePassword.PasswordMatch"].Value)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.ConfirmPassword"].Value));
        }
    }
}

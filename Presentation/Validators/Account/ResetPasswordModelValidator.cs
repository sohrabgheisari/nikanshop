﻿using FluentValidation;
using Shop.Models.Account;

namespace Shop.Validators.Account
{
    public class ResetPasswordModelValidator : AbstractValidator<ResetPasswordModel>
    {
        public ResetPasswordModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.NewPassword)
           .NotEmpty().WithMessage("رمز جدید اجباری است");

            RuleFor(x => x.ConfirmPassword)
                .Equal(x => x.NewPassword).WithMessage("رمزهای وارد شده یکسان نیستند")
                .NotEmpty().WithMessage("تکرار رمز عبور جدید اجباری است");
        }
    }
}

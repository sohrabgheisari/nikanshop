﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Shop.Models.Message;

namespace Shop.Validators.Message
{
    public class MessageModelValidator: AbstractValidator<MessageModel>
    {
        public MessageModelValidator(IStringLocalizer<MessageModel> localizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;
            RuleFor(x => x.FullName)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["contact.field.FullName"].Value));

            RuleFor(x => x.Email)
                .EmailAddress().WithMessage(string.Format(localizer["msg.invalidValue"].Value, localizer["contact.field.Email"].Value))
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["contact.field.Email"].Value));

            RuleFor(x => x.Phone)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["contact.field.Phone"].Value))
                .Matches("09(0[1-9]|1[0-9]|3[0-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}").WithMessage(string.Format(localizer["msg.invalidValue"].Value, localizer["contact.field.Phone"].Value));

            RuleFor(x => x.Subject)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["contact.field.Subject"].Value));


            RuleFor(x => x.Description)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["contact.field.Description"].Value));
        }
    }
}

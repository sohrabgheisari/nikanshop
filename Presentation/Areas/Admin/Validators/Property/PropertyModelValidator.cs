﻿using FluentValidation;
using Shop.Areas.Admin.Models.Property;

namespace Shop.Areas.Admin.Validators.Property
{
    public class PropertyModelValidator : AbstractValidator<PropertyModel>
    {
        public PropertyModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Name)
               .NotEmpty()
               .WithMessage("نام ویژگی اجباری است")
               .MaximumLength(50).WithMessage("حداکثر کاراکتر مجاز برای نام ویژگی 50 کاراکتر می باشد");

            RuleFor(x => x.PropertCategoryId)
                .NotEmpty()
                .WithMessage("انتخاب دسته اجباری است");
        }
    }
}

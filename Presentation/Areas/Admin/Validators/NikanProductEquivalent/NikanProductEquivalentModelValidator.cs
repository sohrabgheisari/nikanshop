﻿using FluentValidation;
using Shop.Areas.Admin.Models.NikanProductEquivalent;

namespace Shop.Areas.Admin.Validators.NikanProductEquivalent
{
    public class NikanProductEquivalentModelValidator : AbstractValidator<NikanProductEquivalentModel>
    {
        public NikanProductEquivalentModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.NikanProductId)
                .NotEmpty()
                .WithMessage("انتخاب محصول نیکان اجباری است");

            RuleFor(x => x.ShopProductId)
                .NotEmpty()
                .WithMessage("انتخاب محصول فروشگاه اجباری است");
        }
    }
}

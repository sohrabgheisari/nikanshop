﻿using FluentValidation;
using Shop.Services.Dto.Slider;

namespace Shop.Areas.Admin.Validators.Slider
{
    public class SliderModelValidator : AbstractValidator<SliderModel>
    {
        public SliderModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Type)
                .NotEmpty()
                .WithMessage("نوع اسلایدر اجباری است");

        }
    }
}

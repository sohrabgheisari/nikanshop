﻿using FluentValidation;
using Shop.Areas.Admin.Models.NikanCustomerEquivalent;

namespace Shop.Areas.Admin.Validators.NikanCustomerEquivalent
{
    public class NikanCustomerEquivalentModelValidator : AbstractValidator<NikanCustomerEquivalentModel>
    {
        public NikanCustomerEquivalentModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.NikanCustomerId)
                .NotEmpty()
                .WithMessage("انتخاب مشتری نیکان اجباری است");

            RuleFor(x => x.ShopUserId)
                .NotEmpty()
                .WithMessage("انتخاب کاربر فروشگاه اجباری است");
        }
    }
}

﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Shop.Areas.Admin.Models.User;
using Shop.Services.Users;

namespace Shop.Areas.Admin.Validators.User
{
    public class UserModelValidator : AbstractValidator<UserModel>
    {
        private readonly UserManager userManager;

        public UserModelValidator(IStringLocalizer<UserModel> localizer, UserManager userManager)
        {
            this.userManager = userManager;

            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.UserName)
              .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Username"].Value));

            RuleFor(x => x.LastName)
               .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.LastName"].Value));

            RuleFor(x => x.FirstName)
           .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.FirstName"].Value));

            RuleFor(x => x).Must(x => userManager.FindByNameAsync(x.UserName).Result == null)
                  .WithMessage(localizer["user.msg.DuplicateUserName"].Value);

            RuleFor(x => x.Password)
             .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Password"].Value));

            RuleFor(x => x.ConfirmPassword)
                .Equal(x => x.Password).WithMessage(localizer["user.changePassword.PasswordMatch"].Value)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.ConfirmPassword"].Value));

            RuleFor(x => x.UserRoleNames)
            .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Role"].Value));
        }
    }
}

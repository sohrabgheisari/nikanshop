﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Shop.Areas.Admin.Models.User;

namespace Shop.Areas.Admin.Validators.User
{
    public class EditUserModelValidator : AbstractValidator<EditUserModel>
    {
        public EditUserModelValidator(IStringLocalizer<EditUserModel> localizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;
            RuleFor(x => x.UserRoleNames)
                .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.field.Role"].Value));
        }
    }
}

﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using Shop.Areas.Admin.Models.User;

namespace Shop.Areas.Admin.Validators.User
{
    public class SetNewPasswordModelValidator : AbstractValidator<SetNewPasswordModel>
    {

        public SetNewPasswordModelValidator(IStringLocalizer<SetNewPasswordModel> localizer)
        {

            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.NewPassword)
               .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.changePassword.field.NewPassword"].Value));

            RuleFor(x => x.ConfirmPassword)
             .NotEmpty().WithMessage(string.Format(localizer["msg.required"].Value, localizer["user.changePassword.field.ConfirmNewPassword"].Value))
             .Equal(m => m.NewPassword).WithMessage(localizer["user.changePassword.PasswordMatch"].Value);

        }
    }
}

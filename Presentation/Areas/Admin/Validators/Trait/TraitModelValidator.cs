﻿using FluentValidation;
using Shop.Services.Dto.Product;

namespace Shop.Areas.Admin.Validators.Trait
{
    public class TraitModelValidator : AbstractValidator<TraitModel>
    {
        public TraitModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Type)
                .NotEmpty()
                .WithMessage("انتخاب نوع مشخصه اجباری است");

            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("عنوان مشخصه اجباری است")
                .MaximumLength(100)
                .WithMessage("حداکثر کاراکتر مجاز برای عنوان مشخصه 100 کاراکتر است");
        }
    }
}

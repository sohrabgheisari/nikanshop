﻿using FluentValidation;
using Shop.Services.Dto.Brand;

namespace Shop.Areas.Admin.Validators.Brand
{
    public class BrandModelValidator : AbstractValidator<BrandModel>
    {
        public BrandModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("نام برند اجباری است")
                .Must(x => x.Contains("/") == false)
                .WithMessage("کاراکتر / مجاز نمی باشد")
                .Must(x => x.Contains("\\") == false)
                .WithMessage("کاراکتر \\ مجاز نمی باشد");
        }
    }
}

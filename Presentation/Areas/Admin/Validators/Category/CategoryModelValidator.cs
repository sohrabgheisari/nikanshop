﻿using FluentValidation;
using Shop.Areas.Admin.Models.Category;

namespace Shop.Areas.Admin.Validators.Category
{
    public class CategoryModelValidator : AbstractValidator<CategoryModel>
    {
        public CategoryModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Title)
                .NotEmpty()
                .WithMessage("عنوان دسته اجباری است")
                .Must(x => x.Contains("/") == false)
                .WithMessage("کاراکتر / مجاز نمی باشد")
                .Must(x => x.Contains("\\") == false)
                .WithMessage("کاراکتر \\ مجاز نمی باشد");
        }
    }
}

﻿using FluentValidation;
using Shop.Areas.Admin.Models.PropertyCategory;

namespace Shop.Areas.Admin.Validators.PropertyCategory
{
    public class PropertyCategoryModelValidator : AbstractValidator<PropertyCategoryModel>
    {
        public PropertyCategoryModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("نام دسته اجباری است")
                .MaximumLength(50).WithMessage("حداکثر کاراکتر مجاز برای نام دسته 50 کاراکتر می باشد");
        }
    }
}

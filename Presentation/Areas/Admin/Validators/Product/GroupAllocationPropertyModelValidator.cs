﻿using FluentValidation;
using Shop.Services.Dto.Product;
using System.Linq;

namespace Shop.Areas.Admin.Validators.Product
{
    public class GroupAllocationPropertyModelValidator : AbstractValidator<GroupAllocationPropertyModel>
    {
        public GroupAllocationPropertyModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x)
                .Must(x => x.GroupProperties != null && x.GroupProperties.Count() > 0)
                .WithMessage("دسته ای انتخاب نشده است");
        }
    }
}

﻿using FluentValidation;
using Shop.Services.Dto.Product;

namespace Shop.Areas.Admin.Validators.Product
{
    public class ProductModelValidator : AbstractValidator<ProductModel>
    {
        public ProductModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Name)
                .NotEmpty().WithMessage("نام محصول اجباری است")
                .MaximumLength(100).WithMessage("حداکثر کاراکتر مجاز برای نام محصول 100 کاراکتر است"); 
            
            RuleFor(x => x.EnglishName)
                .MaximumLength(100).WithMessage("حداکثر کاراکتر مجاز برای نام انگلیسی محصول 100 کاراکتر است");

            RuleFor(x => x.BarCode)
                .MaximumLength(100).WithMessage("حداکثر کاراکتر مجاز برای بارکد 50 کاراکتر است");

            RuleFor(x => x.Description)
                .NotEmpty().WithMessage("توضیحات محصول اجباری است");

            RuleFor(x => x.CategoryId)
                .Must(x => x > 0).WithMessage("دسته محصول اجباری است");

            RuleFor(x => x.Score)
                .Must(x=>x>=0)
                .WithMessage("امتیاز را وارد نمایید");
        }
    }
}

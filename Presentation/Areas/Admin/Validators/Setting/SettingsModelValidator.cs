﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using NikanBase.Core.Enums;
using Shop.Areas.Admin.Models.Setting;
using System;

namespace Shop.Areas.Admin.Validators.Setting
{
    public class SettingsModelValidator :AbstractValidator<SettingsModel>
    {
        public SettingsModelValidator(IStringLocalizer<SettingsModel> localizer)
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;
             
            RuleFor(x => x.GetwaySettings.BankGateway_Type)
                .Must(x => x > 0).WithMessage("نوع درگاه اجباری است");

            RuleFor(x => x.GetwaySettings.BankGateway_TerminalNo)
                .NotEmpty().WithMessage("شماره ترمینال بانک اجباری است")
                .When(x => x.GetwaySettings.BankGateway_Type == ZarinPalTypes.ZarinPal);

            RuleFor(x=>x.MellatGetwaySetting.TerminalId)
                .NotEmpty().WithMessage("شماره ترمینال بانک ملت اجباری است")
                .When(x => String.IsNullOrEmpty(x.MellatGetwaySetting.UserName)==false || String.IsNullOrEmpty(x.MellatGetwaySetting.Password) == false);

            RuleFor(x=>x.MellatGetwaySetting.UserName)
                .NotEmpty().WithMessage("نام کاربری درگاه بانک ملت اجباری است")
                .When(x => String.IsNullOrEmpty(x.MellatGetwaySetting.TerminalId) ==false || String.IsNullOrEmpty(x.MellatGetwaySetting.Password) == false);

            RuleFor(x=>x.MellatGetwaySetting.Password)
                .NotEmpty().WithMessage("رمز عبور درگاه بانک ملت اجباری است")
                .When(x => String.IsNullOrEmpty(x.MellatGetwaySetting.TerminalId) ==false || String.IsNullOrEmpty(x.MellatGetwaySetting.UserName) == false);

            RuleFor(x => x.DerakSetting.PublicKey)
                .NotEmpty()
                .WithMessage("کلید دراک اجباری است")
                .When(x => String.IsNullOrEmpty(x.DerakSetting.BaseUrl) == false);

            RuleFor(x => x.DerakSetting.BaseUrl)
                .NotEmpty()
                .WithMessage("آدرس دراک اجباری است")
                .When(x => String.IsNullOrEmpty(x.DerakSetting.PublicKey) == false);


        }
    }
}

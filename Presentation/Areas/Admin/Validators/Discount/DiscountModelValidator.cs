﻿using FluentValidation;
using System;
using Shop.Areas.Admin.Models.Discount;

namespace Shop.Areas.Admin.Validators.Discount
{
    public class DiscountModelValidator:AbstractValidator<DiscountModel>
    {
        public DiscountModelValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;

            RuleFor(x => x.Type)
                .NotNull().WithMessage("نوع تخفیف اجباری است")
                .Must(x => (int)x >= 1).WithMessage("نوع تخفیف اجباری است");  
            
            RuleFor(x => x.Tittle)
                .NotEmpty().WithMessage("عنوان تخفیف اجباری است")
                .MaximumLength(50).WithMessage("حداکثر کاراکتر مجاز برای عنوان تخفیف 50 کاراکتر می باشد");

            RuleFor(x => x.Amount)
                  .NotNull().WithMessage("مبلغ تخفیف اجباری است")
                  .When(x => x.Type == Core.Enums.DiscountTypes.Amount);

            RuleFor(x => x.Percentage)
                  .NotNull().WithMessage("درصد تخفیف اجباری است")
                  .When(x => x.Type == Core.Enums.DiscountTypes.Percentage);


            RuleFor(x => x.DiscountTo)
               //.NotEmpty().WithMessage("تاریخ پایان را وارد نمایید")
               .GreaterThanOrEqualTo(x => x.DiscountFrom)
                .WithMessage("تاریخ پایان باید بزرگترمساوی تاریخ شروع باشد")
                .When(x => !String.IsNullOrEmpty(x.DiscountFrom) && !String.IsNullOrEmpty(x.DiscountTo));

            RuleFor(x => x.DiscountFrom)
             .NotEmpty().WithMessage("تاریخ شروع را وارد نمایید")
              .When(x => !String.IsNullOrEmpty(x.DiscountTo));
        }
    }
}

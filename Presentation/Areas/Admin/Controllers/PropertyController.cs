﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Property;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Infrastructure;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class PropertyController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IPropertyService _propertyService;
        private readonly IPropertyCategoryService _propertyCategoryService;
        #endregion

        #region Constructors
        public PropertyController(
            IStringLocalizer<PropertyModel> localizer,
            IMapper mapper, IPropertyService propertyService,
            IPropertyCategoryService propertyCategoryService,
            ILogger<PropertyController> logger
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _propertyService = propertyService;
            _propertyCategoryService = propertyCategoryService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _propertyService.GetAllAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<PropertyIndexModel>>(result.Data);
            models.ForEach(x => x.FillFields());
            result.Data = models;
            return result;
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                PropertyModel model = new PropertyModel();
                ViewBag.Categories = GetPropertyCategorySelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(PropertyModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Property>(model);
                await _propertyService.InsertProperty(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _propertyService.GetPropertyById(id);
                var model = _mapper.Map<PropertyModel>(entity);
                ViewBag.Categories = GetPropertyCategorySelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(PropertyModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Property>(model);
                await _propertyService.UpdateProperty(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                await _propertyService.DeleteProperty(new Property() {Id=id });
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]/{catId}")]
        public Task<ResponseState<List<PropertyModel>>> GetPropertyInCategory(long catId)
        {
            return TryCatch(async () =>
            {
                var entities = await _propertyService.GetAllPropertyInCategory(catId);
                var models = _mapper.Map<List<PropertyModel>>(entities);
                return Success(data: models);
            });
        }

        #endregion

        #region Utilities
        private List<SelectListItem> GetPropertyCategorySelectList()
        {
            var entities = _propertyCategoryService.GetAllAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in entities)
            {
                selectList.Add(new SelectListItem
                {
                    Text = en.Name,
                    Value = en.Id.ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.ProductImage;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class ProductImageController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IProductImageService _productImageService;
        private readonly IHostingEnvironment _environment;
        private string ImagePath = "ShopImage/ProductGallery";
        #endregion

        #region Constructors
        public ProductImageController(
            IStringLocalizer<ProductImageModel> localizer,
            IMapper mapper, IProductImageService productImageService,
            ILogger<ProductImageController> logger, IHostingEnvironment environment
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _productImageService = productImageService;
            _environment = environment;
        }

        #endregion

        #region Actions
     
        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Index(long id)
        {
            return TryCatch(async () =>
            {
                ViewBag.Id = id;
                var view = await PartialViewToString(PartialView("Index"), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request,long id)
        {
            var entities = _productImageService.GetAllProductImageAsQueryable(id);
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<ProductImageModel>>(result.Data);
            result.Data = models;
            return result;
        }

    
        [HttpGet("[action]/{productId}")]
        public Task<ResponseState<string>> Create(long productId)
        {
            return TryCatch(async () =>
            {
                ProductImageModel model = new ProductImageModel() { ProductId=productId};
                var view = await PartialViewToString(PartialView("Create", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(ProductImageModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
                if (!Directory.Exists(uploadsRootFolder))
                {
                    Directory.CreateDirectory(uploadsRootFolder);
                }
                //تصویر اصلی
                if (ImageUrl != null && ImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        model.ImageUrl = "/"+ ImagePath+"/" + newFilename;
                    };
                }
                else
                {
                    return Error(message: "تصویر محصول الزامی است");
                }
            
                var entity = _mapper.Map<ProductImage>(model);
                await _productImageService.InsertProductImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _productImageService.GetProductImageById(id);
                var model = _mapper.Map<ProductImageModel>(entity);
                var view = await PartialViewToString(PartialView("Edit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(ProductImageModel model)
        {
            return TryCatch(async () =>
            {
                var entity = await _productImageService.GetProductImageById(model.Id);
                entity.Order = model.Order;
                await _productImageService.UpdateProductImage(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _productImageService.GetProductImageById(id);
                await _productImageService.DeleteProductImage(entity);
                DeleteProductImageImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private void  DeleteProductImageImage(ProductImage entity)
        {
            var mainPath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
            var mobilePath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mobilePath))
            {
                System.IO.File.Delete(mobilePath);
            }
        }
        #endregion
    }
}
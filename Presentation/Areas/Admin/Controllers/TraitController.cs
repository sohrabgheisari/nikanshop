﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using NikanBase.Core.Enums;
using Shop.Areas.Admin.Models;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Product;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class TraitController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly ITraitService _traitService;
        #endregion

        #region Constructors
        public TraitController(
            IStringLocalizer<TraitModel> localizer,
            IMapper mapper, ITraitService traitService,
            ILogger<TraitController> logger
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _traitService = traitService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Types = GetTraitTypesSelectList();
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _traitService.GetAllTraitAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<TraitModel>>(result.Data);
            result.Data = models;
            return result;
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                TraitModel model = new TraitModel();
                ViewBag.Types = GetTraitTypesSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(TraitModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Trait>(model);
                await _traitService.InsertTrait(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _traitService.GetTraitById(id);
                var model = _mapper.Map<TraitModel>(entity);
                ViewBag.Types = GetTraitTypesSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(TraitModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Trait>(model);
                await _traitService.UpdateTrait(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                await _traitService.DeleteTrait(new Trait() {Id=id });
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

    

        #endregion

        #region Utilities
        private List<SelectListItem> GetTraitTypesSelectList()
        {
            Array values = Enum.GetValues(typeof(TraitTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}
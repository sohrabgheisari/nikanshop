﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Shop.Areas.Admin.Models;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Services.Dto.Brand;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class BrandController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IBrandService _brandService;
        private readonly IHostingEnvironment _environment;
        private string ImagePath = "ShopImage/Brand";
        #endregion

        #region Constructors
        public BrandController(
            IStringLocalizer<BrandModel> localizer,
            IMapper mapper, IBrandService brandService,
            ILogger<BrandController> logger, IHostingEnvironment environment
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _brandService = brandService;
            _environment = environment;
        }

        #endregion

        #region Actions

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _brandService.GetAllAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<BrandModel>>(result.Data);
            result.Data = models;
            return result;
        }


        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                BrandModel model = new BrandModel();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(BrandModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {             
                await _brandService.InsertBrand(model, ImageUrl);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(string id)
        {
            return TryCatch(async () =>
            {
                var entity = await _brandService.GetBrandById(long.Parse(id));
                var model = _mapper.Map<BrandModel>(entity);
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Edit(BrandModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                await _brandService.UpdateBrand(model, ImageUrl);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                await _brandService.DeleteBrand(id);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

  
    }
}
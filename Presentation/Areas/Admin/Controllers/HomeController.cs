﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Shop.Controllers;
using Shop.Core.Domain.Users;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Shop;
namespace Shop.Areas.Admin.Controllers
{
    [Authorize(Roles = UserRoleNames.Admin + "," + UserRoleNames.User)]
    public class HomeController : BaseAdminController
    {
        #region Fields

        private readonly IMessageService _messageService;
        private readonly IMapper _mapper;
        private readonly IAppContext _appContext;
        private readonly SignInManager<User> _signInManager;

        #endregion

        #region Constructors

        public HomeController(
            IStringLocalizer<HomeController> localizer,
            IMapper mapper,
            ILogger<HomeController> logger,
            IAppContext appContext,
            IMessageService messageService,
            SignInManager<User> signInManager
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _appContext = appContext;
            _messageService = messageService;
            _signInManager = signInManager;
        }


        #endregion

        #region Actions

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await SetViewBag();
            return View();
        }

        [HttpGet("logout")]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);
            return LocalRedirect("/");
        }

        #endregion

        #region Utilities
        private async Task SetViewBag()
        {
            var messagemodel = await _messageService.GetAllMessages();
            ViewBag.Messages = messagemodel.Where(x => x.IsRead == false).Count();
        }
        #endregion
    }
}
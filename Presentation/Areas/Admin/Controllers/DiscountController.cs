﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Discount;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Infrastructure;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class DiscountController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IDiscountService _DiscountService;
        #endregion

        #region Constructors
        public DiscountController(
            IStringLocalizer<DiscountModel> localizer,
            IMapper mapper, IDiscountService DiscountService,
            ILogger<DiscountController> logger
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _DiscountService = DiscountService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _DiscountService.GetAllDescountAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<DiscountIndexModel>>(result.Data);
            result.Data = models;
            return result;
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                DiscountModel model = new DiscountModel();
                ViewBag.Disconts = GetDiscountSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(DiscountModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Discount>(model);
                if (!String.IsNullOrEmpty(model.DiscountFrom))
                {
                    entity.DiscountFrom = model.DiscountFrom.PersianDateToMiladi();
                } 
                if (!String.IsNullOrEmpty(model.DiscountTo))
                {
                    entity.DiscountTo = model.DiscountTo.PersianDateToMiladi();
                }
                await _DiscountService.InsertDiscount(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _DiscountService.GetDiscountById(id);
                var model = _mapper.Map<DiscountModel>(entity);
                ViewBag.Disconts = GetDiscountSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(DiscountModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<Discount>(model);
                if (!String.IsNullOrEmpty(model.DiscountFrom))
                {
                    entity.DiscountFrom = model.DiscountFrom.PersianDateToMiladi();
                }
                else
                {
                    entity.DiscountFrom = null;
                }
                if (!String.IsNullOrEmpty(model.DiscountTo))
                {
                    entity.DiscountTo = model.DiscountTo.PersianDateToMiladi();
                }
                else
                {
                    entity.DiscountTo = null;
                }
                await _DiscountService.UpdateDiscount(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _DiscountService.GetDiscountById(id);
                await _DiscountService.DeleteDiscount(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private List<SelectListItem> GetDiscountSelectList()
        {
            Array values = Enum.GetValues(typeof(DiscountTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}
﻿
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Shop.Controllers;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.User;
using Shop.Core.Domain.Users;
using Shop.Infrastructure.Security;
using Shop.Services.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shop.Core.Infrastructure;
using Shop.Core.Enums;
using Shop.Infrastructure;
using Microsoft.AspNetCore.Identity;
using Shop.Services.Shop;
using Shop.Areas.Admin.Models.NikanCustomerEquivalent;
using Microsoft.AspNetCore.Mvc.Rendering;
using NikanBase.Core.Domain.Shop;
using System;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    public class UserController : BaseAdminController
    {
        #region Fields

        private readonly IAppContext appContext;
        private readonly UserManager userManager;
        private readonly RoleManager<Role> roleManager;
        private readonly SignInManager signInManager;
        private readonly IMapper mapper;
        private readonly INikanCustomerEquivalentService _nikanCustomerEquivalentService;
        private readonly IDerakService _derakService;
        #endregion

        #region Constructors

        public UserController(IAppContext appContext, 
            IStringLocalizer<UserModel> userLocalizer, 
            IMapper mapper, SignInManager signInManager,
            ILogger<UserController> logger, 
            UserManager userManager, RoleManager<Role> roleManager,
            INikanCustomerEquivalentService nikanCustomerEquivalentService,
            IDerakService derakService
            ) :base(logger, userLocalizer)
        {

            this.appContext = appContext;
            this.userManager = userManager;
            this.mapper = mapper;
            this.roleManager = roleManager;
            this.signInManager = signInManager;
            _nikanCustomerEquivalentService = nikanCustomerEquivalentService;
            _derakService = derakService;
        }

        #endregion

        #region Actions

        [HttpGet]
        [Authorize(Roles = UserRoleNames.Admin)]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public async Task<ResponseState<List<UserIndexModel>>> Data_Read()
        {
            return await TryCatch(async () =>
            {
                var result = await userManager.GetAll();
                var models = mapper.Map<List<UserIndexModel>>(result);
                models.ForEach(m => { 
                    m.Roles = RoleExtension.ConvertRolesToLocalizedName(result.FirstOrDefault(n => n.Id == m.Id).UserRoleNames, Localizer);
                    m.NikanUserId = m.NikanCustomerEquivalent != null ? m.NikanCustomerEquivalent.NikanCustomerId.ToString() : "";
                });
                return Success(models);
            });
        }

        [HttpGet("[action]/{id}")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<string>> EditUser(long id)
        {
            return TryCatch(async () =>
            {
                var user = await userManager.GetByIdWithRoles(id);
                var model = mapper.Map<EditUserModel>(user);
                ViewBag.Roles = RoleExtension.ConvertRolesToSelectList(roleManager, Localizer);

                var view = await PartialViewToString(PartialView("EditUser", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<long>> EditUser(EditUserModel model)
        {
            return TryCatch(async () =>
            {
                var user = await userManager.GetById(model.Id);
                user.IsActive = model.IsActive;
                await userManager.Update(user, model.UserRoleNames, true);
                return Success(user.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                var model = new UserModel() { IsActive=true};
                ViewBag.Roles = RoleExtension.ConvertRolesToSelectList(roleManager, Localizer);
                var view = await PartialViewToString(PartialView("CreateUser", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<long>> Create(UserModel model)
        {
            return TryCatch(async () =>
            {
                var entity = mapper.Map<User>(model);

                await userManager.Insert(entity, model.Password, model.UserRoleNames);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> ChangePassword()
        {
            return TryCatch(async () =>
            {
                var model = new ChangePasswordModel();
                var view = await PartialViewToString(PartialView("ChangePassword", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> ChangePassword(ChangePasswordModel model)
        {
            return TryCatch(async () =>
            {
                    var user = await userManager.GetUserAsync(User);
                    if (user == null)
                    {
                        return Error(user.Id, message: "کاربری یافت نشد");
                    }
                    var Result = await userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
                    if (Result.Succeeded)
                    {
                        await signInManager.RefreshSignInAsync(user);
                        return Success(user.Id, message: "رمز عبور با موفقیت تغییر یافت");
                    }
                    return Error(user.Id, message: "رمز عبور قدیم اشتباه است");            
            });
        }

        [HttpGet("[action]/{id}")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<string>> SetNewPassword(long id)
        {
            return TryCatch(async () =>
            {
                var model = new SetNewPasswordModel() { Id = id };
                var view = await PartialViewToString(PartialView("SetNewPass", model), ControllerContext);

                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<long>> SetNewPassword(SetNewPasswordModel model)
        {
            return TryCatch(async () =>
            {
                var user = await userManager.GetById(model.Id);
                string passwordHash = userManager.PasswordHasher.HashPassword(user, model.NewPassword);
                user.PasswordHash = passwordHash;
                var result = await userManager.UpdateAsync(user);
                if (result.Succeeded)
                {
                    return Success(user.Id, message: Localizer["msg.save-succeed"].Value);
                }
                return Error(user.Id, message: "عملیات با شکست مواجه گردید");
            });
        }


        [HttpGet("[action]/{userId}")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<string>> CreateEquivalent(long userId)
        {
            return TryCatch(async () =>
            {
                var eq = await _nikanCustomerEquivalentService.GetCustomerEquivalentById(userId);
                if (eq != null)
                {
                    var res = await EditEquivalent(userId);
                    return Success(data: res.Data);
                }
                NikanCustomerEquivalentModel model = new NikanCustomerEquivalentModel() { ShopUserId = userId };
                ViewBag.NikanUsers = GetNikanUserSelectList();
                var view = await PartialViewToString(PartialView("CreateEditEquivalent", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<DefaultResponseState> CreateEquivalent(NikanCustomerEquivalentModel model)
        {
            return TryCatch(async () =>
            {
                var entity = mapper.Map<NikanCustomerEquivalent>(model);
                await _nikanCustomerEquivalentService.InsertCustomerEquivalent(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpGet("[action]/{productId}")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<string>> EditEquivalent(long userId)
        {
            return TryCatch(async () =>
            {
                var eq = await _nikanCustomerEquivalentService.GetCustomerEquivalentById(userId);
                NikanCustomerEquivalentModel eqModel = mapper.Map<NikanCustomerEquivalentModel>(eq);
                ViewBag.NikanUsers = GetNikanUserSelectList();
                var view = await PartialViewToString(PartialView("CreateEditEquivalent", eqModel), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<ResponseState<long>> EditEquivalent(NikanCustomerEquivalentModel model)
        {
            return TryCatch(async () =>
            {
                var entity = mapper.Map<NikanCustomerEquivalent>(model);
                await _nikanCustomerEquivalentService.UpdateCustomerEquivalent(entity);
                return Success(model.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities

        private string ConvertRoleNameToLocalizedName(List<string> roles)
        {
            var localizedRoles = "";
            foreach (var role in roles)
            {
                localizedRoles += Localizer["UserRoleNames." + role].Value + ",";
            }
            return localizedRoles.Trim(',');
        }

        private List<SelectListItem> GetNikanUserSelectList()
        {
            var users = _derakService.GetNikanCustomer().Result;
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var user in users)
            {
                string text = "";
                text += user.Tel1;
                if (!String.IsNullOrEmpty(user.CustAddr))
                {
                    text = text + "-" + user.CustAddr;
                }
                selectList.Add(new SelectListItem()
                {
                    Value =user.CustId.ToString(),
                    Text = $"{ user.CustName} ( {text} )"
                });
            }
            return selectList.OrderBy(x => x.Text).ToList();
        }
        #endregion
    }
}

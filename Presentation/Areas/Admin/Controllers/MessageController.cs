﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Message;
using Shop.Controllers;
using Shop.Core.Domain.Shop;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Infrastructure;
using Shop.Models.Message;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]/[action]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class MessageController : BaseAdminController
    {

        #region Fields

        private readonly IMapper _mapper;
        private readonly IMessageService _messageService;
        private readonly IAppContext _appContext;
        private readonly IEmailSender _emailSender;
        private readonly IHostingEnvironment _environment;
        #endregion

        #region Constructors
        public MessageController(
            IStringLocalizer<MessageModel> localizer,
            IMapper mapper,
            ILogger<MessageController> logger,
            IAppContext appContext,
            IMessageService messageService,
            IEmailSender emailSender,
            IHostingEnvironment environment
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _appContext = appContext;
            _messageService = messageService;
            _emailSender = emailSender;
            _environment = environment;
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            return View();
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Archive(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
               
                await _messageService.ArchiveMessage(id, true);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Details(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _messageService.GetMessageById(id);
                await _messageService.MessageIsRead(id);
                var model = _mapper.Map<MessageViewModel>(entity);
                var view = await PartialViewToString(PartialView("Details", model), ControllerContext);

                return Success(data: view);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {

            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                await _messageService.DeleteMessage(new Message() { Id = id });
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities =_messageService.GetAllMessagesAsQueryable().Where(x=>x.IsArchived==false);
            var result = await entities.ToDataSourceResultAsync(request);
            result.Data = _mapper.Map<List<MessageViewModel>>(result.Data);
            return result;
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ArchiveIndex()
        {
            return View();
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> UnArchive(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }

                await _messageService.ArchiveMessage(id, false);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_ReadArchive([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _messageService.GetAllMessagesAsQueryable().Where(x => x.IsArchived == true);
            var result = await entities.ToDataSourceResultAsync(request);
            result.Data = _mapper.Map<List<MessageViewModel>>(result.Data);
            return result;
        }

        [HttpPost("[action]")]
        public Task<ResponseState<MessageModel>> CreateMessage([FromBody]MessageModel model)
        {
            return TryCatch(async () =>
            {
                if (ModelState.IsValid)
                {
                    var entity = _mapper.Map<Message>(model);
                    entity.IsArchived = false;
                    entity.IsRead = false;
                    await _messageService.InsertMessage(entity);
                    return Success(model, message: Localizer["msg.save-succeed"].Value);
                }

                var msg = string.Join(" | ", ModelState.Values
                              .SelectMany(v => v.Errors)
                              .Select(e => e.ErrorMessage));

                return Error(model, message: msg);
            });
        }
    }
}
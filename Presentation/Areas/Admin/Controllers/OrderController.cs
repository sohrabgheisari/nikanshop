﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Order;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Models.ShopCard;
using Shop.Services.Dto.Product;
using Shop.Services.Shop;
using Shop.Services.Users;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    public class OrderController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IOrderService _orderService;
        private readonly IAppContext _appContext;
        private readonly UserManager _userManager;
        #endregion

        #region Constructors
        public OrderController(
            IStringLocalizer<OrderIndexModel> localizer,
            IMapper mapper, IOrderService orderService,
            ILogger<OrderController> logger,
             IAppContext appContext,
              UserManager userManager
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _orderService = orderService;
            _appContext = appContext;
            _userManager = userManager;
        }

        #endregion

        #region Actions
        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.User)]
        public IActionResult Index()
        {
            ViewBag.Status = GetStatusTypeSelectList();
            ViewBag.Shippings = GetShippingStatusTypeSelectList();
            return View();
        }

        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.User)]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request, OrderSearchModel model)
        {
            var entities = _orderService.GetAllOrderAsQueryable(_appContext.User.Id, model);
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<OrderIndexModel>>(result.Data);
            models.ForEach(x => x.FillField());
            result.Data = models;
            return result;
        }

        [HttpGet("[action]/{orderId}")]
        [Authorize(Roles = UserRoleNames.User+","+UserRoleNames.Admin)]
        public Task<ResponseState<string>> OrderDetails(long orderId)
        {
            return TryCatch(async () =>
            {
                var order = await _orderService.GetOrderById(orderId);
                var card=User.IsInRole(UserRoleNames.Admin)? await _orderService.GetCardOrder(orderId,order.UserId):
                await _orderService.GetCardOrder(orderId);
                ShippingModel model = new ShippingModel()
                {
                    Card = card,
                    OrderId = orderId
                };
                ViewBag.Number = order.Number;
                ViewBag.IsPayable = false;
                ViewBag.IsPaid = false;
                if (order.Status==StatusTypes.Registered || order.Status==StatusTypes.UnsuccessfulPayment)
                {
                    ViewBag.IsPayable = true;
                }
                if (order.Status == StatusTypes.SuccessfulPayment)
                {
                    ViewBag.IsPaid = true;
                }
                var view = await PartialViewToString(PartialView("Details", model), ControllerContext);
                return Success(data: view);
            });
        }


        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public IActionResult OrderIndex()
        {
            ViewBag.Status = GetStatusTypeSelectList();
            ViewBag.Shippings = GetShippingStatusTypeSelectList();
            ViewBag.Users = GetUserSelectList();
            return View();
        }

        [HttpGet("[action]")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public async Task<DataSourceResult> Data_ReadOrder([DataSourceRequest]DataSourceRequest request, OrderSearchModel model)
        {
            var entities = _orderService.GetAllOrderAsQueryable(null, model);
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<OrderIndexModel>>(result.Data);
            models.ForEach(x => x.FillField());
            result.Data = models;
            return result;
        }


        [HttpPost("[action]/{id}/{state}")]
        [Authorize(Roles = UserRoleNames.Admin)]
        public Task<DefaultResponseState> ChangeStatus(long id, ShippingSatusTypes state)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _orderService.GetOrderById(id);
                entity.ShippingStatus = state;
                await _orderService.UpdateOrder(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private List<SelectListItem> GetStatusTypeSelectList()
        {
            Array values = Enum.GetValues(typeof(StatusTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        private List<SelectListItem> GetShippingStatusTypeSelectList()
        {
            Array values = Enum.GetValues(typeof(ShippingSatusTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        private List<SelectListItem> GetUserSelectList()
        {
            var users = _userManager.GetUsers().Result;
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var user in users)
            {
                selectList.Add(new SelectListItem
                {
                    Text = user.FullName,
                    Value = user.Id.ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}
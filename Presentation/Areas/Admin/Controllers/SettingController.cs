﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Enums;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Setting;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Setting;
using Shop.Services.Shop;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class SettingController : BaseAdminController
    {
        #region Fields
        private readonly IAppContext _appContext;
        private readonly ISettingService _settingService;
        #endregion

        #region Constructors

        public SettingController(
          IStringLocalizer<SettingController> localizer,
          ILogger<SettingController> logger,
           IAppContext appContext,
           ISettingService settingService
          ) : base(logger, localizer)
        {
            _appContext = appContext;
            _settingService = settingService;
        }

        #endregion

        #region Actions


        [HttpGet("[action]")]
        public async Task<IActionResult> ModifySettings()
        {
            ViewBag.Units = GetPriceUnitTypesSelectList();
            var setting =  _settingService.Load<GetwaySettingsModel>(null);
            var unitSetting =  _settingService.Load<PriceUnitSettingModel>(null);
            var mellatSetting =  _settingService.Load<MellatGetwaySettingModel>(null);
            var derakSetting =  _settingService.Load<DerakSettingModel>(null);
            var websiteSetting =  _settingService.Load<WebsiteSettingModel>(null);

            return View("ModifySettings", new SettingsModel() 
            { 
                GetwaySettings = setting,
                PriceUnitSetting = unitSetting,
                MellatGetwaySetting=mellatSetting,
                DerakSetting=derakSetting,
                WebsiteSetting=websiteSetting
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> ModifySettings(SettingsModel model,IFormFile WebsiteSetting_LogoUrl, IFormFile WebsiteSetting_IconUrl)
        {
            return TryCatch(async () =>
            {
                var settings = new Dictionary<object, List<string>>();
                if (model.GetwaySettings != null)
                {
                    settings.Add(model.GetwaySettings, null);
                }
                if (model.PriceUnitSetting != null)
                {
                    settings.Add(model.PriceUnitSetting, null);
                } 
                if (model.MellatGetwaySetting != null)
                {
                    settings.Add(model.MellatGetwaySetting, null);
                } 
                if (model.DerakSetting != null)
                {
                    settings.Add(model.DerakSetting, null);
                }
                if (model.WebsiteSetting!=null)
                {
                    var websiteSetting = _settingService.Load<WebsiteSettingModel>(null);
                    model.WebsiteSetting.LogoUrl= await _settingService.SaveWebsiteLogo(websiteSetting.LogoUrl, WebsiteSetting_LogoUrl, WebsiteSetting_IconUrl);
                    settings.Add(model.WebsiteSetting, null);
                }
                if (settings.Count > 0)
                {
                    await _settingService.SaveAll(settings, null, false);
                }
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }
 
        #endregion

        #region Utilities
        private List<SelectListItem> GetPriceUnitTypesSelectList()
        {
            Array values = Enum.GetValues(typeof(PriceUnitTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}

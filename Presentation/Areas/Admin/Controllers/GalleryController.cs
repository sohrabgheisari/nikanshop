﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class GalleryController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IGalleryService _galleryService;
        private readonly IHostingEnvironment _environment;
        private readonly IProductService _productService;
        private string ImagePath = "ShopImage/Gallery";
        #endregion

        #region Constructors
        public GalleryController(
            IStringLocalizer<GalleryModel> localizer,
            IMapper mapper, IGalleryService galleryService,
            ILogger<GalleryController> logger,
            IHostingEnvironment environment,
             IProductService productService
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _galleryService = galleryService;
            _environment = environment;
            _productService = productService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _galleryService.GetAllGalleries();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<GalleryModel>>(result.Data);
            result.Data = models;
            return result;
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> ViewGallery(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _galleryService.GetGalleryById(id);
                var model = _mapper.Map<GalleryModel>(entity);
                var view = await PartialViewToString(PartialView("ViewGallery", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                GalleryModel model = new GalleryModel();
                ViewBag.Products = GetProductsSelectList();
                var view = await PartialViewToString(PartialView("Create", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(GalleryModel model, IFormFile MainImageUrl, IFormFile MobileImageUrl)
        {
            return TryCatch(async () =>
            {
                var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
                if (!Directory.Exists(uploadsRootFolder))
                {
                    Directory.CreateDirectory(uploadsRootFolder);
                }
                //تصویر اصلی
                if (MainImageUrl != null && MainImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(MainImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await MainImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        model.MainImageUrl = "/"+ ImagePath+"/" + newFilename;
                    };
                }
                else
                {
                    return Error(message: "تصویر اصلی الزامی است");
                }
                //تصویر موبایلی
                if (MobileImageUrl != null && MobileImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(MobileImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await MobileImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        model.MobileImageUrl = "/"+ ImagePath + "/" + newFilename;
                    };
                }
                else
                {
                    return Error(message: "تصویر موبایلی الزامی است");
                }
                var entity = _mapper.Map<Gallery>(model);
                await _galleryService.InsertGallery(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _galleryService.GetGalleryById(id);
                ViewBag.Products = GetProductsSelectList();
                var model = _mapper.Map<GalleryModel>(entity);
                var view = await PartialViewToString(PartialView("Edit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(GalleryModel model)
        {
            return TryCatch(async () =>
            {
                var entity =await _galleryService.GetGalleryById(model.Id);
                entity.DisplayOrder = model.DisplayOrder;
                entity.ProductId = model.ProductId;
                await _galleryService.UpdateGallery(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _galleryService.GetGalleryById(id);
                await _galleryService.DeleteGallery(entity);
                DeleteGalleryImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private void  DeleteGalleryImage(Gallery entity)
        {
            var mainPath = _environment.WebRootPath + entity.MainImageUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
            var mobilePath = _environment.WebRootPath + entity.MobileImageUrl;
            if (System.IO.File.Exists(mobilePath))
            {
                System.IO.File.Delete(mobilePath);
            }
        }

        private List<SelectListItem> GetProductsSelectList()
        {
            var products = _productService.GetAllProductAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var prd in products)
            {
                selectList.Add(new SelectListItem
                {
                    Text = prd.Name,
                    Value = prd.Id.ToString()
                });
            }
            return selectList;
        }
        #endregion
    }
}
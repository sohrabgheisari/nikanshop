﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Models.JsTree;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class CategoryController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly ICategoryService _categoryService;
        private readonly IHostingEnvironment _environment;
        private string ImagePath = "ShopImage/Category";
        #endregion

        #region Constructors
        public CategoryController(
            IStringLocalizer<GalleryModel> localizer,
            IMapper mapper, ICategoryService categoryService,
            ILogger<CategoryController> logger, IHostingEnvironment environment
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _categoryService = categoryService;
            _environment = environment;
        }

        #endregion

        #region Actions
        [HttpPost("[action]")]
        public JsonResult GetRoot()
        {
            var nodesList = new List<JsTreeNode>();
            var categories = _categoryService.GetAllRootCategoryAsQueryable().ToList();
            foreach (var cat in categories)
            {
                JsTreeNode node = new JsTreeNode()
                {
                    id = cat.Id.ToString(),
                    children = HasChild(cat.Id),
                    text = cat.Title,
                    parent = "#",
                    icon = "fas fa-user text-success",
                    state=new JsTreeNodeState()
                    {
                        opened=true
                    }
                };
                nodesList.Add(node);
            }

            return Json(nodesList);
        }

        [HttpPost("[action]/{id}")]
        public JsonResult GetChildren(string id)
        {
            var categories = _categoryService.GetAllCategoryChildsAsQueryable(long.Parse(id)).ToList();
            var nodesList = new List<JsTreeNode>();
            foreach (var cat in categories)
            {
                JsTreeNode node = new JsTreeNode()
                {
                    id = cat.Id.ToString(),
                    children = HasChild(cat.Id),
                    text = cat.Title,
                    parent = id
                };
                nodesList.Add(node);
            }
            return Json(nodesList);
        }

        [HttpGet("[action]")]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> ViewCategories()
        {
            return TryCatch(async () =>
            {
                var view = await PartialViewToString(PartialView("Categories"), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpGet("[action]/{parentId}")]
        public Task<ResponseState<string>> Create(string parentId)
        {
            return TryCatch(async () =>
            {
                CategoryModel model = new CategoryModel()
                {
                    Parent_Id = long.Parse(parentId),
                    Level = await _categoryService.GetLevel(long.Parse(parentId))
                };
                if (model.Level>3)
                {
                    return Error<string>(message:"شما مجاز به ایجاد بیشتر از سه سطح نمی باشید");
                }
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(CategoryModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
                if (!Directory.Exists(uploadsRootFolder))
                {
                    Directory.CreateDirectory(uploadsRootFolder);
                }
                //تصویر اصلی
                if (ImageUrl != null && ImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        model.ImageUrl = "/"+ ImagePath+"/" + newFilename;
                    };
                }
                var entity = _mapper.Map<Category>(model);
                await _categoryService.InsertCategory(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(string id)
        {
            return TryCatch(async () =>
            {
                var entity = await _categoryService.GetCategoryById(long.Parse(id));
                var model = _mapper.Map<CategoryModel>(entity);
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(CategoryModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                var entity = await _categoryService.GetCategoryById(model.Id);
                var uploadsRootFolder = Path.Combine(_environment.WebRootPath, ImagePath);
                if (!Directory.Exists(uploadsRootFolder))
                {
                    Directory.CreateDirectory(uploadsRootFolder);
                }
                //تصویر اصلی
                if (ImageUrl != null && ImageUrl.Length > 0)
                {
                    string strFileExtension = Path.GetExtension(Path.GetFileName(ImageUrl.FileName));
                    string newFilename = Guid.NewGuid().ToString() + strFileExtension;
                    var FilePath = Path.Combine(uploadsRootFolder, newFilename);
                    using (var FileStream = new FileStream(FilePath, FileMode.Create))
                    {
                        await ImageUrl.CopyToAsync(FileStream).ConfigureAwait(false);
                        model.ImageUrl = "/" + ImagePath + "/" + newFilename;
                        DeleteImage(entity);
                    };
                }
                else
                {
                    model.ImageUrl = entity.ImageUrl;
                }
                var updatedEntity = _mapper.Map<Category>(model);
                await _categoryService.UpdateCategory(updatedEntity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(string id)
        {
            return TryCatch(async () =>
            {
                if (long.Parse(id) <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _categoryService.GetCategoryById(long.Parse(id));
                await _categoryService.DeleteCategory(entity);
                DeleteImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private void DeleteImage(Category entity)
        {
            var mainPath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
            var mobilePath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mobilePath))
            {
                System.IO.File.Delete(mobilePath);
            }
        }

        private bool HasChild(long id)
        {
            var result = _categoryService.CategoryHasChild(id).Result;
            return result;
        }
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.PropertyCategory;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Infrastructure;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class PropertyCategoryController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IPropertyCategoryService _propertyCategoryService;
        #endregion

        #region Constructors
        public PropertyCategoryController(
            IStringLocalizer<PropertyCategoryModel> localizer,
            IMapper mapper, IPropertyCategoryService propertyCategoryService,
            ILogger<PropertyCategoryController> logger
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _propertyCategoryService = propertyCategoryService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
            var entities = _propertyCategoryService.GetAllAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<PropertyCategoryModel>>(result.Data);
            result.Data = models;
            return result;
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                PropertyCategoryModel model = new PropertyCategoryModel();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(PropertyCategoryModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<PropertyCategory>(model);
                await _propertyCategoryService.InsertPropertyCategory(entity);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _propertyCategoryService.GetPropertyCategoryById(id);
                var model = _mapper.Map<PropertyCategoryModel>(entity);
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(PropertyCategoryModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<PropertyCategory>(model);
                await _propertyCategoryService.UpdatePropertyCategory(entity);
                return Success(entity.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                await _propertyCategoryService.DeletePropertyCategory(new PropertyCategory() {Id=id });
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Slider;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class SliderController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IHostingEnvironment _environment;
        private readonly ISliderService _sliderService;
        private string ImagePath = "ShopImage/Slider";
        #endregion

        #region Constructors
        public SliderController(
            IStringLocalizer<SliderModel> localizer,
            IMapper mapper, ISliderService sliderService,
            ILogger<SliderController> logger, IHostingEnvironment environment
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _environment = environment;
            _sliderService = sliderService;
        }

        #endregion

        #region Actions
        [HttpGet]
        public IActionResult Index()
        {
            ViewBag.Types = GetSliderTypeSelectList();
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest]DataSourceRequest request)
        {
           
            var entities = _sliderService.GetAllSliderAsQueryable(null);
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<SliderModel>>(result.Data);
            result.Data = models;
            return result;
        }
   
        [HttpGet("[action]")]
        public Task<ResponseState<string>> Create()
        {
            return TryCatch(async () =>
            {
                SliderModel model = new SliderModel();
                ViewBag.Types = GetSliderTypeSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(SliderModel model, IFormFile SliderUrl)
        {
            return TryCatch(async () =>
            {             
                await _sliderService.InsertSlider(model,SliderUrl);
                return Success(message: Localizer["msg.save-succeed"].Value); ;
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> Edit(long id)
        {
            return TryCatch(async () =>
            {
                var entity = await _sliderService.GetSliderById(id);
                var model = _mapper.Map<SliderModel>(entity);
                ViewBag.Types = GetSliderTypeSelectList();
                var view = await PartialViewToString(PartialView("CreateEdit", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Edit(SliderModel model, IFormFile SliderUrl)
        {
            return TryCatch(async () =>
            {               
                await _sliderService.UpdateSlider(model, SliderUrl);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(long id)
        {
            return TryCatch(async () =>
            {
                if (id <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _sliderService.GetSliderById(id);
                await _sliderService.DeleteSlider(entity);
                DeleteImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        #endregion

        #region Utilities
        private void DeleteImage(Slider entity)
        {
            var mainPath = _environment.WebRootPath + entity.SliderUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
        }

        private List<SelectListItem> GetSliderTypeSelectList()
        {
            Array values = Enum.GetValues(typeof(SliderTypes));
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in values)
            {
                selectList.Add(new SelectListItem
                {
                    Text = ((Enum)en).EnumToDescription(),
                    Value = ((byte)en).ToString()
                });

            }
            return selectList;
        }
        #endregion
    }
}
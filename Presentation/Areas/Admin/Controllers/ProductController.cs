﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Areas.Admin.Models.Product;
using Shop.Areas.Admin.Models.Gallery;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Services.Shop;
using Microsoft.AspNetCore.Mvc.Rendering;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Product;
using NikanBase.Core.Enums;
using Shop.Areas.Admin.Models.NikanProductEquivalent;
using System;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    [Authorize(Roles = UserRoleNames.Admin)]
    public class ProductController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IProductService _ProductService;
        private readonly ICategoryService _categoryService;
        private readonly IHostingEnvironment _environment;
        private readonly IDiscountService _discountService;
        private readonly IPropertyService _propertyService;
        private readonly IPropertyCategoryService _propertyCategoryService;
        private readonly ITraitService _traitService;
        private readonly IDerakService _derakService;
        private readonly IBrandService _brandService;
        #endregion

        #region Constructors
        public ProductController(
            IStringLocalizer<GalleryModel> localizer,
            IMapper mapper, IProductService ProductService,
            ILogger<ProductController> logger, IHostingEnvironment environment,
            ICategoryService categoryService, IDiscountService discountService,
            IPropertyService propertyService, IPropertyCategoryService propertyCategoryService,
            ITraitService traitService, IDerakService derakService, IBrandService brandService
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _ProductService = ProductService;
            _environment = environment;
            _categoryService = categoryService;
            _discountService = discountService;
            _propertyService = propertyService;
            _propertyCategoryService = propertyCategoryService;
            _traitService = traitService;
            _derakService = derakService;
            _brandService = brandService;
        }

        #endregion

        #region Actions

        public IActionResult Index()
        {
            ViewBag.Result = TempData["Result"];
            return View();
        }

        [HttpGet("[action]")]
        public async Task<DataSourceResult> Data_Read([DataSourceRequest] DataSourceRequest request)
        {
            var entities = _ProductService.GetAllProductAsQueryable();
            var result = await entities.ToDataSourceResultAsync(request);
            var models = _mapper.Map<List<ProductIndexModel>>(result.Data);
            models.ForEach(x =>
            {
                x.FillFields();
                var catName = GetCategoryName(x.CategoryId).Result;
                x.CategoryName = catName.Data;
            });
            result.Data = models;
            return result;
        }


        [HttpGet("[action]")]
        public IActionResult Create()
        {
            ProductModel model = new ProductModel() { Publish = true };
            ViewBag.Disconts = GetDiscountSelectList();
            ViewBag.CategoryName = "";
            ViewBag.Categories = GetPropertyCategorySelectList();
            ViewBag.Cats = GetCategorySelectList();
            ViewBag.Colors = GetTraitSelectList(TraitTypes.Color);
            ViewBag.Warranties = GetTraitSelectList(TraitTypes.Warranty);
            ViewBag.Brands = GetBrandsSelectList();
            return View("CreateEdit", model);
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> Create(ProductModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                await _ProductService.InsertProduct(model, ImageUrl);
                TempData["Result"] = Localizer["msg.save-succeed"].Value;
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Edit(long id)
        {

            var entity = await _ProductService.GetProductById(id);
            var model = _mapper.Map<ProductModel>(entity);

            List<ProductTraitModel> productTraits = entity.ProductTraits.Select(x => new ProductTraitModel()
            {
                ColorTriatId=x.ColorTriatId,
                ColorTriatName= x.ColorTriatId.HasValue?x.ColorTrait.Title:"",
                Id=x.Id,
                PriceIncrease=x.PriceIncrease,
                ProductId=x.ProductId,
                TraitInventory=x.Inventory,
                WarrantyDate= x.WarrantyDate.HasValue?x.WarrantyDate.ToPersianDate():"",
               WarrantyTriatId=x.WarrantyTriatId,
               WarrantyTriatName= x.WarrantyTriatId.HasValue?x.WarrantyTrait.Title:"",
               IsActive=x.IsActive,
               MinimumCart=x.MinimumCart,
               MaximumCart=x.MaximumCart,
               BarCode=x.BarCode
            }).ToList();
            List<ProductPropertyModel> productProperties = entity.ProductProperties.Select(x => new ProductPropertyModel()
            {
                CategoryId = x.Property.PropertyCategory.Id,
                CategoryName = x.Property.PropertyCategory.Name,
                Order = x.Order,
                ProductId = x.ProductId,
                PropertyId = x.PropertyId,
                PropertyName = x.Property.Name,
                Value = x.Value,
                Visible = x.Visible
            }).ToList();
            List<SimilarProductModel> similarProducts = entity.SimilarProduct_Product.Select(x => new SimilarProductModel()
            {
                SimilarProductId = x.SimilarProductId,
                SimilarProductName = x.ProductSimilar.Name
            }).ToList();
            List<long> productTraitIds = entity.OrderItems.Where(x => x.ProductTraitId != null).Select(x => x.ProductTraitId.Value).Distinct().ToList();
            ViewBag.TraitIds = productTraitIds;
            ViewBag.Properties = productProperties;
            ViewBag.Traits = productTraits;
            ViewBag.Similars = similarProducts;
            ViewBag.CategoryName = GetName(entity.Category);
            ViewBag.Disconts = GetDiscountSelectList();
            ViewBag.Categories = GetPropertyCategorySelectList();
            ViewBag.Cats = GetCategorySelectList();
            ViewBag.Colors = GetTraitSelectList(TraitTypes.Color);
            ViewBag.Warranties = GetTraitSelectList(TraitTypes.Warranty);
            ViewBag.Brands = GetBrandsSelectList();
            return View("CreateEdit", model);
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> Edit(ProductModel model, IFormFile ImageUrl)
        {
            return TryCatch(async () =>
            {
                await _ProductService.UpdateProduct(model, ImageUrl);
                TempData["Result"] = Localizer["msg.save-succeed"].Value;
                return Success(model.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpGet("[action]")]
        public IActionResult GroupAllocationProperty()
        {
            GroupAllocationPropertyModel model = new GroupAllocationPropertyModel();
            ViewBag.Categories = GetPropertyCategorySelectList();
            ViewBag.Cats = GetCategorySelectList();
            return View("GroupAllocationProperty", model);
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> GroupAllocationProperty(GroupAllocationPropertyModel model)
        {
            return TryCatch(async () =>
            {
                await _ProductService.GroupAllocation(model);
                TempData["Result"] = Localizer["msg.save-succeed"].Value;
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpPost("[action]/{id}")]
        public Task<DefaultResponseState> Delete(string id)
        {
            return TryCatch(async () =>
            {
                if (long.Parse(id) <= 0)
                {
                    return Error(Localizer["msg.invalid-request"].Value);
                }
                var entity = await _ProductService.GetProductById(long.Parse(id));
                await _ProductService.DeleteProduct(entity);
                DeleteImage(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }

        [HttpGet("[action]")]
        public Task<ResponseState<string>> ViewCategory()
        {
            return TryCatch(async () =>
            {
                var view = await PartialViewToString(PartialView("Categories"), ControllerContext);

                return Success(data: view);
            });
        }

        [HttpGet("[action]/{id}")]
        public Task<ResponseState<string>> GetCategoryName(long id)
        {
            return TryCatch(async () =>
            {
                var cat = await _categoryService.GetCategoryById(id);
                string name = GetName(cat);
                return Success(data: name);
            });
        }

        [HttpGet("[action]/{catId}")]
        public Task<ResponseState<List<ProductIndexModel>>> GetProductsInCategory(long catId)
        {
            return TryCatch(async () =>
            {
                var entities = _ProductService.GetAllProductAsQueryable(catId).ToList();
                var models = _mapper.Map<List<ProductIndexModel>>(entities);
                return Success(data: models);
            });
        }


        [HttpGet("[action]/{productId}")]
        public Task<ResponseState<string>> CreateEquivalent(long productId)
        {           
            return TryCatch(async () =>
            {
                var eq = await _ProductService.GetProductEquivalentById(productId);
                if (eq!=null)
                {
                   var res= await EditEquivalent(productId);
                    return Success(data: res.Data);
                }
                NikanProductEquivalentModel model = new NikanProductEquivalentModel() { ShopProductId = productId };
                ViewBag.NikanProducts = GetNikanProductSelectList();
                var view = await PartialViewToString(PartialView("CreateEditEquivalent", model), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<DefaultResponseState> CreateEquivalent(NikanProductEquivalentModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<NikanProductEquivalent>(model);
                await _ProductService.InsertProductEquivalent(entity);
                return Success(message: Localizer["msg.save-succeed"].Value);
            });
        }


        [HttpGet("[action]/{productId}")]
        public Task<ResponseState<string>> EditEquivalent(long productId)
        {
            return TryCatch(async () =>
            {
                var eq = await _ProductService.GetProductEquivalentById(productId);
                NikanProductEquivalentModel eqModel = _mapper.Map<NikanProductEquivalentModel>(eq);
                ViewBag.NikanProducts = GetNikanProductSelectList();
                var view = await PartialViewToString(PartialView("CreateEditEquivalent", eqModel), ControllerContext);
                return Success(data: view);
            });
        }

        [HttpPost("[action]")]
        public Task<ResponseState<long>> EditEquivalent(NikanProductEquivalentModel model)
        {
            return TryCatch(async () =>
            {
                var entity = _mapper.Map<NikanProductEquivalent>(model);
                await _ProductService.UpdateProductEquivalent(entity);
                return Success(model.Id, message: Localizer["msg.save-succeed"].Value);
            });
        }
        #endregion

        #region Utilities
        private void DeleteImage(Product entity)
        {
            var mainPath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mainPath))
            {
                System.IO.File.Delete(mainPath);
            }
            var mobilePath = _environment.WebRootPath + entity.ImageUrl;
            if (System.IO.File.Exists(mobilePath))
            {
                System.IO.File.Delete(mobilePath);
            }
        }
        private List<SelectListItem> GetDiscountSelectList()
        {
            var discounts = _discountService.GetAllValidDescountAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var dis in discounts)
            {
                selectList.Add(new SelectListItem
                {
                    Text = dis.Tittle,
                    Value = dis.Id.ToString()
                });
            }
            return selectList;
        }

        private List<SelectListItem> GetBrandsSelectList()
        {
            var discounts = _brandService.GetAllAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var dis in discounts)
            {
                selectList.Add(new SelectListItem
                {
                    Text = dis.Title,
                    Value = dis.Id.ToString()
                });
            }
            return selectList;
        }
        private string GetName(Category entity)
        {
            string s = entity.Title;
            long id = entity.Id;
            while (id != 0)
            {
                long parentId = Getparent(id);
                if (parentId > 0)
                {
                    var cat = _categoryService.GetCategoryById(parentId).Result;
                    if (cat.Parent_Id != null)
                    {
                        s = cat.Title + "/" + s;
                    }
                }
                id = parentId;
            }
            return s;
        }

        private long Getparent(long id)
        {
            var cat = _categoryService.GetCategoryById(id).Result;
            if (cat != null)
            {
                if (cat.Parent_Id.HasValue && cat.Id != 1)
                {
                    return cat.Parent_Id.Value;
                }
            }
            return 0;
        }

        private List<SelectListItem> GetPropertyCategorySelectList()
        {
            var entities = _propertyCategoryService.GetAllAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in entities)
            {
                selectList.Add(new SelectListItem
                {
                    Text = en.Name,
                    Value = en.Id.ToString()
                });

            }
            return selectList;
        }

        private List<SelectListItem> GetCategorySelectList()
        {
            var entities = _categoryService.GetAllCategoryAsQueryable().ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in entities.Where(x => x.Level >= 1).OrderBy(x => x.Level).ToList())
            {
                if (en.Level == 1)
                {
                    selectList.Add(new SelectListItem
                    {
                        Text = en.Title,
                        Value = en.Id.ToString()
                    });

                }
                else if (en.Level == 2)
                {
                    string parent = entities.FirstOrDefault(x => x.Id == en.Parent_Id.Value).Title;
                    selectList.Add(new SelectListItem
                    {
                        Text = parent + "/" + en.Title,
                        Value = en.Id.ToString()
                    });
                }
                else if (en.Level == 3)
                {
                    var parentL2 = entities.FirstOrDefault(x => x.Id == en.Parent_Id.Value);
                    string level2 = parentL2.Title;
                    string level1 = entities.FirstOrDefault(x => x.Id == parentL2.Parent_Id.Value).Title;
                    selectList.Add(new SelectListItem
                    {
                        Text = level1 + "/"+level2+"/" + en.Title,
                        Value = en.Id.ToString()
                    });
                }
            }
            return selectList.OrderBy(x=>x.Text).ToList();
        }

        private List<SelectListItem> GetTraitSelectList(TraitTypes type)
        {
            var entities = _traitService.GetAllTraitAsQueryable(type).ToList();
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var en in entities)
            {
                selectList.Add(new SelectListItem
                {
                    Text = en.Title,
                    Value = en.Id.ToString()
                });
            }
            return selectList;
        }

        private List<SelectListItem> GetNikanProductSelectList()
        {
            var products = _derakService.GetNikanProduct().Result;
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (var prd in products)
            {
                string levelName = "";
                levelName += prd.Level1_Name;
                if (!String.IsNullOrEmpty(prd.Level2_Name))
                {
                    levelName = levelName + "-" + prd.Level2_Name;
                }  
                if (!String.IsNullOrEmpty(prd.Level3_Name))
                {
                    levelName = levelName + "-" + prd.Level3_Name;
                }
                selectList.Add(new SelectListItem()
                {
                    Value=prd.LotsId.ToString(),
                    Text=$"{prd.LotsName} ( {levelName} )"
                });
            }
            return selectList.OrderBy(x=>x.Text).ToList();
        }
        #endregion
    }
}
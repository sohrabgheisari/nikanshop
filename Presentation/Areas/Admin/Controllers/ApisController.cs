﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NikanBase.Core.Domain.Shop;
using Shop.Areas.Admin.Models;
using Shop.Controllers;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Services.Dto.Brand;
using Shop.Services.Shop;

namespace Shop.Areas.Admin.Controllers
{
    [Route("api/[controller]")]
    [Route("[area]/[controller]")]
    public class ApisController : BaseAdminController
    {
        #region Fields
        private readonly IMapper _mapper;
        private readonly IAppContext _appContext;
        private readonly IProductFavoriteService _productFavoriteService;
        #endregion

        #region Constructors
        public ApisController(
            IStringLocalizer<BrandModel> localizer,
            IMapper mapper, IBrandService brandService,
            ILogger<ApisController> logger, IHostingEnvironment environment,
            IProductFavoriteService productFavoriteService,
            IAppContext appContext
        ) : base(logger, localizer)
        {
            _mapper = mapper;
            _productFavoriteService = productFavoriteService;
            _appContext = appContext;
        }

        #endregion

        #region Actions
        [HttpPost("favorite")]
        [Authorize(Roles =UserRoleNames.User)]
        public Task<ResponseState<int>> SetFavorite(long productId)
        {
            return TryCatch(async () =>
            {
                var favId = await _productFavoriteService.HasFav(productId);
                if (favId>0)
                {
                    await _productFavoriteService.DeleteFav(favId);
                    return Success(0);
                }
                else
                {
                    await _productFavoriteService.InsertFav(new ProductFavorite() { ProductId = productId, UserId = _appContext.User.Id });
                    return Success(1);
                }

            });
        }

 

        #endregion

  
    }
}
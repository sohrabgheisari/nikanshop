﻿using Shop.Services.Dto.Product;

namespace Shop.Areas.Admin.Models.Gallery
{
    public class GalleryModel:BaseModel
    {
        public GalleryModel()
        {

        }
        public string MainImageUrl { get; set; }
        public string MobileImageUrl { get; set; }
        public int DisplayOrder { get; set; }
        public long? ProductId { get; set; }
        public ProductModel Product { get; set; }
        public string ProductName { get; set; }
        public void FillFileds()
        {
            ProductName = Product != null ? Product.Name : "";
        }
    }
}

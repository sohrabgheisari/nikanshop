﻿using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.ProductImage;
using Shop.Areas.Admin.Models.ProductProperty;
using Shop.Services.Dto.Brand;
using Shop.Services.Dto.Product;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shop.Areas.Admin.Models.Product
{
    public class ProductShopIndexModel : BaseModel
    {
        public ProductShopIndexModel()
        {

        }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public decimal Price { get; set; }
        public int Inventory { get; set; }
        public string ImageUrl { get; set; }
        public long? DiscountId { get; set; }
        public NikanBase.Core.Domain.Shop.Discount Discount { get; set; }
        public ICollection<ProductImageModel> ProductImages { get; set; }
        public CategoryModel Category { get; set; }
        public string CategoryName { get; set; }
        public BrandModel Brand { get; set; }
        public string BrandName { get; set; }
        public long? BrandId { get; set; }
        public decimal Score { get; set; }
        public bool HasDiscount { get; set; }
        public string DiscountPercentage { get; set; }
        public decimal PriceWithDiscount { get; set; }
        public List<ProductPropertyIndexModel> ProductProperties { get; set; }
        public List<ProductTraitsModel> ProductTraits { get; set; }
        public bool HasTraitInventory { get; set; }
        public void FillFields()
        {
            CategoryName = Category.Title;
            BrandName = Brand != null ? Brand.Title : "";
            if (Discount != null)
            {
                if ((Discount.DiscountTo.HasValue && Discount.DiscountFrom.HasValue && DateTime.Now.Date >= Discount.DiscountFrom.Value.Date &&
                    DateTime.Now.Date <= Discount.DiscountTo.Value.Date) || (!Discount.DiscountTo.HasValue && !Discount.DiscountFrom.HasValue) ||
                    (!Discount.DiscountTo.HasValue && Discount.DiscountFrom.HasValue && DateTime.Now.Date >= Discount.DiscountFrom.Value.Date))
                {
                    if (Discount.Type == Core.Enums.DiscountTypes.Percentage)
                    {
                        if (Discount.Percentage.HasValue)
                        {
                            decimal discountPrice = Math.Round(Discount.Percentage.Value * Price / 100);
                            if (Discount.MaxAmount.HasValue && discountPrice > Discount.MaxAmount.Value)
                            {
                                discountPrice = Discount.MaxAmount.Value;
                            }
                            if (discountPrice > 0)
                            {
                                HasDiscount = true;
                                PriceWithDiscount = Price - discountPrice;
                                if (Shop.Core.Infrastructure.CommonHelper.IsInteger(Discount.Percentage.Value))
                                {
                                    DiscountPercentage = ((int)Discount.Percentage.Value).ToString() + "%";
                                }
                                else
                                {
                                    DiscountPercentage = String.Format("{0:0.##}", Discount.Percentage.Value) + "%";
                                }
                            }
                            else
                            {
                                PriceWithDiscount = Price;
                            }
                        }

                    }
                    else
                    {
                        if (Discount.Amount.HasValue && Discount.Amount.Value > 0 && Discount.Amount.Value < Price)
                        {
                            var percent = Math.Round(Discount.Amount.Value * 100 / Price);
                            DiscountPercentage = percent.ToString() + "%";
                            PriceWithDiscount = Price - Discount.Amount.Value;
                            HasDiscount = true;
                        }
                    }
                }
            }
            else
            {
                PriceWithDiscount = Price;
            }

            if (ProductTraits.Any())
            {
                if (ProductTraits.Sum(x=>x.Inventory)>0)
                {
                    HasTraitInventory = true;
                }
            }
        }
    }
}

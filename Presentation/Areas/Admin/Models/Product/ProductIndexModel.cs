﻿using Shop.Areas.Admin.Models.Category;
using Shop.Areas.Admin.Models.Discount;
using Shop.Areas.Admin.Models.NikanProductEquivalent;
using Shop.Services.Dto.Brand;

namespace Shop.Areas.Admin.Models.Product
{
    public class ProductIndexModel:BaseModel
    {
        public ProductIndexModel()
        {

        }
        public string Name { get; set; }
        public string EnglishName { get; set; }
        public string Description { get; set; }
        public long CategoryId { get; set; }
        public bool Publish { get; set; }
        public bool SpecialProduct { get; set; }
        public bool NewProduct { get; set; }
        public decimal Price { get; set; }
        public int Inventory { get; set; }
        public string ImageUrl { get; set; }
        public string InsertDate { get; set; }
        public long? DiscountId { get; set; }
        public long? BrandId { get; set; }
        public decimal Score { get; set; }
        public string BrandName { get; set; }
        public BrandModel Brand { get; set; }
        public CategoryModel Category { get; set; }
        public DiscountModel Discount { get; set; }
        public NikanProductEquivalentModel NikanProductEquivalent { get; set; }
        public string CategoryName { get; set; }
        public string DiscountName { get; set; }
        public string NikanProductId { get; set; }
        public void FillFields()
        {
            BrandName = Brand != null ? Brand.Title : "";
            DiscountName = DiscountId.HasValue ? Discount.Tittle : "";
            NikanProductId = NikanProductEquivalent != null ? NikanProductEquivalent.NikanProductId.ToString() : "";
        }
    }
}

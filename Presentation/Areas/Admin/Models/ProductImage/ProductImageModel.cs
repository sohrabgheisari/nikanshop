﻿namespace Shop.Areas.Admin.Models.ProductImage
{
    public class ProductImageModel:BaseModel
    {
        public ProductImageModel()
        {

        }
        public long ProductId { get; set; }
        public string ImageUrl { get; set; }
        public int Order { get; set; }
    }
}

﻿using Shop.Areas.Admin.Models.PropertyCategory;

namespace Shop.Areas.Admin.Models.Property
{
    public class PropertyIndexModel:BaseModel
    {
        public PropertyIndexModel()
        {

        }
        public string Name { get; set; }
        public long PropertCategoryId { get; set; }
        public string PropertCategoryName { get; set; }
        public PropertyCategoryModel PropertyCategory { get; set; }
        public void FillFields()
        {
            PropertCategoryName = PropertyCategory.Name;
        }
    }
}

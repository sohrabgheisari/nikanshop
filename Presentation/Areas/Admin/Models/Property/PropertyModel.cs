﻿namespace Shop.Areas.Admin.Models.Property
{
    public class PropertyModel:BaseModel
    {
        public PropertyModel()
        {

        }
        public string Name { get; set; }
        public long PropertCategoryId { get; set; }
    }
}

﻿using Shop.Core.Enums;

namespace Shop.Areas.Admin.Models.Discount
{
    public class DiscountModel:BaseModel
    {
        public DiscountModel()
        {

        }
        public string Tittle { get; set; }
        public DiscountTypes Type { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Percentage { get; set; }
        public string DiscountFrom { get; set; }
        public string DiscountTo { get; set; }
    }
}

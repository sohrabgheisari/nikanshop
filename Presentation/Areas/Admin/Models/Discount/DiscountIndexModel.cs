﻿using Shop.Core.Enums;
using Shop.Core.Infrastructure;

namespace Shop.Areas.Admin.Models.Discount
{
    public class DiscountIndexModel : BaseModel
    {
        public DiscountIndexModel()
        {

        }
        public string Tittle { get; set; }
        public DiscountTypes Type { get; set; }
        public decimal? Amount { get; set; }
        public decimal? MaxAmount { get; set; }
        public decimal? Percentage { get; set; }
        public string DiscountFrom { get; set; }
        public string DiscountTo { get; set; }
        public string TypeName { get; set; }
        public void FillFields()
        {
            TypeName = Type.EnumToDescription();
        }
    }
}

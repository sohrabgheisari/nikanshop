﻿using NikanBase.Core.Enums;
using Shop.Core.Enums;
using Shop.Core.Infrastructure;
using Shop.Models.Account;

namespace Shop.Areas.Admin.Models.Order
{
    public class OrderIndexModel:BaseModel
    {
        public OrderIndexModel()
        {

        }
        public string Number { get; set; }
        public string CreatedAt { get; set; }
        public string PaidAt { get; set; }
        public decimal PayableAmount { get; set; }
        public long UserId { get; set; }
        public RegisterModel User { get; set; }
        public StatusTypes Status { get; set; }
        public string StatusName { get; set; }
        public BankGatewayTypes? Getway { get; set; }
        public ShippingSatusTypes? ShippingStatus { get; set; }
        public string GetwayName { get; set; }
        public string ShippingStatusName { get; set; }
        public string TransactionNumber { get; set; }
        public string TransactionCode { get; set; }
        public bool Payable { get; set; }
        public string UserFullName { get; set; }
        public string Mobile { get; set; }
        public void FillField()
        {
            UserFullName = User.FirstName + " " + User.LastName;
            Mobile = User.PhoneNumber;
            StatusName = Status.EnumToDescription();
            GetwayName = Getway.HasValue ? Getway.EnumToDescription() : "";
            ShippingStatusName = ShippingStatus.HasValue ? ShippingStatus.EnumToDescription() : "";
            if (Status==StatusTypes.Registered||Status==StatusTypes.UnsuccessfulPayment)
            {
                Payable = true;
            }
        }
    }
}

﻿using System.Collections.Generic;

namespace Shop.Areas.Admin.Models.Order
{
    public class OrderModel:BaseModel
    {
        public OrderModel()
        {

        }
        public string Number { get; set; }
        public decimal PayableAmount { get; set; }
        public long UserId { get; set; }
        public string Address { get; set; }
        public ICollection<OrderItemModel> OrderItems { get; set; }
    }
}

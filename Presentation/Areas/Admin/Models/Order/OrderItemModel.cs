﻿using Shop.Services.Dto.Product;

namespace Shop.Areas.Admin.Models.Order
{
    public class OrderItemModel:BaseModel
    {
        public OrderItemModel()
        {

        }
        public long ProductId { get; set; }
        public long OrderId { get; set; }
        public int Count { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal PriceWithDiscount { get; set; }
        public ProductModel Product { get; set; }
    }
}

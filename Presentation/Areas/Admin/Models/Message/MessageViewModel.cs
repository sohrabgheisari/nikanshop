﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shop.Areas.Admin.Models.Message
{
    public class MessageViewModel: BaseModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Subject { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public string Phone { get; set; }
        public Boolean IsRead { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime? ReadDate { get; set; }
        public Boolean? IsArchived { get; set; }
    }
}

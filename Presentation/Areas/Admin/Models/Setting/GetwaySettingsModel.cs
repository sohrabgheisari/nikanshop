﻿using NikanBase.Core.Enums;

namespace Shop.Areas.Admin.Models.Setting
{
    public class GetwaySettingsModel
    {
        public BankGatewayTypes BankGateway_Type { get; set; }
        public string BankGateway_TerminalNo { get; set; }
    }
}

﻿using Shop.Services.Dto.Setting;

namespace Shop.Areas.Admin.Models.Setting
{
    public class SettingsModel
    {
        public GetwaySettingsModel GetwaySettings { get; set; } = new GetwaySettingsModel();
        public PriceUnitSettingModel PriceUnitSetting { get; set; } = new PriceUnitSettingModel();
        public MellatGetwaySettingModel MellatGetwaySetting { get; set; } = new MellatGetwaySettingModel();
        public DerakSettingModel DerakSetting { get; set; } = new DerakSettingModel();
        public WebsiteSettingModel WebsiteSetting { get; set; } = new WebsiteSettingModel();
    }
}

﻿namespace Shop.Areas.Admin.Models.Category
{
    public class CategoryModel:BaseModel
    {
        public CategoryModel()
        {

        }
        public long? Parent_Id { get; set; }
        public int Level { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public bool AddToMenu { get; set; }
    }
}

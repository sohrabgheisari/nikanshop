﻿using Shop.Areas.Admin.Models.Property;
using Shop.Services.Dto;

namespace Shop.Areas.Admin.Models.ProductProperty
{
    public class ProductPropertyIndexModel:BaseModel
    {
        public ProductPropertyIndexModel()
        {

        }
        public long ProductId { get; set; }
        public long PropertyId { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
        public bool Visible { get; set; }
        public PropertyModel Property { get; set; }
        public string PropertyName { get; set; }
        public void FillFields()
        {
            PropertyName = Property.Name;
        }
    }
}

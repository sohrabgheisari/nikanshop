﻿namespace Shop.Areas.Admin.Models
{
    public enum ResponseStateMessageTypes
    {
        Info = 0,
        Error = 1,
        Warning = 2,
        Success = 10
    }
}

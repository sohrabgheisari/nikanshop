﻿using Microsoft.AspNetCore.Mvc;

namespace Shop.Areas.Admin.Models
{
    public class BaseModel
    {
        public long Id { get; set; }
    }
}

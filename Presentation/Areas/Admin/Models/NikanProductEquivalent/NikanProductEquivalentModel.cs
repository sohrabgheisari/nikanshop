﻿namespace Shop.Areas.Admin.Models.NikanProductEquivalent
{
    public class NikanProductEquivalentModel:BaseModel
    {
        public NikanProductEquivalentModel()
        {

        }
        public long ShopProductId { get; set; }
        public int NikanProductId { get; set; }
    }
}

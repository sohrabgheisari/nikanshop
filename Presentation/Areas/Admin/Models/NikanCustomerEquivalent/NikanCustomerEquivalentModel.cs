﻿namespace Shop.Areas.Admin.Models.NikanCustomerEquivalent
{
    public class NikanCustomerEquivalentModel:BaseModel
    {
        public long ShopUserId { get; set; }
        public int NikanCustomerId { get; set; }
    }
}

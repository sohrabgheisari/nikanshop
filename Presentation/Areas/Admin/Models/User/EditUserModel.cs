﻿using System.Collections.Generic;

namespace Shop.Areas.Admin.Models.User
{
    public class EditUserModel:BaseModel
    {
        public List<string> UserRoleNames { get; set; }

        public bool IsActive { get; set; }
    }
}

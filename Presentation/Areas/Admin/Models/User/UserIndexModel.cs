﻿using Shop.Areas.Admin.Models.NikanCustomerEquivalent;

namespace Shop.Areas.Admin.Models.User
{
    public class UserIndexModel:BaseModel
    {
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

        public bool IsActive { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }

        public string Description { get; set; }
        public string ExpireDate { get; set; }
        public string Roles { get; set; }
        public NikanCustomerEquivalentModel NikanCustomerEquivalent { get; set; }
        public string NikanUserId { get; set; }

    }
}

﻿namespace Shop.Areas.Admin.Models.User
{
    public class ChangePasswordModel
    {  
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
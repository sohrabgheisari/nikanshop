﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore;
using NLog.Web;
using System;
using Shop.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using NLog;

namespace Shop
{
    public class Program
    {
        public static void Main(string[] args)
        {
            NLogBuilder.ConfigureNLog("nlog.config");

            IWebHost host = CreateWebHostBuilder(args).Build();
            using (IServiceScope scope = host.Services.CreateScope())
            {
                try
                {
                    var context = scope.ServiceProvider.GetService<AppDbContext>();
                    context.Database.Migrate();

                    DBInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    ILogger<Startup> logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();
                    logger.LogError(ex, "An error occurred while migrating or initializing the database.");
                }
            }

            try
            {
                host.Run();
            }
            finally
            {
                LogManager.Shutdown();
            }
        }


        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseNLog()
            .UseDefaultServiceProvider(options => options.ValidateScopes = false); // important::access ServiceProvider.GetServices method in classes other than Startup.cs
    }
}
